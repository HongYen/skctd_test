-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 15, 2020 at 03:23 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yentheme`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_actionscheduler_actions`
--

TRUNCATE TABLE `wp_actionscheduler_actions`;
--
-- Dumping data for table `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(141, 'action_scheduler/migration_hook', 'complete', '2020-06-10 11:28:16', '2020-06-10 11:28:16', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1591788496;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1591788496;}', 1, 1, '2020-06-10 11:28:24', '2020-06-10 11:28:24', 0, NULL),
(142, 'action_scheduler/migration_hook', 'complete', '2020-06-10 11:29:24', '2020-06-10 11:29:24', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1591788564;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1591788564;}', 1, 1, '2020-06-10 11:29:38', '2020-06-10 11:29:38', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_actionscheduler_claims`
--

TRUNCATE TABLE `wp_actionscheduler_claims`;
-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_actionscheduler_groups`
--

TRUNCATE TABLE `wp_actionscheduler_groups`;
--
-- Dumping data for table `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration');

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_actionscheduler_logs`
--

TRUNCATE TABLE `wp_actionscheduler_logs`;
--
-- Dumping data for table `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 141, 'action created', '2020-06-10 11:27:16', '2020-06-10 11:27:16'),
(2, 141, 'action started via WP Cron', '2020-06-10 11:28:24', '2020-06-10 11:28:24'),
(3, 141, 'action complete via WP Cron', '2020-06-10 11:28:24', '2020-06-10 11:28:24'),
(4, 142, 'action created', '2020-06-10 11:28:24', '2020-06-10 11:28:24'),
(5, 142, 'action started via Async Request', '2020-06-10 11:29:38', '2020-06-10 11:29:38'),
(6, 142, 'action complete via Async Request', '2020-06-10 11:29:38', '2020-06-10 11:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_commentmeta`
--

TRUNCATE TABLE `wp_commentmeta`;
-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_comments`
--

TRUNCATE TABLE `wp_comments`;
--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-06-05 09:15:59', '2020-06-05 09:15:59', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_links`
--

TRUNCATE TABLE `wp_links`;
-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_options`
--

TRUNCATE TABLE `wp_options`;
--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://yentheme.test:8080', 'yes'),
(2, 'home', 'http://yentheme.test:8080', 'yes'),
(3, 'blogname', 'Yen Theme', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hongyenhd97@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '', 'yes'),
(11, 'comments_notify', '', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'closed', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '', 'yes'),
(28, 'permalink_structure', '/%category%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:94:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:31:\".+?/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\".+?/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\".+?/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\".+?/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\".+?/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\".+?/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"(.+?)/([^/]+)/embed/?$\";s:63:\"index.php?category_name=$matches[1]&name=$matches[2]&embed=true\";s:26:\"(.+?)/([^/]+)/trackback/?$\";s:57:\"index.php?category_name=$matches[1]&name=$matches[2]&tb=1\";s:46:\"(.+?)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:41:\"(.+?)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:34:\"(.+?)/([^/]+)/page/?([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&paged=$matches[3]\";s:41:\"(.+?)/([^/]+)/comment-page-([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&cpage=$matches[3]\";s:30:\"(.+?)/([^/]+)(?:/([0-9]+))?/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&page=$matches[3]\";s:20:\".+?/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:30:\".+?/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:50:\".+?/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\".+?/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:45:\".+?/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:26:\".+?/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:14:\"(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:26:\"(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:33:\"(.+?)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&cpage=$matches[2]\";s:8:\"(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:78:\"C:\\xampp\\htdocs\\yentheme/wp-content/plugins/advanced-custom-fields-pro/acf.php\";i:2;s:63:\"C:\\xampp\\htdocs\\yentheme/wp-content/plugins/akismet/akismet.php\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'yentheme', 'yes'),
(41, 'stylesheet', 'yentheme', 'yes'),
(42, 'comment_whitelist', '', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '', 'yes'),
(93, 'admin_email_lifespan', '1606900559', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:7:{i:1592191396;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1592194560;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1592212560;a:4:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1592212608;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1592212609;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1592644560;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1591348759;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(130, 'can_compress_scripts', '1', 'no'),
(145, 'current_theme', 'Twenty Twenty/yentheme', 'yes'),
(146, 'theme_mods_twentyseventeen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1591348793;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(147, 'theme_switched', '', 'yes'),
(150, 'theme_mods_yentheme', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:4:{s:7:\"primary\";i:35;s:8:\"expanded\";i:35;s:6:\"footer\";i:0;s:6:\"social\";i:0;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(151, 'recently_activated', 'a:1:{s:30:\"advanced-custom-fields/acf.php\";i:1591758669;}', 'yes'),
(182, '_transient_health-check-site-status-result', '{\"good\":6,\"recommended\":10,\"critical\":1}', 'yes'),
(206, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:1:{i:0;i:35;}}', 'yes'),
(324, 'acf_version', '5.8.9', 'yes'),
(329, 'options_id_danh_muc', '24', 'no'),
(330, '_options_id_danh_muc', 'field_5ee0501418368', 'no'),
(332, 'options_limit_post_category_home', '', 'no'),
(333, '_options_limit_post_category_home', 'field_5ee051512393e', 'no'),
(334, 'options_id_danh_muc_2', '15', 'no'),
(335, '_options_id_danh_muc_2', 'field_5ee05cad3d674', 'no'),
(336, 'options_limit_post_category_home_2', '7', 'no'),
(337, '_options_limit_post_category_home_2', 'field_5ee05cdb3d675', 'no'),
(360, 'options_id_danh_muc_theo_list', '', 'no'),
(361, '_options_id_danh_muc_theo_list', 'field_5ee0720fdc1cb', 'no'),
(362, 'options_limit_post_category_home_theo_list', '1', 'no'),
(363, '_options_limit_post_category_home_theo_list', 'field_5ee07247dc1cc', 'no'),
(367, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.9\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1591771105;s:7:\"version\";s:5:\"5.1.9\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(390, 'wp_mail_smtp_initial_version', '2.1.1', 'no'),
(391, 'wp_mail_smtp_version', '2.1.1', 'no'),
(392, 'wp_mail_smtp', 'a:8:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:17:\"habadvance@hab.vn\";s:9:\"from_name\";s:9:\"Yen Theme\";s:6:\"mailer\";s:4:\"smtp\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:1;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:7:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;s:4:\"host\";s:15:\"smtp.google.com\";s:10:\"encryption\";s:3:\"tls\";s:4:\"port\";i:587;s:4:\"user\";s:17:\"habadvance@hab.vn\";s:4:\"pass\";s:8:\"Hab@2020\";}s:7:\"smtpcom\";a:2:{s:7:\"api_key\";s:0:\"\";s:7:\"channel\";s:0:\"\";}s:11:\"pepipostapi\";a:1:{s:7:\"api_key\";s:0:\"\";}s:10:\"sendinblue\";a:1:{s:7:\"api_key\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:1:{s:7:\"api_key\";s:0:\"\";}s:5:\"gmail\";a:2:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";}}', 'no'),
(393, 'wp_mail_smtp_activated_time', '1591788435', 'no'),
(394, 'action_scheduler_hybrid_store_demarkation', '140', 'yes'),
(395, 'schema-ActionScheduler_StoreSchema', '3.0.1591788436', 'yes'),
(396, 'schema-ActionScheduler_LoggerSchema', '2.0.1591788436', 'yes'),
(400, 'wp_mail_smtp_migration_version', '2', 'yes'),
(401, 'wp_mail_smtp_review_notice', 'a:2:{s:4:\"time\";i:1591788436;s:9:\"dismissed\";b:0;}', 'yes'),
(402, 'action_scheduler_lock_async-request-runner', '1592191448', 'yes'),
(404, 'action_scheduler_migration_status', 'complete', 'yes'),
(405, 'wp_mail_smtp_debug', 'a:1:{i:0;s:102:\"Mailer: Other SMTP\r\nSMTP connect() failed. https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting\";}', 'no'),
(591, 'options_id_chuyen_gia', '23', 'no'),
(592, '_options_id_chuyen_gia', 'field_5ee0f4647c31b', 'no'),
(593, 'options_so_luong_hien_thi', '5', 'no'),
(594, '_options_so_luong_hien_thi', 'field_5ee0f4e17c31c', 'no'),
(633, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1592183960;s:15:\"version_checked\";s:5:\"5.4.2\";s:12:\"translations\";a:0:{}}', 'no'),
(636, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:21:\"hongyenhd97@gmail.com\";s:7:\"version\";s:5:\"5.4.2\";s:9:\"timestamp\";i:1591837293;}', 'no'),
(937, 'options_id_post_for_category', '27', 'no'),
(938, '_options_id_post_for_category', 'field_5ee1eb04e5a1a', 'no'),
(939, 'options_show_quantity_max', '6', 'no'),
(940, '_options_show_quantity_max', 'field_5ee1f131ceeff', 'no'),
(1088, '_site_transient_timeout_php_check_73ecd64509db505b6046b20394d377da', '1592530674', 'no'),
(1089, '_site_transient_php_check_73ecd64509db505b6046b20394d377da', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(1103, 'WPLANG', '', 'yes'),
(1104, 'new_admin_email', 'hongyenhd97@gmail.com', 'yes'),
(1755, 'category_children', 'a:3:{i:20;a:3:{i:0;i:7;i:1;i:8;i:2;i:32;}i:3;a:5:{i:0;i:26;i:1;i:28;i:2;i:29;i:3;i:30;i:4;i:31;}i:34;a:2:{i:0;i:37;i:1;i:38;}}', 'yes'),
(1801, 'options_id_category_cha', '20, 32, 7', 'no'),
(1802, '_options_id_category_cha', 'field_5ee4446cc89c0', 'no'),
(1803, 'options_', '', 'no'),
(1804, '_options_', 'field_5ee444fcc89c1', 'no'),
(1907, '_site_transient_timeout_theme_roots', '1592185758', 'no'),
(1908, '_site_transient_theme_roots', 'a:4:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:8:\"yentheme\";s:7:\"/themes\";}', 'no'),
(1910, '_transient_timeout_acf_plugin_updates', '1592356761', 'no'),
(1911, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.8.12\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.4.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.9\";}}', 'no'),
(1912, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1592183964;s:7:\"checked\";a:4:{s:14:\"twentynineteen\";s:3:\"1.5\";s:15:\"twentyseventeen\";s:3:\"2.3\";s:12:\"twentytwenty\";s:3:\"1.2\";s:8:\"yentheme\";s:3:\"1.2\";}s:8:\"response\";a:2:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.6.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.4.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(1913, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1592183965;s:7:\"checked\";a:6:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.9\";s:19:\"akismet/akismet.php\";s:5:\"4.1.5\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.5\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.9\";s:9:\"hello.php\";s:5:\"1.7.2\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"2.1.1\";}s:8:\"response\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.8.12\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.4.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=2279696\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"2.1.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.2.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2120094\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2120094\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_postmeta`
--

TRUNCATE TABLE `wp_postmeta`;
--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(6, 8, '_edit_last', '1'),
(7, 8, '_edit_lock', '1591804304:1'),
(8, 9, '_edit_last', '1'),
(9, 9, '_edit_lock', '1591600117:1'),
(10, 9, '_wp_page_template', 'page-about.php'),
(11, 11, '_edit_last', '1'),
(12, 11, '_edit_lock', '1591860499:1'),
(13, 11, '_wp_page_template', 'page-dinhduong.php'),
(14, 8, '_wp_page_template', 'default'),
(132, 2, '_edit_lock', '1591602486:1'),
(142, 30, '_edit_last', '1'),
(143, 30, '_edit_lock', '1591860482:1'),
(144, 30, '_wp_page_template', 'page-tieuduonglagi.php'),
(153, 33, '_edit_last', '1'),
(154, 33, '_wp_page_template', 'page-tientieuduong.php'),
(163, 33, '_edit_lock', '1591860491:1'),
(164, 36, '_edit_last', '1'),
(165, 36, '_wp_page_template', 'page-vandong.php'),
(174, 36, '_edit_lock', '1591860514:1'),
(175, 39, '_edit_last', '1'),
(176, 39, '_wp_page_template', 'page-dieutri.php'),
(185, 39, '_edit_lock', '1591860660:1'),
(186, 42, '_edit_last', '1'),
(187, 42, '_wp_page_template', 'default'),
(196, 42, '_edit_lock', '1591797322:1'),
(197, 45, '_edit_last', '1'),
(198, 45, '_wp_page_template', 'page-kiemsoatcannang.php'),
(207, 45, '_edit_lock', '1591860675:1'),
(208, 48, '_edit_last', '1'),
(209, 48, '_wp_page_template', 'page-nganhangcauhoi.php'),
(218, 48, '_edit_lock', '1591861255:1'),
(219, 51, '_edit_last', '1'),
(220, 51, '_wp_page_template', 'page-nghiencuuvatintuc.php'),
(229, 51, '_edit_lock', '1591847144:1'),
(230, 1, '_edit_lock', '1591674865:1'),
(231, 54, '_edit_last', '1'),
(232, 54, '_edit_lock', '1592014283:1'),
(233, 54, '_wp_page_template', 'default'),
(235, 56, '_edit_last', '1'),
(236, 56, '_edit_lock', '1591672536:1'),
(237, 57, '_wp_attached_file', '2020/06/1.jpg'),
(238, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:257;s:4:\"file\";s:13:\"2020/06/1.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"1-300x214.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(239, 56, '_wp_page_template', 'default'),
(242, 59, '_edit_last', '1'),
(243, 59, '_edit_lock', '1591672516:1'),
(244, 60, '_wp_attached_file', '2020/06/4.jpg'),
(245, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:257;s:4:\"file\";s:13:\"2020/06/4.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"4-300x214.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(246, 61, '_wp_attached_file', '2020/06/4-1.jpg'),
(247, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:257;s:4:\"file\";s:15:\"2020/06/4-1.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"4-1-300x214.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"4-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(248, 60, '_wp_attachment_image_alt', 'Các biến chứng của bệnh tiểu đường'),
(249, 59, '_wp_page_template', 'default'),
(255, 65, '_edit_last', '1'),
(256, 65, '_edit_lock', '1591672475:1'),
(257, 66, '_wp_attached_file', '2020/06/41.jpg'),
(258, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2100;s:6:\"height\";i:1400;s:4:\"file\";s:14:\"2020/06/41.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"41-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"41-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"41-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"41-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:16:\"41-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:16:\"41-2048x1365.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1365;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:15:\"41-1200x800.jpg\";s:5:\"width\";i:1200;s:6:\"height\";i:800;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:23:\"twentytwenty-fullscreen\";a:4:{s:4:\"file\";s:16:\"41-1980x1320.jpg\";s:5:\"width\";i:1980;s:6:\"height\";i:1320;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(259, 65, '_wp_page_template', 'default'),
(261, 68, '_edit_last', '1'),
(262, 68, '_edit_lock', '1591862264:1'),
(263, 69, '_wp_attached_file', '2020/06/52.jpg'),
(264, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:14:\"2020/06/52.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"52-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"52-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(265, 68, '_wp_page_template', 'default'),
(270, 71, '_edit_last', '1'),
(271, 71, '_edit_lock', '1591755966:1'),
(272, 72, '_wp_attached_file', '2020/06/6.png'),
(273, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:667;s:4:\"file\";s:13:\"2020/06/6.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"6-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:13:\"6-768x512.png\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(274, 71, '_wp_page_template', 'default'),
(276, 74, '_wp_attached_file', '2020/06/3.jpg'),
(277, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:257;s:4:\"file\";s:13:\"2020/06/3.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"3-300x214.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(279, 71, '_thumbnail_id', '72'),
(281, 68, '_thumbnail_id', '69'),
(283, 65, '_thumbnail_id', '66'),
(286, 59, '_thumbnail_id', '60'),
(288, 56, '_thumbnail_id', '57'),
(290, 54, '_thumbnail_id', '74'),
(293, 1, '_wp_trash_meta_status', 'publish'),
(294, 1, '_wp_trash_meta_time', '1591675010'),
(295, 1, '_wp_desired_post_slug', 'hello-world'),
(296, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(300, 84, '_edit_last', '1'),
(301, 84, '_edit_lock', '1591847388:1'),
(302, 85, '_wp_attached_file', '2020/06/HAB-tieuduong-1.jpg'),
(303, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:195;s:6:\"height\";i:118;s:4:\"file\";s:27:\"2020/06/HAB-tieuduong-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"HAB-tieuduong-1-150x118.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:118;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(304, 84, '_thumbnail_id', '85'),
(305, 84, '_wp_page_template', 'default'),
(307, 87, '_edit_last', '1'),
(308, 87, '_edit_lock', '1591932662:1'),
(309, 88, '_wp_attached_file', '2020/06/1-1.jpg'),
(310, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:195;s:6:\"height\";i:118;s:4:\"file\";s:15:\"2020/06/1-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"1-1-150x118.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:118;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(311, 87, '_thumbnail_id', '85'),
(312, 87, '_wp_page_template', 'default'),
(314, 90, '_edit_last', '1'),
(315, 90, '_edit_lock', '1592188149:1'),
(316, 91, '_wp_attached_file', '2020/06/HAB-tieuduong.jpg'),
(317, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:195;s:6:\"height\";i:118;s:4:\"file\";s:25:\"2020/06/HAB-tieuduong.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"HAB-tieuduong-150x118.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:118;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(318, 90, '_thumbnail_id', '88'),
(319, 90, '_wp_page_template', 'default'),
(325, 97, '_edit_last', '1'),
(326, 97, '_edit_lock', '1591763227:1'),
(327, 101, '_edit_last', '1'),
(328, 101, '_edit_lock', '1591768547:1'),
(337, 110, '_edit_last', '1'),
(338, 110, '_edit_lock', '1592190055:1'),
(339, 111, '_wp_attached_file', '2020/06/Tong-quan-kien-thuc-ve-benh-tieu-duong-02.jpg'),
(340, 111, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:900;s:6:\"height\";i:599;s:4:\"file\";s:53:\"2020/06/Tong-quan-kien-thuc-ve-benh-tieu-duong-02.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:53:\"Tong-quan-kien-thuc-ve-benh-tieu-duong-02-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:53:\"Tong-quan-kien-thuc-ve-benh-tieu-duong-02-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:53:\"Tong-quan-kien-thuc-ve-benh-tieu-duong-02-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(341, 110, '_thumbnail_id', '111'),
(342, 110, '_wp_page_template', 'default'),
(345, 113, '_edit_last', '1'),
(346, 113, '_edit_lock', '1591963061:1'),
(347, 113, '_thumbnail_id', '88'),
(348, 113, '_wp_page_template', 'default'),
(351, 115, '_edit_last', '1'),
(352, 115, '_wp_page_template', 'default'),
(354, 115, '_edit_lock', '1592187769:1'),
(355, 117, '_wp_attached_file', '2020/06/HAB-tieuduong-2.jpg'),
(356, 117, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:195;s:6:\"height\";i:118;s:4:\"file\";s:27:\"2020/06/HAB-tieuduong-2.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"HAB-tieuduong-2-150x118.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:118;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(357, 115, '_thumbnail_id', '117'),
(360, 119, '_edit_last', '1'),
(361, 119, '_edit_lock', '1592187753:1'),
(362, 119, '_thumbnail_id', '69'),
(363, 119, '_wp_page_template', 'default'),
(365, 121, '_edit_last', '1'),
(366, 121, '_edit_lock', '1591770895:1'),
(367, 121, '_thumbnail_id', '85'),
(368, 121, '_wp_page_template', 'default'),
(370, 123, '_edit_last', '1'),
(371, 123, '_edit_lock', '1592189073:1'),
(373, 127, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(374, 127, '_mail', 'a:8:{s:7:\"subject\";s:26:\"Yen Theme \"[your-subject]\"\";s:6:\"sender\";s:33:\"Yen Theme <hongyenhd97@gmail.com>\";s:4:\"body\";s:178:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\";s:9:\"recipient\";s:21:\"hongyenhd97@gmail.com\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(375, 127, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:26:\"Yen Theme \"[your-subject]\"\";s:6:\"sender\";s:33:\"Yen Theme <hongyenhd97@gmail.com>\";s:4:\"body\";s:120:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:31:\"Reply-To: hongyenhd97@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(376, 127, '_messages', 'a:8:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";}'),
(377, 127, '_additional_settings', NULL),
(378, 127, '_locale', 'en_US'),
(391, 130, '_form', '<div class=\"col-xs-12 col-sm-6\">\n        <div class=\"form-group\" id=\"input-name\">\n            [text* your-name placeholder=\'Họ và tên\']\n        </div>\n    </div>\n    <div class=\"col-xs-12 col-sm-6\">\n        <div class=\"form-group\" id=\"input-phone\">\n          [tel* tel-966 placeholder=\"Số điện thoại\"]\n        </div>\n    </div>\n    <div class=\"col-xs-12 col-sm-6\">\n        <div class=\"form-group\" id=\"input-email\">\n            [email* your-email placeholder=\"Email\"]\n        </div>\n    </div>\n    <div class=\"col-xs-12 col-sm-6\">\n        <div class=\"form-group\" id=\"input-content\">\n            [text your-message placeholder=\"Nội dung cần tư vấn\"]\n        </div>\n    </div>\n    <div class=\"col-sm-6\">\n        <div class=\"text-center hotline\">\n            <b>HOTLINE: <a href=\"tel:088.885.4313\">088.885.4313</a></b>\n        </div>\n    </div>\n    <div class=\"col-sm-6\">\n        <div class=\"btn-register\">\n[submit \"Đăng ký ngay\"]\n        </div>\n    </div>'),
(392, 130, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:26:\"Yen Theme \"[your-subject]\"\";s:6:\"sender\";s:33:\"Yen Theme <hongyenhd97@gmail.com>\";s:9:\"recipient\";s:21:\"hongyenhd97@gmail.com\";s:4:\"body\";s:178:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:1;s:13:\"exclude_blank\";b:0;}'),
(393, 130, '_mail_2', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:26:\"Yen Theme \"[your-subject]\"\";s:6:\"sender\";s:33:\"Yen Theme <hongyenhd97@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:120:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\";s:18:\"additional_headers\";s:31:\"Reply-To: hongyenhd97@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(394, 130, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(395, 130, '_additional_settings', ''),
(396, 130, '_locale', 'en_US'),
(398, 131, '_menu_item_type', 'taxonomy'),
(399, 131, '_menu_item_menu_item_parent', '0'),
(400, 131, '_menu_item_object_id', '3'),
(401, 131, '_menu_item_object', 'category'),
(402, 131, '_menu_item_target', ''),
(403, 131, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(404, 131, '_menu_item_xfn', ''),
(405, 131, '_menu_item_url', ''),
(406, 131, '_menu_item_orphaned', '1591785076'),
(425, 134, '_menu_item_type', 'post_type'),
(426, 134, '_menu_item_menu_item_parent', '0'),
(427, 134, '_menu_item_object_id', '113'),
(428, 134, '_menu_item_object', 'post'),
(429, 134, '_menu_item_target', ''),
(430, 134, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(431, 134, '_menu_item_xfn', ''),
(432, 134, '_menu_item_url', ''),
(433, 134, '_menu_item_orphaned', '1591785976'),
(480, 140, '_edit_last', '1'),
(481, 140, '_wp_page_template', 'default'),
(490, 140, '_edit_lock', '1591861670:1'),
(491, 143, '_edit_last', '1'),
(492, 143, '_edit_lock', '1591804364:1'),
(493, 143, '_wp_page_template', 'default'),
(494, 145, '_menu_item_type', 'post_type'),
(495, 145, '_menu_item_menu_item_parent', '0'),
(496, 145, '_menu_item_object_id', '143'),
(497, 145, '_menu_item_object', 'page'),
(498, 145, '_menu_item_target', ''),
(499, 145, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(500, 145, '_menu_item_xfn', ''),
(501, 145, '_menu_item_url', ''),
(502, 145, '_menu_item_orphaned', '1591791773'),
(503, 42, '_wp_trash_meta_status', 'publish'),
(504, 42, '_wp_trash_meta_time', '1591797467'),
(505, 42, '_wp_desired_post_slug', 'goc-chuyen-gia-2'),
(515, 147, '_menu_item_type', 'custom'),
(516, 147, '_menu_item_menu_item_parent', '0'),
(517, 147, '_menu_item_object_id', '147'),
(518, 147, '_menu_item_object', 'custom'),
(519, 147, '_menu_item_target', ''),
(520, 147, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(521, 147, '_menu_item_xfn', ''),
(522, 147, '_menu_item_url', 'http://localhost:8080/yentheme/'),
(523, 147, '_menu_item_orphaned', '1591797685'),
(677, 166, '_edit_last', '1'),
(678, 166, '_edit_lock', '1591800535:1'),
(679, 168, '_edit_last', '1'),
(680, 168, '_edit_lock', '1591861609:1'),
(681, 168, '_wp_page_template', 'page-hoidongchuyengia.php'),
(690, 171, '_edit_last', '1'),
(691, 171, '_edit_lock', '1592015437:1'),
(692, 171, '_thumbnail_id', '199'),
(693, 171, '_wp_page_template', 'default'),
(695, 166, '_wp_trash_meta_status', 'draft'),
(696, 166, '_wp_trash_meta_time', '1591800679'),
(697, 166, '_wp_desired_post_slug', ''),
(698, 174, '_edit_last', '1'),
(699, 174, '_edit_lock', '1591802222:1'),
(701, 8, '_wp_trash_meta_status', 'publish'),
(702, 8, '_wp_trash_meta_time', '1591804449'),
(703, 8, '_wp_desired_post_slug', 'goc-chuyen-gia'),
(704, 143, '_wp_trash_meta_status', 'publish'),
(705, 143, '_wp_trash_meta_time', '1591804509'),
(706, 143, '_wp_desired_post_slug', 'chien-luoc-kieng-3-chan'),
(707, 178, '_menu_item_type', 'post_type'),
(708, 178, '_menu_item_menu_item_parent', '0'),
(709, 178, '_menu_item_object_id', '168'),
(710, 178, '_menu_item_object', 'page'),
(711, 178, '_menu_item_target', ''),
(712, 178, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(713, 178, '_menu_item_xfn', ''),
(714, 178, '_menu_item_url', ''),
(715, 178, '_menu_item_orphaned', '1591804579'),
(716, 179, '_edit_last', '1'),
(717, 179, '_edit_lock', '1591837590:1'),
(718, 180, '_edit_last', '1'),
(719, 180, '_edit_lock', '1591804674:1'),
(720, 180, '_thumbnail_id', '74'),
(721, 180, '_wp_page_template', 'page-chuyengiadinhduong.php'),
(730, 183, '_edit_last', '1'),
(731, 183, '_edit_lock', '1591804698:1'),
(732, 183, '_thumbnail_id', '57'),
(733, 183, '_wp_page_template', 'page-chuyengianvandong.php'),
(742, 186, '_menu_item_type', 'post_type'),
(743, 186, '_menu_item_menu_item_parent', '0'),
(744, 186, '_menu_item_object_id', '183'),
(745, 186, '_menu_item_object', 'page'),
(746, 186, '_menu_item_target', ''),
(747, 186, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(748, 186, '_menu_item_xfn', ''),
(749, 186, '_menu_item_url', ''),
(750, 186, '_menu_item_orphaned', '1591804853'),
(751, 187, '_menu_item_type', 'post_type'),
(752, 187, '_menu_item_menu_item_parent', '0'),
(753, 187, '_menu_item_object_id', '180'),
(754, 187, '_menu_item_object', 'page'),
(755, 187, '_menu_item_target', ''),
(756, 187, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(757, 187, '_menu_item_xfn', ''),
(758, 187, '_menu_item_url', ''),
(759, 187, '_menu_item_orphaned', '1591804853'),
(841, 179, '_wp_trash_meta_status', 'draft'),
(842, 179, '_wp_trash_meta_time', '1591837735'),
(843, 179, '_wp_desired_post_slug', ''),
(844, 199, '_wp_attached_file', '2020/06/quang7.png'),
(845, 199, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:365;s:6:\"height\";i:365;s:4:\"file\";s:18:\"2020/06/quang7.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"quang7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"quang7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(847, 201, '_edit_last', '1'),
(848, 201, '_edit_lock', '1592015181:1'),
(849, 202, '_wp_attached_file', '2020/06/bichnga.png'),
(850, 202, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:365;s:6:\"height\";i:365;s:4:\"file\";s:19:\"2020/06/bichnga.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"bichnga-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"bichnga-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(851, 201, '_thumbnail_id', '202'),
(852, 201, '_wp_page_template', 'default'),
(855, 205, '_edit_last', '1'),
(856, 205, '_edit_lock', '1592015151:1'),
(857, 206, '_wp_attached_file', '2020/06/dieuvan.png'),
(858, 206, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:365;s:6:\"height\";i:365;s:4:\"file\";s:19:\"2020/06/dieuvan.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"dieuvan-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"dieuvan-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(859, 205, '_thumbnail_id', '206'),
(860, 205, '_wp_page_template', 'default'),
(862, 208, '_edit_last', '1'),
(863, 208, '_edit_lock', '1591859961:1'),
(864, 208, '_wp_page_template', 'default'),
(866, 210, '_wp_attached_file', '2020/06/quang.png'),
(867, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:365;s:6:\"height\";i:365;s:4:\"file\";s:17:\"2020/06/quang.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"quang-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"quang-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(868, 208, '_thumbnail_id', '210'),
(871, 213, '_edit_last', '1'),
(872, 213, '_edit_lock', '1592185273:1'),
(873, 213, '_thumbnail_id', '85'),
(874, 213, '_wp_page_template', 'default'),
(896, 219, '_edit_last', '1'),
(897, 219, '_edit_lock', '1591861486:1'),
(898, 219, '_wp_page_template', 'page-chienluockieng3chan.php'),
(907, 3, '_edit_lock', '1591861623:1'),
(908, 140, '_wp_trash_meta_status', 'publish'),
(909, 140, '_wp_trash_meta_time', '1591861814'),
(910, 140, '_wp_desired_post_slug', 'trang-chu'),
(921, 223, '_edit_last', '1'),
(922, 223, '_edit_lock', '1592189365:1'),
(923, 226, '_edit_last', '1'),
(924, 226, '_edit_lock', '1591960255:1'),
(925, 226, '_thumbnail_id', '57'),
(926, 226, '_wp_page_template', 'default'),
(928, 228, '_edit_last', '1'),
(929, 228, '_edit_lock', '1591867806:1'),
(930, 228, '_thumbnail_id', '60'),
(931, 228, '_wp_page_template', 'default'),
(933, 230, '_edit_last', '1'),
(934, 230, '_edit_lock', '1592012576:1'),
(935, 230, '_thumbnail_id', '74'),
(936, 230, '_wp_page_template', 'default'),
(938, 232, '_edit_last', '1'),
(939, 232, '_edit_lock', '1591963104:1'),
(940, 232, '_thumbnail_id', '66'),
(941, 232, '_wp_page_template', 'default'),
(943, 234, '_edit_last', '1'),
(944, 234, '_edit_lock', '1591963090:1'),
(945, 234, '_thumbnail_id', '69'),
(946, 234, '_wp_page_template', 'default'),
(948, 236, '_edit_last', '1'),
(949, 236, '_edit_lock', '1591963076:1'),
(950, 236, '_thumbnail_id', '72'),
(951, 236, '_wp_page_template', 'default'),
(971, 240, '_menu_item_type', 'taxonomy'),
(972, 240, '_menu_item_menu_item_parent', '0'),
(973, 240, '_menu_item_object_id', '32'),
(974, 240, '_menu_item_object', 'category'),
(975, 240, '_menu_item_target', ''),
(976, 240, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(977, 240, '_menu_item_xfn', ''),
(978, 240, '_menu_item_url', ''),
(980, 241, '_menu_item_type', 'taxonomy'),
(981, 241, '_menu_item_menu_item_parent', '0'),
(982, 241, '_menu_item_object_id', '8'),
(983, 241, '_menu_item_object', 'category'),
(984, 241, '_menu_item_target', ''),
(985, 241, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(986, 241, '_menu_item_xfn', ''),
(987, 241, '_menu_item_url', ''),
(989, 242, '_menu_item_type', 'taxonomy'),
(990, 242, '_menu_item_menu_item_parent', '0'),
(991, 242, '_menu_item_object_id', '34'),
(992, 242, '_menu_item_object', 'category'),
(993, 242, '_menu_item_target', ''),
(994, 242, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(995, 242, '_menu_item_xfn', ''),
(996, 242, '_menu_item_url', ''),
(998, 243, '_menu_item_type', 'taxonomy'),
(999, 243, '_menu_item_menu_item_parent', '0'),
(1000, 243, '_menu_item_object_id', '11'),
(1001, 243, '_menu_item_object', 'category'),
(1002, 243, '_menu_item_target', ''),
(1003, 243, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1004, 243, '_menu_item_xfn', ''),
(1005, 243, '_menu_item_url', ''),
(1007, 244, '_menu_item_type', 'taxonomy'),
(1008, 244, '_menu_item_menu_item_parent', '0'),
(1009, 244, '_menu_item_object_id', '3'),
(1010, 244, '_menu_item_object', 'category'),
(1011, 244, '_menu_item_target', ''),
(1012, 244, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1013, 244, '_menu_item_xfn', ''),
(1014, 244, '_menu_item_url', ''),
(1016, 245, '_menu_item_type', 'taxonomy'),
(1017, 245, '_menu_item_menu_item_parent', '244'),
(1018, 245, '_menu_item_object_id', '26'),
(1019, 245, '_menu_item_object', 'category'),
(1020, 245, '_menu_item_target', ''),
(1021, 245, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1022, 245, '_menu_item_xfn', ''),
(1023, 245, '_menu_item_url', ''),
(1025, 246, '_menu_item_type', 'taxonomy'),
(1026, 246, '_menu_item_menu_item_parent', '244'),
(1027, 246, '_menu_item_object_id', '29'),
(1028, 246, '_menu_item_object', 'category'),
(1029, 246, '_menu_item_target', ''),
(1030, 246, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1031, 246, '_menu_item_xfn', ''),
(1032, 246, '_menu_item_url', ''),
(1034, 247, '_menu_item_type', 'taxonomy'),
(1035, 247, '_menu_item_menu_item_parent', '244'),
(1036, 247, '_menu_item_object_id', '28'),
(1037, 247, '_menu_item_object', 'category'),
(1038, 247, '_menu_item_target', ''),
(1039, 247, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1040, 247, '_menu_item_xfn', ''),
(1041, 247, '_menu_item_url', ''),
(1043, 248, '_menu_item_type', 'taxonomy'),
(1044, 248, '_menu_item_menu_item_parent', '244'),
(1045, 248, '_menu_item_object_id', '30'),
(1046, 248, '_menu_item_object', 'category'),
(1047, 248, '_menu_item_target', ''),
(1048, 248, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1049, 248, '_menu_item_xfn', ''),
(1050, 248, '_menu_item_url', ''),
(1052, 249, '_menu_item_type', 'taxonomy'),
(1053, 249, '_menu_item_menu_item_parent', '244'),
(1054, 249, '_menu_item_object_id', '31'),
(1055, 249, '_menu_item_object', 'category'),
(1056, 249, '_menu_item_target', ''),
(1057, 249, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1058, 249, '_menu_item_xfn', ''),
(1059, 249, '_menu_item_url', ''),
(1061, 250, '_menu_item_type', 'taxonomy'),
(1062, 250, '_menu_item_menu_item_parent', '0'),
(1063, 250, '_menu_item_object_id', '12'),
(1064, 250, '_menu_item_object', 'category'),
(1065, 250, '_menu_item_target', ''),
(1066, 250, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1067, 250, '_menu_item_xfn', ''),
(1068, 250, '_menu_item_url', ''),
(1088, 253, '_menu_item_type', 'taxonomy'),
(1089, 253, '_menu_item_menu_item_parent', '0'),
(1090, 253, '_menu_item_object_id', '7'),
(1091, 253, '_menu_item_object', 'category'),
(1092, 253, '_menu_item_target', ''),
(1093, 253, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1094, 253, '_menu_item_xfn', ''),
(1095, 253, '_menu_item_url', ''),
(1099, 90, '_wp_old_slug', 'tieu-duong-tuyp-1'),
(1100, 87, '_wp_trash_meta_status', 'publish'),
(1101, 87, '_wp_trash_meta_time', '1591932807'),
(1102, 87, '_wp_desired_post_slug', 'tong-quan-kien-thuc-ve-benh-tieu-duong'),
(1106, 254, '_menu_item_type', 'custom'),
(1107, 254, '_menu_item_menu_item_parent', '0'),
(1108, 254, '_menu_item_object_id', '254'),
(1109, 254, '_menu_item_object', 'custom'),
(1110, 254, '_menu_item_target', ''),
(1111, 254, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1112, 254, '_menu_item_xfn', ''),
(1113, 254, '_menu_item_url', '/'),
(1131, 54, '_wp_old_slug', 'tieu-duong-la-gi'),
(1132, 263, '_menu_item_type', 'taxonomy'),
(1133, 263, '_menu_item_menu_item_parent', '0'),
(1134, 263, '_menu_item_object_id', '36'),
(1135, 263, '_menu_item_object', 'category'),
(1136, 263, '_menu_item_target', ''),
(1137, 263, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1138, 263, '_menu_item_xfn', ''),
(1139, 263, '_menu_item_url', ''),
(1144, 264, '_menu_item_type', 'taxonomy'),
(1145, 264, '_menu_item_menu_item_parent', '242'),
(1146, 264, '_menu_item_object_id', '37'),
(1147, 264, '_menu_item_object', 'category'),
(1148, 264, '_menu_item_target', ''),
(1149, 264, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1150, 264, '_menu_item_xfn', ''),
(1151, 264, '_menu_item_url', ''),
(1153, 265, '_menu_item_type', 'taxonomy'),
(1154, 265, '_menu_item_menu_item_parent', '242'),
(1155, 265, '_menu_item_object_id', '38'),
(1156, 265, '_menu_item_object', 'category'),
(1157, 265, '_menu_item_target', ''),
(1158, 265, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1159, 265, '_menu_item_xfn', ''),
(1160, 265, '_menu_item_url', ''),
(1163, 266, '_edit_last', '1'),
(1164, 266, '_edit_lock', '1592189083:1'),
(1168, 269, '_edit_last', '1'),
(1169, 269, '_edit_lock', '1592188273:1'),
(1170, 269, '_wp_page_template', 'default'),
(1172, 269, '_thumbnail_id', '72'),
(1174, 271, '_edit_last', '1'),
(1175, 271, '_edit_lock', '1592189033:1'),
(1176, 271, '_thumbnail_id', '88'),
(1177, 271, '_wp_page_template', 'default'),
(1179, 123, '_wp_trash_meta_status', 'publish'),
(1180, 123, '_wp_trash_meta_time', '1592189218'),
(1181, 123, '_wp_desired_post_slug', 'group_5ee071e6eacb5'),
(1182, 124, '_wp_trash_meta_status', 'publish'),
(1183, 124, '_wp_trash_meta_time', '1592189218'),
(1184, 124, '_wp_desired_post_slug', 'field_5ee0720fdc1cb'),
(1185, 125, '_wp_trash_meta_status', 'publish'),
(1186, 125, '_wp_trash_meta_time', '1592189218'),
(1187, 125, '_wp_desired_post_slug', 'field_5ee07247dc1cc'),
(1188, 266, '_wp_trash_meta_status', 'publish'),
(1189, 266, '_wp_trash_meta_time', '1592189227'),
(1190, 266, '_wp_desired_post_slug', 'group_5ee44442d2624'),
(1191, 267, '_wp_trash_meta_status', 'publish'),
(1192, 267, '_wp_trash_meta_time', '1592189227'),
(1193, 267, '_wp_desired_post_slug', 'field_5ee4446cc89c0'),
(1194, 268, '_wp_trash_meta_status', 'publish'),
(1195, 268, '_wp_trash_meta_time', '1592189227'),
(1196, 268, '_wp_desired_post_slug', 'field_5ee444fcc89c1'),
(1197, 273, '_edit_last', '1'),
(1198, 273, '_edit_lock', '1592190026:1'),
(1199, 273, '_thumbnail_id', '117'),
(1200, 273, '_wp_page_template', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_posts`
--

TRUNCATE TABLE `wp_posts`;
--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-06-05 09:15:59', '2020-06-05 09:15:59', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2020-06-09 03:56:50', '2020-06-09 03:56:50', '', 0, 'http://localhost:8080/yentheme/?p=1', 0, 'post', '', 1),
(2, 1, '2020-06-05 09:15:59', '2020-06-05 09:15:59', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8080/yentheme/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-06-05 09:15:59', '2020-06-05 09:15:59', '', 0, 'http://localhost:8080/yentheme/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-06-05 09:15:59', '2020-06-05 09:15:59', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8080/yentheme.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-06-05 09:15:59', '2020-06-05 09:15:59', '', 0, 'http://localhost:8080/yentheme/?page_id=3', 0, 'page', '', 0),
(8, 1, '2020-06-08 07:12:09', '2020-06-08 07:12:09', 'Góc chuyên gia', 'Góc chuyên gia', '', 'trash', 'closed', 'closed', '', 'goc-chuyen-gia__trashed', '', '', '2020-06-10 15:54:09', '2020-06-10 15:54:09', '', 0, 'http://localhost:8080/yentheme/?page_id=8', 0, 'page', '', 0),
(9, 1, '2020-06-06 04:32:38', '2020-06-06 04:32:38', '', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2020-06-06 04:32:38', '2020-06-06 04:32:38', '', 0, 'http://localhost:8080/yentheme/?page_id=9', 0, 'page', '', 0),
(10, 1, '2020-06-06 04:32:38', '2020-06-06 04:32:38', '', 'About', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2020-06-06 04:32:38', '2020-06-06 04:32:38', '', 9, 'http://localhost:8080/yentheme/2020/06/06/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2020-06-08 07:10:09', '2020-06-08 07:10:09', '', 'Dinh dưỡng', '', 'publish', 'closed', 'closed', '', 'dinh-duong', '', '', '2020-06-10 14:42:07', '2020-06-10 14:42:07', '', 0, 'http://localhost:8080/yentheme/?page_id=11', 0, 'page', '', 0),
(12, 1, '2020-06-08 07:10:09', '2020-06-08 07:10:09', 'Dinh dưỡng', 'Dinh dưỡng', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2020-06-08 07:10:09', '2020-06-08 07:10:09', '', 11, 'http://localhost:8080/yentheme/2020/06/08/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2020-06-08 07:12:09', '2020-06-08 07:12:09', 'Góc chuyên gia', 'About us', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2020-06-08 07:12:09', '2020-06-08 07:12:09', '', 8, 'http://localhost:8080/yentheme/2020/06/08/8-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2020-06-08 07:36:32', '2020-06-08 07:36:32', 'Góc chuyên gia', 'Góc chuyên gia', '', 'inherit', 'closed', 'closed', '', '8-autosave-v1', '', '', '2020-06-08 07:36:32', '2020-06-08 07:36:32', '', 8, 'http://localhost:8080/yentheme/2020/06/08/8-autosave-v1/', 0, 'revision', '', 0),
(29, 1, '2020-06-08 07:53:39', '2020-06-08 07:53:39', 'Góc chuyên gia', 'Góc chuyên gia', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2020-06-08 07:53:39', '2020-06-08 07:53:39', '', 8, 'http://localhost:8080/yentheme/2020/06/08/8-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2020-06-08 07:54:30', '2020-06-08 07:54:30', '', 'Tiểu đường là gì', '', 'publish', 'closed', 'closed', '', 'tieu-duong-la-gi', '', '', '2020-06-11 07:26:45', '2020-06-11 07:26:45', '', 0, 'http://localhost:8080/yentheme/?page_id=30', 0, 'page', '', 0),
(32, 1, '2020-06-08 07:54:30', '2020-06-08 07:54:30', '', 'Tiểu đường là gì', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-06-08 07:54:30', '2020-06-08 07:54:30', '', 30, 'http://localhost:8080/yentheme/2020/06/08/30-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2020-06-08 07:55:01', '2020-06-08 07:55:01', '', 'Tiền tiểu đường', '', 'publish', 'closed', 'closed', '', 'tien-tieu-duong', '', '', '2020-06-11 07:25:16', '2020-06-11 07:25:16', '', 0, 'http://localhost:8080/yentheme/?page_id=33', 0, 'page', '', 0),
(35, 1, '2020-06-08 07:55:01', '2020-06-08 07:55:01', '', 'Tiền tiểu đường', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2020-06-08 07:55:01', '2020-06-08 07:55:01', '', 33, 'http://localhost:8080/yentheme/2020/06/08/33-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2020-06-08 07:55:28', '2020-06-08 07:55:28', '', 'Vận động', '', 'publish', 'closed', 'closed', '', 'van-dong', '', '', '2020-06-11 07:30:53', '2020-06-11 07:30:53', '', 0, 'http://localhost:8080/yentheme/?page_id=36', 0, 'page', '', 0),
(38, 1, '2020-06-08 07:55:28', '2020-06-08 07:55:28', '', 'Vận động', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2020-06-08 07:55:28', '2020-06-08 07:55:28', '', 36, 'http://localhost:8080/yentheme/2020/06/08/36-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2020-06-08 07:55:47', '2020-06-08 07:55:47', '', 'Điều trị', '', 'publish', 'closed', 'closed', '', 'dieu-tri', '', '', '2020-06-11 07:31:06', '2020-06-11 07:31:06', '', 0, 'http://localhost:8080/yentheme/?page_id=39', 0, 'page', '', 0),
(41, 1, '2020-06-08 07:55:47', '2020-06-08 07:55:47', '', 'Điều trị', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-06-08 07:55:47', '2020-06-08 07:55:47', '', 39, 'http://localhost:8080/yentheme/2020/06/08/39-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2020-06-08 07:55:58', '2020-06-08 07:55:58', '', 'Góc chuyên gia', '', 'trash', 'closed', 'closed', '', 'goc-chuyen-gia-2__trashed', '', '', '2020-06-10 13:57:47', '2020-06-10 13:57:47', '', 0, 'http://localhost:8080/yentheme/?page_id=42', 0, 'page', '', 0),
(44, 1, '2020-06-08 07:55:58', '2020-06-08 07:55:58', '', 'Góc chuyên gia', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2020-06-08 07:55:58', '2020-06-08 07:55:58', '', 42, 'http://localhost:8080/yentheme/2020/06/08/42-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-06-08 07:56:15', '2020-06-08 07:56:15', '', 'Kiểm soát cân nặng', '', 'publish', 'closed', 'closed', '', 'kiem-soat-can-nang', '', '', '2020-06-11 07:33:38', '2020-06-11 07:33:38', '', 0, 'http://localhost:8080/yentheme/?page_id=45', 0, 'page', '', 0),
(47, 1, '2020-06-08 07:56:15', '2020-06-08 07:56:15', '', 'Kiểm soát cân nặng', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2020-06-08 07:56:15', '2020-06-08 07:56:15', '', 45, 'http://localhost:8080/yentheme/2020/06/08/45-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2020-06-08 07:56:26', '2020-06-08 07:56:26', '', 'Ngân hàng câu hỏi', '', 'publish', 'closed', 'closed', '', 'ngan-hang-cau-hoi', '', '', '2020-06-11 07:33:48', '2020-06-11 07:33:48', '', 0, 'http://localhost:8080/yentheme/?page_id=48', 0, 'page', '', 0),
(50, 1, '2020-06-08 07:56:26', '2020-06-08 07:56:26', '', 'Ngân hàng câu hỏi', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2020-06-08 07:56:26', '2020-06-08 07:56:26', '', 48, 'http://localhost:8080/yentheme/2020/06/08/48-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2020-06-08 07:56:38', '2020-06-08 07:56:38', '', 'Nghiên cứu & tin tức', '', 'publish', 'closed', 'closed', '', 'kien-thuc', '', '', '2020-06-11 03:35:11', '2020-06-11 03:35:11', '', 0, 'http://localhost:8080/yentheme/?page_id=51', 0, 'page', '', 0),
(53, 1, '2020-06-08 07:56:38', '2020-06-08 07:56:38', '', 'Nghiên cứu và tin tức', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-06-08 07:56:38', '2020-06-08 07:56:38', '', 51, 'http://localhost:8080/yentheme/2020/06/08/51-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2020-06-08 08:03:45', '2020-06-08 08:03:45', 'Bạn cần chăm sóc bản thân như thế nào?', 'Chăm sóc bản thân', '', 'publish', 'closed', 'closed', '', 'cham-soc-ban-than-2', '', '', '2020-06-13 01:46:19', '2020-06-13 01:46:19', '', 0, 'http://localhost:8080/yentheme/?p=54', 0, 'post', '', 0),
(55, 1, '2020-06-08 08:03:45', '2020-06-08 08:03:45', 'Tả lười câu hỏi tiểu đường là gì???', 'Tiểu đường là gì', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2020-06-08 08:03:45', '2020-06-08 08:03:45', '', 54, 'http://localhost:8080/yentheme/2020/06/08/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2020-06-08 11:14:52', '2020-06-08 11:14:52', 'Tiến triển', 'Tiến triển của bệnh tiểu đường', '', 'publish', 'open', 'open', '', 'tien-trien-cua-benh-tieu-duong', '', '', '2020-06-09 03:17:58', '2020-06-09 03:17:58', '', 0, 'http://localhost:8080/yentheme/?p=56', 0, 'post', '', 0),
(57, 1, '2020-06-08 11:13:21', '2020-06-08 11:13:21', 'Tiến triển của bệnh tiểu đường (d)', '1', 'Tiến triển của bệnh tiểu đường (c)', 'inherit', 'open', 'closed', '', '1', '', '', '2020-06-08 11:14:30', '2020-06-08 11:14:30', '', 56, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/1.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2020-06-08 11:14:52', '2020-06-08 11:14:52', '[caption id=\"attachment_57\" align=\"alignnone\" width=\"300\"]<img src=\"http://localhost:8080/yentheme/wp-content/uploads/2020/06/1-300x214.jpg\" alt=\"\" width=\"300\" height=\"214\" class=\"size-medium wp-image-57\" /> Tiến triển của bệnh tiểu đường (c)[/caption]', 'Tiến triển của bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2020-06-08 11:14:52', '2020-06-08 11:14:52', '', 56, 'http://localhost:8080/yentheme/2020/06/08/56-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2020-06-08 11:17:57', '2020-06-08 11:17:57', 'Các biến chứng', 'Các biến chứng của bệnh tiểu đường', '', 'publish', 'open', 'open', '', 'cac-bien-chung-cua-benh-tieu-duong', '', '', '2020-06-09 03:17:25', '2020-06-09 03:17:25', '', 0, 'http://localhost:8080/yentheme/?p=59', 0, 'post', '', 0),
(60, 1, '2020-06-08 11:16:43', '2020-06-08 11:16:43', '', '4', 'Các biến chứng của bệnh tiểu đường', 'inherit', 'open', 'closed', '', '4', '', '', '2020-06-08 11:17:55', '2020-06-08 11:17:55', '', 59, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/4.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2020-06-08 11:17:01', '2020-06-08 11:17:01', '', '4', '', 'inherit', 'open', 'closed', '', '4-2', '', '', '2020-06-08 11:17:01', '2020-06-08 11:17:01', '', 59, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/4-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(62, 1, '2020-06-08 11:17:57', '2020-06-08 11:17:57', '[caption id=\"attachment_60\" align=\"alignnone\" width=\"300\"]<img src=\"http://localhost:8080/yentheme/wp-content/uploads/2020/06/4-300x214.jpg\" alt=\"Các biến chứng của bệnh tiểu đường\" width=\"300\" height=\"214\" class=\"size-medium wp-image-60\" /> Các biến chứng của bệnh tiểu đường[/caption]', 'Các biến chứng của bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-06-08 11:17:57', '2020-06-08 11:17:57', '', 59, 'http://localhost:8080/yentheme/2020/06/08/59-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2020-06-09 02:28:49', '2020-06-09 02:28:49', 'Bạn cần chăm sóc bản thân như thế nào?', 'Chăm sóc bản thân', '', 'inherit', 'closed', 'closed', '', '54-autosave-v1', '', '', '2020-06-09 02:28:49', '2020-06-09 02:28:49', '', 54, 'http://localhost:8080/yentheme/2020/06/09/54-autosave-v1/', 0, 'revision', '', 0),
(64, 1, '2020-06-09 02:29:04', '2020-06-09 02:29:04', 'Bạn cần chăm sóc bản thân như thế nào?', 'Chăm sóc bản thân', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2020-06-09 02:29:04', '2020-06-09 02:29:04', '', 54, 'http://localhost:8080/yentheme/2020/06/09/54-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2020-06-09 02:30:05', '2020-06-09 02:30:05', 'Tự theo dõi và kiểm soát bệnh tiều đường tại nhà an toàn', 'Theo dõi và kiểm soát bệnh tiểu đường', '', 'publish', 'open', 'open', '', 'theo-doi-va-kiem-soat-benh-tieu-duong', '', '', '2020-06-09 03:16:57', '2020-06-09 03:16:57', '', 0, 'http://localhost:8080/yentheme/?p=65', 0, 'post', '', 0),
(66, 1, '2020-06-09 02:29:41', '2020-06-09 02:29:41', '', '4(1)', '', 'inherit', 'open', 'closed', '', '41', '', '', '2020-06-09 02:29:41', '2020-06-09 02:29:41', '', 65, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/41.jpg', 0, 'attachment', 'image/jpeg', 0),
(67, 1, '2020-06-09 02:30:05', '2020-06-09 02:30:05', '<img src=\"http://localhost:8080/yentheme/wp-content/uploads/2020/06/41-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" class=\"alignnone size-medium wp-image-66\" />', 'Theo dõi và kiểm soát bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2020-06-09 02:30:05', '2020-06-09 02:30:05', '', 65, 'http://localhost:8080/yentheme/2020/06/09/65-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2020-06-09 02:31:01', '2020-06-09 02:31:01', 'Tiểu đường và stress thật sự ko tốt.', 'Tiểu đường và stress', '', 'publish', 'open', 'open', '', 'tieu-duong-va-stress', '', '', '2020-06-11 07:59:30', '2020-06-11 07:59:30', '', 0, 'http://localhost:8080/yentheme/?p=68', 0, 'post', '', 0),
(69, 1, '2020-06-09 02:30:56', '2020-06-09 02:30:56', '', '5(2)', '', 'inherit', 'open', 'closed', '', '52-2', '', '', '2020-06-09 02:30:56', '2020-06-09 02:30:56', '', 68, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/52.jpg', 0, 'attachment', 'image/jpeg', 0),
(70, 1, '2020-06-09 02:31:01', '2020-06-09 02:31:01', '<img src=\"http://localhost:8080/yentheme/wp-content/uploads/2020/06/52-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" class=\"alignnone size-medium wp-image-69\" />', 'Tiểu đường và stress', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2020-06-09 02:31:01', '2020-06-09 02:31:01', '', 68, 'http://localhost:8080/yentheme/2020/06/09/68-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-06-09 02:32:05', '2020-06-09 02:32:05', 'Tự kiểm tra đừng máu tại nhà.', 'Tự kiểm tra đường máu ở nhà', '', 'publish', 'open', 'open', '', 'tu-kiem-tra-duong-mau-o-nha', '', '', '2020-06-10 02:27:29', '2020-06-10 02:27:29', '', 0, 'http://localhost:8080/yentheme/?p=71', 0, 'post', '', 0),
(72, 1, '2020-06-09 02:32:02', '2020-06-09 02:32:02', '', '6', '', 'inherit', 'open', 'closed', '', '6', '', '', '2020-06-09 02:32:02', '2020-06-09 02:32:02', '', 71, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/6.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2020-06-09 02:32:05', '2020-06-09 02:32:05', '<img src=\"http://localhost:8080/yentheme/wp-content/uploads/2020/06/6-300x200.png\" alt=\"\" width=\"300\" height=\"200\" class=\"alignnone size-medium wp-image-72\" />', 'Tự kiểm tra đường máu ở nhà', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2020-06-09 02:32:05', '2020-06-09 02:32:05', '', 71, 'http://localhost:8080/yentheme/2020/06/09/71-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2020-06-09 02:33:05', '2020-06-09 02:33:05', '', '3', '', 'inherit', 'open', 'closed', '', '3', '', '', '2020-06-09 02:33:05', '2020-06-09 02:33:05', '', 54, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/3.jpg', 0, 'attachment', 'image/jpeg', 0),
(75, 1, '2020-06-09 02:33:12', '2020-06-09 02:33:12', '<img src=\"http://localhost:8080/yentheme/wp-content/uploads/2020/06/3-300x214.jpg\" alt=\"\" width=\"300\" height=\"214\" class=\"alignnone size-medium wp-image-74\" />Bạn cần chăm sóc bản thân như thế nào?', 'Chăm sóc bản thân', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2020-06-09 02:33:12', '2020-06-09 02:33:12', '', 54, 'http://localhost:8080/yentheme/2020/06/09/54-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2020-06-09 03:15:17', '2020-06-09 03:15:17', 'Tự kiểm tra đừng máu tại nhà.', 'Tự kiểm tra đường máu ở nhà', '', 'inherit', 'closed', 'closed', '', '71-autosave-v1', '', '', '2020-06-09 03:15:17', '2020-06-09 03:15:17', '', 71, 'http://localhost:8080/yentheme/2020/06/09/71-autosave-v1/', 0, 'revision', '', 0),
(77, 1, '2020-06-09 03:15:23', '2020-06-09 03:15:23', 'Tự kiểm tra đừng máu tại nhà.', 'Tự kiểm tra đường máu ở nhà', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2020-06-09 03:15:23', '2020-06-09 03:15:23', '', 71, 'http://localhost:8080/yentheme/2020/06/09/71-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2020-06-09 03:16:08', '2020-06-09 03:16:08', 'Tiểu đường và stress thật sự ko tốt.', 'Tiểu đường và stress', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2020-06-09 03:16:08', '2020-06-09 03:16:08', '', 68, 'http://localhost:8080/yentheme/2020/06/09/68-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2020-06-09 03:16:47', '2020-06-09 03:16:47', 'Tự theo dõi và kiểm soát bệnh tiều đường tại nhà an toàn', 'Theo dõi và kiểm soát bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2020-06-09 03:16:47', '2020-06-09 03:16:47', '', 65, 'http://localhost:8080/yentheme/2020/06/09/65-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2020-06-09 03:17:25', '2020-06-09 03:17:25', 'Các biến chứng', 'Các biến chứng của bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-06-09 03:17:25', '2020-06-09 03:17:25', '', 59, 'http://localhost:8080/yentheme/2020/06/09/59-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2020-06-09 03:17:58', '2020-06-09 03:17:58', 'Tiến triển', 'Tiến triển của bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2020-06-09 03:17:58', '2020-06-09 03:17:58', '', 56, 'http://localhost:8080/yentheme/2020/06/09/56-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2020-06-09 03:18:18', '2020-06-09 03:18:18', 'Bạn cần chăm sóc bản thân như thế nào?', 'Chăm sóc bản thân', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2020-06-09 03:18:18', '2020-06-09 03:18:18', '', 54, 'http://localhost:8080/yentheme/2020/06/09/54-revision-v1/', 0, 'revision', '', 0),
(83, 1, '2020-06-09 03:56:50', '2020-06-09 03:56:50', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-06-09 03:56:50', '2020-06-09 03:56:50', '', 1, 'http://localhost:8080/yentheme/2020/06/09/1-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2020-06-10 02:33:01', '2020-06-10 02:33:01', 'Biến chứng bệnh tiểu đường thường phát triển dần dần. Bạn mắc bệnh tiểu đường...', 'Biến chứng của tiểu đường', '', 'publish', 'open', 'open', '', 'bien-chung-cua-tieu-duong', '', '', '2020-06-10 04:13:07', '2020-06-10 04:13:07', '', 0, 'http://localhost:8080/yentheme/?p=84', 0, 'post', '', 0),
(85, 1, '2020-06-10 02:32:56', '2020-06-10 02:32:56', '', 'HAB-tieuduong (1)', '', 'inherit', 'open', 'closed', '', 'hab-tieuduong-1', '', '', '2020-06-10 02:32:56', '2020-06-10 02:32:56', '', 84, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/HAB-tieuduong-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2020-06-10 02:33:01', '2020-06-10 02:33:01', '', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '84-revision-v1', '', '', '2020-06-10 02:33:01', '2020-06-10 02:33:01', '', 84, 'http://localhost:8080/yentheme/2020/06/10/84-revision-v1/', 0, 'revision', '', 0),
(87, 1, '2020-06-10 02:33:53', '2020-06-10 02:33:53', '', 'Biến chứng của tiểu đường', '', 'trash', 'open', 'open', '', 'tong-quan-kien-thuc-ve-benh-tieu-duong__trashed', '', '', '2020-06-12 03:33:27', '2020-06-12 03:33:27', '', 0, 'http://localhost:8080/yentheme/?p=87', 0, 'post', '', 0),
(88, 1, '2020-06-10 02:33:50', '2020-06-10 02:33:50', '', '1 (1)', '', 'inherit', 'open', 'closed', '', '1-1', '', '', '2020-06-10 02:33:50', '2020-06-10 02:33:50', '', 87, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2020-06-10 02:33:53', '2020-06-10 02:33:53', '', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2020-06-10 02:33:53', '2020-06-10 02:33:53', '', 87, 'http://localhost:8080/yentheme/2020/06/10/87-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2020-06-10 02:34:47', '2020-06-10 02:34:47', '<p dir=\"ltr\">Bài viết dưới đây sẽ khái quát những điểm cơ bản như “bệnh tiểu đường là gì?”, các triệu chứng, phân loại, nguyên nhân, cách điều trị, chi phí điều trị bệnh tiểu đường. Thông qua bài viết này, mọi người sẽ có kiến thức cơ bản về bệnh tiểu đường!</p>\r\n\r\n<h2 dir=\"ltr\">1. Bệnh tiểu đường là bệnh như thế nào?</h2>\r\n<h3 dir=\"ltr\">Tìm hiểu bệnh tiểu đường là gì?</h3>\r\n<p dir=\"ltr\">Chúng ta ăn uống hàng ngày. Lượng carbohydrates từ bữa ăn được hấp thu vào đường ruột như glucose và hòa tan trong máu. Khi đó, một hormone gọi là insulin được tiết ra, nhờ insulin tiết ra, glucose được đưa vào các tế bào và trở thành nguồn năng lượng của cơ thể.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-01(1).jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Nếu sự hoạt động của insulin không hiệu quả và nếu glucose tăng lên đến mức mà việc xử lý của insulin không thể đáp ứng được, thì sẽ có một lượng lớn glucose không được chuyển hóa thành nguồn năng lượng của cơ thể và bị dư thừa trong máu. “Tình trạng lượng glucose trong máu (lượng đường trong máu) vượt quá tỷ lệ nhất định” được gọi là “bệnh tiểu đường”.</p>\r\n<p dir=\"ltr\">Do bệnh tiểu đường có một số loại khác nhau nên không thể gộp chung vào một loại, nhưng nhìn chung các triệu chứng cơ năng điển hình của bệnh tiểu đường cụ thể như sau:</p>\r\n<p dir=\"ltr\">– Đa niệu (lượng nước tiểu nhiều)</p>\r\n<p dir=\"ltr\">– Khô miệng, uống nhiều (hay khát nước và uống nhiều nước)</p>\r\n<p dir=\"ltr\">– Cân nặng giảm</p>\r\n<p dir=\"ltr\">– Trở nên dễ mệt mỏi</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh tiểu đường có gì đáng sợ?</h3>\r\n<p dir=\"ltr\">Khi bị tiểu đường (tình trạng lượng đường trong máu vượt quá tỷ lệ nhất định), người bệnh sẽ gặp khó khăn gì?</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-02.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Nếu lượng đường trong máu tăng lên, máu trở nên đặc hơn, tạo áp lực lớn lên mạch máu. Hiện tượng này lâu dài sẽ gây ra nhiều biến chứng và đây chính là điểm đáng sợ của bệnh tiểu đường. Đặc biệt phần lớn các biến chứng của bệnh tiểu đường đều liên quan đến mao mạch và tùy từng trường hợp, bệnh nhân có thể bị mất thị giác, thậm chí các chi của chân tay có thể bị hoại tử.</p>\r\n\r\n<h3 dir=\"ltr\">Ba loại bệnh tiểu đường</h3>\r\n<p dir=\"ltr\">Như đã đề cập trước đó, bệnh tiểu đường là “Trạng thái lượng glucose trong máu (lượng đường trong máu) vượt quá tỷ lệ nhất định”, nhưng có rất nhiều nguyên nhân dẫn đến tình trạng này.</p>\r\n<p dir=\"ltr\">Tùy thuộc vào từng nguyên nhân, bệnh tiểu đường có thể được chia thành 3 loại sau đây.</p>\r\n<p dir=\"ltr\">– Bệnh tiểu đường tuýp là bệnh tiểu đường do các tế bào β của tuyến tụy tiết ra insulin bị phá hủy. Đây là bệnh có tỷ lệ khởi phát cao ở đối tượng trẻ em và vị thành niên, có đặc trưng là thường đột nhiên phát triển và tiến triển nhanh chóng. Ngoài ra, các triệu chứng ban đầu của bệnh rất giống với triệu chứng bệnh cảm lạnh, nhưng sau đó xuất hiện thêm các triệu chứng như khát nước, đa niệu, đột nhiên giảm cân.</p>\r\n<p dir=\"ltr\">Về phương pháp điều trị, khi tế bào β của tuyến tụy phá hủy, insulin không thể được tiết ra, vì vậy nếu bệnh nhân không thường xuyên cung cấp insulin từ bên ngoài vào cơ thể thì bệnh có thể gây nguy hiểm đến tính mạng. Do đó biện pháp tiêm insulin là rất cần thiết.</p>\r\n<p dir=\"ltr\">– Bệnh tiểu đường tuýp 2</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-03.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 2 (type 2) là một loại bệnh tiểu đường khởi phát ở những người có yếu tố di truyền về bệnh tiểu đường và có “vấn đề lối sống” như béo phì, phàm ăn tục uống, thiếu vận động, căng thẳng. Bệnh tiểu đường tuýp 2 được coi là một trong “7 bệnh lối sống lớn”.</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 khởi phát chủ yếu ở những người sau độ tuổi trung niên. Đặc trưng của loại bệnh tiểu đường này là gần như không có triệu chứng cơ năng, thường khởi phát mà bệnh nhân không biết. Không ít các trường hợp bệnh nhân ngạc nhiên khi lần đầu tiên phát hiện bệnh trong kỳ khám sức khỏe tại nơi làm việc và nơi sinh sống mặc dù nghĩ rằng bản thân khỏe mạnh.</p>\r\n<p dir=\"ltr\">Tuy nhiên, không thể không quan tâm đến bệnh tiểu đường vì cho rằng bệnh không gây đau đớn cho cơ thể. Bởi nếu để bệnh tiến triển, có thể gây ra các biến chứng như mù lòa, hoại tử bàn chân.</p>\r\n<p dir=\"ltr\">Tất nhiên, không thể khẳng định một cách chắc chắn về các biến chứng bởi bệnh sẽ tiến triển khác nhau tùy thuộc vào tình trạng bệnh và quá trình, nhưng ở giai đoạn đầu, bệnh nhân có thể điều chỉnh lượng đường trong máu bằng cách cải thiện lối sống như chế độ ăn uống và vận động. Tuy nhiên, theo thời gian, việc kiểm soát lượng đường trong máu chỉ bằng cách cải thiện lối sống trở nên khó khăn hơn và trong một số trường hợp, bệnh nhân có thể chuyển sang điều trị bằng thuốc như thuốc uống hoặc tiêm insulin.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-04.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Ngoài ra, có thể nói bệnh tiểu đường là một căn bệnh khó chữa khỏi. Ngay cả khi chỉ số đường huyết đã ổn định nhưng nếu bệnh nhân ngừng điều trị, chỉ số này sẽ lại tăng lên rất nhanh. Vì vậy, trường hợp bệnh nhân phát hiện bị bệnh tiểu đường type 2, hãy đến tư vấn tại một cơ sở y tế chuyên khoa để được duy trì điều trị và tiến hành xét nghiệm thích hợp.</p>\r\n<p dir=\"ltr\">– Bệnh tiểu đường thai kỳ</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường thai kỳ là một tình trạng bất thường trong trao đổi chất đường do ảnh hưởng của việc mang thai và được phát hiện hoặc khởi phát lần đầu khi mang thai. Đây là một bệnh tiêu biểu ở phụ nữ mang thai, khi phụ nữ mang thai, hormone được tạo ra bởi nhau thai ức chế chức năng insulin, do đó gây ra sự tăng lượng đường trong máu.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-05.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tất nhiên, không phải tất cả phụ nữ mang thai đều bị tiểu đường, nhưng trường hợp thai phụ béo phì, mang thai khi lớn tuổi, tiền sử gia đình thai phụ có người bị tiểu đường thường sẽ có xu hướng dễ bị tiểu đường thai kỳ hơn.</p>\r\n\r\n<h2 dir=\"ltr\">2. Biến chứng của bệnh tiểu đường là gì?</h2>\r\n<p dir=\"ltr\">Điều đáng lo ngại trong bệnh tiểu đường là những biến chứng khác nhau xuất hiện do lượng đường trong máu tăng lên tạo áp lực lớn lên mạch máu. Có lẽ mọi người thường nghe những câu chuyện nói rằng “Nếu bị bệnh tiểu đường, mắt sẽ trở nên không thể nhìn thấy, thận trở nên tồi tệ hơn, hoặc đầu chân tay bị hoại tử.” Thực tế, đây được coi là những triệu chứng tiêu biểu gây ra bởi 3 biến chứng chính của bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">Phần này sẽ giới thiệu khái quát về 3 biến chứng tiểu đường chính:</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh võng mạc tiểu đường</h3>\r\n<p dir=\"ltr\">Cơ chế con người nhìn thấy mọi thứ bằng mắt của mình có thể được mô phỏng như cấu trúc của một bộ phim. “Việc nhìn thấy mọi thứ” có thể nói rằng đó là quá trình ánh sáng đi vào từ tròng mắt được khúc xạ bởi giác mạc hoặc thủy tinh thể (thấu kính của máy chiếu), liên kết các hình ảnh được chiếu lên võng mạc (màn hình) và não ghi nhận hình ảnh đó. Việc bạn đọc ghi nhận dòng chữ được viết ở đây ngay lúc này cũng chính là từ việc cảm nhận ánh sáng trên võng mạc thông qua giác mạc và thủy tinh thể.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-06.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Võng mạc là một màng mỏng với vô số dây thần kinh ở đáy mắt và mao mạch căng xung quanh. “Bệnh võng mạc tiểu đường” là một biến chứng dẫn đến giảm thị lực và mù lòa do áp lực lên mạch máu gây ra bởi bệnh tiểu đường, máu và oxy không đủ để truyền tới mao mạch của võng mạc.</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh thận do tiểu đường</h3>\r\n<p dir=\"ltr\">Vai trò quan trọng của thận trong cơ thể là lọc chất thải và các chất không cần thiết trong máu rồi thải chúng ra khỏi cơ thể dưới dạng nước tiểu. Có thể nói máu của cơ thể chúng ta được duy trì ở trạng thái ổn định chính là nhờ chức năng lọc của thận.</p>\r\n<p dir=\"ltr\">Tuy nhiên, khi lượng đường trong máu tăng lên do bệnh tiểu đường, bộ lọc thận bị tắc nghẽn, không thể xử lý được chất thải và các chất không cần thiết, khiến cho việc giữ ổn định tình trạng của máu trở nên khó khăn. “Bệnh thận do tiểu đường” là rối loạn chức năng thận do bệnh tiểu đường gây ra.</p>\r\n<p dir=\"ltr\">Trong trường hợp xấu nhất, có thể dẫn đến các triệu chứng đe dọa tính mạng nghiêm trọng như tăng ure máu, suy thận và có thể cần phải chạy thận nhân tạo bằng thiết bị y tế.</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh thần kinh tiểu đường</h3>\r\n<p dir=\"ltr\">“Bệnh thần kinh tiểu đường” là một trong ba biến chứng lớn của bệnh tiểu đường cùng với bệnh võng mạc tiểu đường và bệnh thận do tiểu đường, nhưng có vẻ như phần lớn mọi người vẫn chưa hiểu rõ tại sao bệnh tiểu đường gây tổn thương thần kinh. Có rất nhiều giả thuyết khác nhau về nguyên nhân của biến chứng thần kinh tiểu đường như “Bởi vì nếu bệnh tiểu đường càng tiến triển, các tế bào thần kinh sẽ tích lũy nhiều vật chất gây tổn thương”, “Tiểu đường khiến việc lưu thông máu trong mao mạch trở nên kém đi, dẫn đến thiếu oxy và chất dinh dưỡng cần thiết cho tế bào thần kinh”, nhưng có vẻ như để nói rằng trong các giả thuyết trên, giả thuyết nào đúng thì chưa có kết luận cuối cùng.</p>\r\n<p dir=\"ltr\">Người ta nói rằng các triệu chứng ban đầu của bệnh thần kinh tiểu đường thường là bị tê và đau chủ yếu ở các ngón chân và bàn chân. Về cảm giác, các triệu chứng thường được biểu hiện ở trạng thái “châm chích”, “đau rát”,…, khi các triệu chứng tiến triển, đau và tê sẽ dần dần xuất hiện ở các ngón tay. Hơn nữa, khi thần kinh tiếp tục bị tổn thương, cảm giác của tứ chi trở nên chậm hơn và bệnh nhân thường bị thương trên tay chân mà không biết.</p>\r\n<p dir=\"ltr\">Điều phải chú ý đặc biệt trong bệnh thần kinh tiểu đường là khi bệnh nhân không chú ý đến một vết trầy xước rất nhỏ do đi giày bị chật hoặc bị cứa, vết thương có thể trở nên tồi tệ hơn như bị nhiễm khuẩn và thời điểm bệnh nhân đi khám tại bệnh viện thì tay và chân đã bị hư hỏng nặng không thể sử dụng được nữa, cách chữa trị duy nhất là cắt bỏ chân tay. Bệnh nhân tiểu đường nên xác nhận trực quan các chi của chân tay trong khi tắm.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-07.jpg\" alt=\"\" /></p>\r\n\r\n<h2 dir=\"ltr\">3. Phương pháp điều trị bệnh tiểu đường</h2>\r\n<p dir=\"ltr\">Có ba phương pháp điều trị bệnh tiểu đường tiêu biểu. Liệu pháp ăn uống, liệu pháp vận động, điều trị bằng thuốc. Cả ba đều là phương pháp điều trị nhằm kiểm soát mức đường trong máu và giảm áp lực lên các mạch máu, có thể áp dụng tùy theo loại và triệu chứng của bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">Hãy cùng tìm hiểu cụ thể về ba phương pháp điều trị này.</p>\r\n\r\n<h3 dir=\"ltr\">Ăn uống</h3>\r\n<p dir=\"ltr\">Liệu pháp ăn uống là liệu pháp nhằm điều chỉnh lượng đường trong máu bằng thói quen ăn uống lành mạnh có cân bằng. Các điểm chung cần lưu ý được đưa ra là “ăn từ từ và nhai kỹ”, “ăn uống lành mạnh, điều độ vào buổi sáng, buổi trưa và buổi tối”, “không ăn vào đêm muộn hoặc trước khi đi ngủ”, “không ăn quá no, chỉ nên ăn vừa đủ no”, “Cố gắng cân bằng dinh dưỡng với nhận thức đầy đủ về khoáng chất và vitamin, đặc biệt là ba chất dinh dưỡng chính carbohydrate, protein và chất béo”.</p>\r\n<p dir=\"ltr\">Tuy nhiên, vì sự thèm ăn là một trong 3 ham muốn tuyệt vời của con người, ngay cả khi nhận thức điều đó trong đầu, đôi khi bệnh nhân vẫn bị rối loạn trong thói quen ăn uống như “muốn ăn đồ ăn có dầu mỡ!”, “muốn ăn đến no căng bụng”. Để giải quyết vấn đề này, đôi khi có thể thử tạo “ngày đặc biệt để ăn đồ bản thân thích”.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-08.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Dù bằng cách nào, do đây là “điều trị” bằng chế độ ăn uống nên vẫn cần tham khảo ý kiến bác sĩ phụ trách.</p>\r\n\r\n<h3 dir=\"ltr\">Vận động</h3>\r\n<p dir=\"ltr\">Liệu pháp vận động là một phương pháp điều trị thúc đẩy glucose trong máu được đưa vào tế bào để trở thành năng lượng và làm giảm lượng đường trong máu thông qua việc vận động. Tuy nhiên, trong điều trị tiểu đường, không phải loại vận động nào cũng tốt, nhưng nhìn chung thì tập thể dục nhịp điệu vừa phải với mức độ cảm thấy “hơi khó khăn…” và rèn luyện cơ bắp dường như hiệu quả hơn vận động nặng như thi đấu của vận động viên thể thao,.</p>\r\n<p dir=\"ltr\">Ngoài ra, khi bệnh nhân bắt đầu tăng cường vận động nặng nhưng bỏ cuộc giữa chừng do cảm thấy khó hoặc làm tổn thương cơ thể thì việc tập tăng cường cũng không mang lại hiệu quả gì. Hãy tham khảo ý kiến bác sĩ phụ trách trước và lập một chương trình tập luyện ổn định, hiệu quả và phù hợp với khả năng của bản thân.</p>\r\n\r\n<h3 dir=\"ltr\">Điều trị bằng thuốc</h3>\r\n<p dir=\"ltr\">Điều trị bằng thuốc là một phương pháp điều trị kiểm soát lượng đường trong máu trong phạm vi bình thường với sự hỗ trợ của thuốc. Có hai phương pháp chính của việc điều trị bằng thuốc là “tiêm insulin” và “uống thuốc hạ đường huyết”, mỗi phương pháp có cách tiến hành khác nhau.</p>\r\n<p dir=\"ltr\">Như đã đề cập trước đó, bệnh tiểu đường là một căn bệnh do lượng đường trong máu tăng lên khi tuyến tụy không tiết insulin hoặc lượng insulin tiết ra bị giảm.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-09.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Về cơ bản, có vẻ như điều trị bằng loại thuốc nào phụ thuộc vào “lượng insulin tiết ra từ tuyến tụy”. Thông thường, trường hợp lượng insulin tiết ra từ tuyến tụy rất thấp/gần như không có, biện pháp tiêm insulin được áp dụng, trường hợp việc tiết insulin từ tuyến tụy được duy trì ở một mức độ nào đó, biện pháp “uống thuốc hạ đường huyết” được áp dụng.</p>\r\n\r\n<h2 dir=\"ltr\">4. Chi phí điều trị bệnh tiểu đường</h2>\r\n<p dir=\"ltr\">Bệnh tiểu đường thường được ví là “bệnh nhà giàu” bởi thời gian điều trị lâu dài và chi phí điều trị vô cùng tốn kém. Tuy nhiên chi phí điều trị bệnh tiểu đường khác nhau tùy thuộc vào sự tiến triển của bệnh và tình trạng của bệnh nhân.</p>\r\n<p dir=\"ltr\">Bài viết này đã giới thiệu khái quát về triệu chứng, nguyên nhân, biến chứng, phương pháp điều trị, chi phí điều trị,…của bệnh tiểu đường nói chung. Sau khi đọc xong bài viết, hy vọng mọi người đã có thể hình dung rõ hơn về bệnh tiểu đường nói chung.</p>', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'publish', 'open', 'open', '', 'tong-quan-kien-thuc-ve-benh-tieu-duong-2', '', '', '2020-06-15 02:25:24', '2020-06-15 02:25:24', '', 0, 'http://localhost:8080/yentheme/?p=90', 0, 'post', '', 0),
(91, 1, '2020-06-10 02:34:45', '2020-06-10 02:34:45', '', 'HAB-tieuduong', '', 'inherit', 'open', 'closed', '', 'hab-tieuduong', '', '', '2020-06-10 02:34:45', '2020-06-10 02:34:45', '', 90, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/HAB-tieuduong.jpg', 0, 'attachment', 'image/jpeg', 0),
(92, 1, '2020-06-10 02:34:47', '2020-06-10 02:34:47', '', 'Tiểu đường tuýp 1', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2020-06-10 02:34:47', '2020-06-10 02:34:47', '', 90, 'http://localhost:8080/yentheme/2020/06/10/90-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2020-06-10 02:34:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-06-10 02:34:50', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=93', 0, 'post', '', 0),
(94, 1, '2020-06-10 02:34:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-06-10 02:34:50', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=94', 0, 'post', '', 0),
(95, 1, '2020-06-10 02:53:43', '2020-06-10 02:53:43', 'Biến chứng bệnh tiểu đường thường phát triển dần dần. Bạn mắc bệnh tiểu đường...', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '84-revision-v1', '', '', '2020-06-10 02:53:43', '2020-06-10 02:53:43', '', 84, 'http://localhost:8080/yentheme/2020/06/10/84-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2020-06-10 03:15:06', '2020-06-10 03:15:06', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-cai-dat-chung\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Hiển thị bài viết theo danh mục ngoài trang chủ', 'hien-thi-bai-viet-theo-danh-muc-ngoai-trang-chu', 'publish', 'closed', 'closed', '', 'group_5ee04fe3789d7', '', '', '2020-06-10 04:06:26', '2020-06-10 04:06:26', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&#038;p=97', 0, 'acf-field-group', '', 0),
(98, 1, '2020-06-10 03:15:06', '2020-06-10 03:15:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID danh mục', 'id_danh_muc', 'publish', 'closed', 'closed', '', 'field_5ee0501418368', '', '', '2020-06-10 03:15:06', '2020-06-10 03:15:06', '', 97, 'http://localhost:8080/yentheme/?post_type=acf-field&p=98', 0, 'acf-field', '', 0),
(99, 1, '2020-06-10 03:20:28', '2020-06-10 03:20:28', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";i:1;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Giới hạn số bài lấy', 'limit_post_category_home', 'publish', 'closed', 'closed', '', 'field_5ee051512393e', '', '', '2020-06-10 03:22:20', '2020-06-10 03:22:20', '', 97, 'http://localhost:8080/yentheme/?post_type=acf-field&#038;p=99', 1, 'acf-field', '', 0),
(100, 1, '2020-06-10 03:23:43', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-10 03:23:43', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&p=100', 0, 'acf-field-group', '', 0),
(101, 1, '2020-06-10 04:10:29', '2020-06-10 04:10:29', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-cai-dat-chung\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Hiển thị bài viết theo danh mục ngoài trang chủ - 2', 'hien-thi-bai-viet-theo-danh-muc-ngoai-trang-chu-2', 'publish', 'closed', 'closed', '', 'group_5ee05c9f2c65b', '', '', '2020-06-10 04:10:29', '2020-06-10 04:10:29', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&#038;p=101', 0, 'acf-field-group', '', 0),
(102, 1, '2020-06-10 04:10:29', '2020-06-10 04:10:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID Danh mục', 'id_danh_muc_2', 'publish', 'closed', 'closed', '', 'field_5ee05cad3d674', '', '', '2020-06-10 04:10:29', '2020-06-10 04:10:29', '', 101, 'http://localhost:8080/yentheme/?post_type=acf-field&p=102', 0, 'acf-field', '', 0),
(103, 1, '2020-06-10 04:10:29', '2020-06-10 04:10:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";i:1;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Giới hạn số lượng bài hiển thị', 'limit_post_category_home_2', 'publish', 'closed', 'closed', '', 'field_5ee05cdb3d675', '', '', '2020-06-10 04:10:29', '2020-06-10 04:10:29', '', 101, 'http://localhost:8080/yentheme/?post_type=acf-field&p=103', 1, 'acf-field', '', 0),
(104, 1, '2020-06-10 04:14:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-10 04:14:13', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&p=104', 0, 'acf-field-group', '', 0),
(105, 1, '2020-06-10 04:30:53', '2020-06-10 04:30:53', '', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2020-06-10 04:30:53', '2020-06-10 04:30:53', '', 90, 'http://localhost:8080/yentheme/2020/06/10/90-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2020-06-10 04:32:12', '2020-06-10 04:32:12', '', 'Biến chứng bênh tiểu đường', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2020-06-10 04:32:12', '2020-06-10 04:32:12', '', 90, 'http://localhost:8080/yentheme/2020/06/10/90-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2020-06-10 04:33:21', '2020-06-10 04:33:21', '', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2020-06-10 04:33:21', '2020-06-10 04:33:21', '', 90, 'http://localhost:8080/yentheme/2020/06/10/90-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2020-06-10 04:33:45', '2020-06-10 04:33:45', '', 'Biến chứng bênh tiểu đường', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2020-06-10 04:33:45', '2020-06-10 04:33:45', '', 87, 'http://localhost:8080/yentheme/2020/06/10/87-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2020-06-10 04:34:01', '2020-06-10 04:34:01', '', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2020-06-10 04:34:01', '2020-06-10 04:34:01', '', 87, 'http://localhost:8080/yentheme/2020/06/10/87-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(110, 1, '2020-06-10 04:34:59', '2020-06-10 04:34:59', '<p dir=\"ltr\">Mọi người thường nghĩ rằng tiểu đường là căn bệnh mà khi ta già đi, béo lên sẽ mắc phải. Tuy nhiên vẫn có những người trẻ bị tiểu đường. Hơn nửa số người mắc tiểu đường thuộc loại tiểu đường tuýp 2. Tuy nhiên bệnh tiểu đường tuýp 1 thường gặp ở những người vừa trẻ, vừa gầy là căn bệnh như thế nào? Qua bài viết này chúng ta hãy cùng tìm hiểu về loại tiểu đường này nhé.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>1. Tiểu đường tuýp 1 là căn bệnh như thế nào?</strong></h2>\r\n<p dir=\"ltr\">* Những điểm cần chú ý:</p>\r\n<p dir=\"ltr\">+ Hầu hết mọi người mắc tiểu đường là thuộc tuýp 2, nhưng vẫn có người mắc tuýp 1.</p>\r\n<p dir=\"ltr\">+ Tiểu đường tuýp 1 là bệnh mà các tế bào tụy bị phá hủy nên không thể tiết ra insulin.</p>\r\n<p dir=\"ltr\">+ 80% người mắc tiểu đường tuýp 1 có nguyên nhân do hệ miễn dịch bất thường dẫn tới tự sinh ra các kháng thể.</p>\r\n<p dir=\"ltr\">+ Tiểu đường tuýp 1 hầu hết khởi phát ở người trẻ tuổi nhưng không có nghĩa chỉ có mỗi người trẻ bị mắc bệnh.</p>\r\n<p dir=\"ltr\">Mọi người có nghĩ rằng “Tiểu đường là căn bệnh khởi phát ở người bị béo phì lúc trung tuổi” không? Thực ra suy nghĩ này cũng không hẳn là sai. Phần lớn những người mắc tiểu đường (trên 95%) có nguyên nhân là do béo phì bởi việc ăn uống quá độ, ít vận động, dần lớn tuổi, Những người mắc tiểu đường do nguyên nhân này sẽ được phân loại vào tiểu đường tuýp 2.</p>\r\n<p dir=\"ltr\">Chắc hẳn sẽ có người từng nghĩ có tuýp 2 thì hẳn phải có tuýp 1. Và đúng như vậy, so với bệnh tiểu đường tuýp 2, tiểu đường tuýp 1 là căn bệnh hầu hết được phát hiện ở những người khá trẻ.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-1-la-gi-01-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">* Trước hết thì mọi người cần phân biệt tiểu đường tuýp 1 và tuýp 2.</p>\r\n<p dir=\"ltr\">Tiểu đường là căn bệnh mà lượng đường huyết quá cao gây ra những thay đổi bất thường trong cơ thể.</p>\r\n<p dir=\"ltr\">Đường hay nói cách khác là glucose là một chất cần thiết để duy trì hoạt động của cơ thể người. Bởi vì não bộ chỉ có thể sử dụng glucose như một chất dinh dưỡng giúp điều khiển các chức năng của con người. Do đó cơ thể sẽ phải tự điều chỉnh bằng nhiều cách khác nhau sao cho glucose trong máu, hay lượng đường huyết luôn ở mức cân bằng, không quá cao hoặc không quá thấp.</p>\r\n<p dir=\"ltr\">Lượng glucose mà cơ thể cần được cung cấp hầu hết qua việc ăn uống. Bằng hoạt động tiêu hóa thức ăn mà lượng glucose này sẽ được hấp thụ và chuyển thành dinh dưỡng cho não bộ. Hầu hết người mắc tiểu đường tuýp 2 có chế độ ăn uống quá mức nên lượng dinh dưỡng hấp thụ vào cơ thể cũng quá nhiều, dẫn tới lượng đường huyết luôn duy trì ở mức quá cao và lâu dần thành bệnh.</p>\r\n<p dir=\"ltr\">Tuy vậy, cơ thể người sẽ có cơ chế tự cân bằng đường huyết, tức là đường huyết cao cơ thể sẽ điều chỉnh cho nó giảm bớt xuống để cơ thể trở lại bình thường, cho nên để nói dễ dàng mắc tiểu đường thì không có, vì bệnh tiến triển rất từ từ.</p>\r\n<p dir=\"ltr\">Khi cơ thể có mức đường huyết cao, sẽ xuất hiện một loại hormone có vai trò giảm lượng đường huyết xuống có tên là insulin được tiết ra từ tụy. Ở tiểu đường tuýp 2 thì cơ thể sẽ giảm chức năng của insulin khiến cho đường huyết luôn ở mức cao. Còn có một cách gọi khác của tiểu đường tuýp 2 là “mất khả năng dung nạp glucose”.</p>\r\n<p dir=\"ltr\">Mặt khác sẽ có trường hợp không phải là “giảm hiệu quả insulin” mà là “không thể tiết insulin”. Do insulin không thể tiết ra dẫn tới tình trạng đường huyết cao và bị tiểu đường thì gọi chung là “tiểu đường tuýp 1”. Tiểu đường tuýp 1 có thể do tế bào tụy bị hỏng nên không thể tiết ra insulin. Đây chính là điểm để phân biệt tuýp 1 và tuýp 2.</p>\r\n<p dir=\"ltr\">Mọi người cần phân biệt rõ tiểu đường tuýp 1 và tiểu đường tuýp 2 vì cơ chế khác nhau nên cách điều trị như ăn uống, tập luyện và sử dụng thuốc, những thói quen hàng ngày,v.v… cũng khác nhau.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>2. Các triệu chứng, đặc trưng phát bệnh của tiểu đường tuýp 1. Những xét nghiệm cần thiết cho việc chẩn đoán.</strong></h2>\r\n<p dir=\"ltr\">* Những điều cần chú ý:</p>\r\n<p dir=\"ltr\">+ Tiểu đường tuýp 1 được chia ra làm 3 loại bệnh.</p>\r\n<p dir=\"ltr\">+ Chẩn đoán chính xác tuýp 1 hoặc tuýp 2, từ đó có những lộ trình trị liệu cụ thể, rõ ràng cho từng loại là rất quan trọng.</p>\r\n<p dir=\"ltr\">Mọi người cần hiểu rõ tiểu đường tuýp 1 được chia ra làm 3 loại : loại phát bệnh cấp tính, loại phát bệnh từ từ, loại nguy cấp. Dựa vào thông tin từ trạng thái trước khi phát bệnh, thời gian của các triệu chứng mà có thể phân biệt với tiểu đường tuýp 2. Sau đây chúng ta sẽ cùng tìm hiểu kỹ từng loại.</p>\r\n<p dir=\"ltr\">– Tiểu đường tuýp 1 – loại phát bệnh cấp tính</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 1 – loại phát bệnh cấp tính là loại hay gặp nhất trong tiểu đường tuýp 1. Có thể chỉ vài tháng hoặc vài tuần là đã tiến triển thành bệnh.</p>\r\n<p dir=\"ltr\">Sau khi xuất hiện các triệu chứng như hay khát nước, đi tiểu nhiều lần (lượng nước tiểu mỗi lần nhiều), sụt cân, chỉ trong vài tháng chức năng tiết insulin sẽ hoàn toàn biến mất. Trường hợp đột nhiên sụt giảm khả năng tiết insulin, các triệu chứng đi kèm với rối loạn thần kinh ngoại biên (ketoacidosis tiểu đường) cũng có thể xảy ra. Tiểu đường tuýp 1 – loại cấp tính có đặc trưng là do tính tự miễn dịch và dương tính với “các tự kháng thể liên quan tới đảo tụy”. Các kháng thể này tồn tại dưới dạng protein trong cơ thể sẽ tự tấn công các tế bào tụy, khiến tụy bị phá hủy.</p>\r\n<p dir=\"ltr\">– Tiểu đường tuýp 1 – loại phát bệnh từ từ</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 1 – loại phát bệnh từ từ là bệnh phần lớn xuất hiện ở người từ 30-50 tuổi, và bệnh tiến triển một cách chậm rãi. Do đó cũng có trường hợp bị nhầm lẫn với tiểu đường tuýp 2. Loại này cũng có tính tự miễn dịch, các tế bào tụy cũng bị hệ miễn dịch tấn công, phân biệt rõ với tiểu đường tuýp 2. Việc phát hiện sớm và phân biệt rõ ràng là cực kỳ cần thiết, vì tiểu đường tuýp 1 – loại phát bệnh từ từ và tiểu đường tuýp 2 sẽ có những cách điều trị khác nhau vào những giai đoạn khác nhau.</p>\r\n<p dir=\"ltr\">– Tiểu đường tuýp 1 – loại nguy cấp</p>\r\n<p dir=\"ltr\">Đây là loại khác hoàn toàn so với 2 loại trên, không hề liên quan tới tính tự miễn dịch, và hiện cũng chưa rõ nguyên nhân. Như tên gọi – loại nguy cấp, loại này cũng là loại có thời gian phát bệnh nhanh nhất. Chỉ sau vài ngày với các triệu chứng của cảm cúm như đau họng, sốt, đau bụng,v.v.. là bệnh đã khởi phát. Chỉ sau đó vài ngày, insulin sẽ biến mất và kèm theo rối loạn ý thức có thể xuất hiện.</p>\r\n<p dir=\"ltr\">Đối với những người được xác định là bị bệnh tiểu đường, việc tiếp theo là “chẩn đoán loại bệnh”, cụ thể là dựa vào nguyên nhân bệnh để khám và chẩn đoán phân biệt bệnh tiểu đường tuýp 1 hay tuýp 2.</p>\r\n<p dir=\"ltr\">Mặc dù có nhiều phương pháp để kiểm tra đó có phải tiểu đường tuýp 1 không, nhưng yếu tố quyết định cuối cùng để chẩn đoán tuýp 1 là xem trong cơ thể có tồn tại protein được gọi là “tự kháng thể” sẽ nhầm lẫn tự tấn công và phá hủy tế bào. Các tự kháng thể phổ biến là “kháng thể GAD”, “kháng thể tế bào đảo tụy ”, “tự kháng thể insulin ”, “kháng thể IA-2”,…. Một khi các kháng thể này được tìm thấy, khả năng mắc bệnh tiểu đường tuýp 1 rất cao.</p>\r\n<p dir=\"ltr\">Mặt khác, có 20% bệnh tiểu đường tuýp 1 không phải do mất khả năng kiểm soát miễn dịch và tự kháng thể không được tạo ra. Nói cách khác, trong số 5 người bị bệnh tiểu đường tuýp 1 thì có 1 người mắc bệnh do tế bào tuyến tụy bị phá hủy không rõ nguyên nhân và insulin không được tiết ra. Theo tiêu chuẩn chẩn đoán bệnh tiểu đường tuýp 1 khởi phát cấp tính (2012), trường hợp dù không tìm thấy tự kháng thể nhưng insulin không được tiết ra từ tuyến tụy hoặc có tiết ra nhưng lượng không đủ thì có khả năng cao là bệnh tiểu đường tuýp 1.</p>\r\n<p dir=\"ltr\">Insulin tiết ra từ tuyến tụy được chuyển hóa ngay sau khi được tiết ra, do đó không thể lấy mẫu máu và đo được. Khi insulin được tiết ra từ tuyến tụy, lượng insulin tiết ra có thể được ước tính bằng cách đo thành phần “C-peptide” đồng thời tiết ra với lượng giống nhau. Chất này thường được sử dụng vì nó có thể đo được trong nước tiểu. Đo lượng C-peptide trong nước tiểu và máu, nếu kết quả cho giá trị cực kỳ thấp, bệnh nhân có thể được chẩn đoán là bệnh tiểu đường tuýp 1.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-tuyp-1-la-gi-02-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Để kiểm tra xem insulin có được tiết ra từ tuyến tụy hay không, tiến hành xét nghiệm gọi là “xét nghiệm dung nạp glucose đường uống (OGTT)”. Các bác sĩ sử dụng kết quả của xét nghiệm này như là một căn cứ để chẩn đoán đó có phải là bệnh tiểu đường loại 1 không.</p>\r\n<p dir=\"ltr\">Khi kiểm tra để xác nhận bệnh tiểu đường tuýp 1, tuổi tác cũng là yếu tố cần tham khảo. Nhiều người bị bệnh tiểu đường tuýp 1 được cho biết bệnh đã khởi phát từ nhỏ đến khi trưởng thành. Cầu thủ Sugiyama Arata của giải J League đã bị bệnh tiểu đường tuýp 1 khi 23 tuổi và là cầu thủ được biết đến vẫn hoạt động như một cầu thủ bóng đá chuyên nghiệp dù bị bệnh tiểu đường. Người ta nói rằng nhiều bệnh tiểu đường tuýp 1 thường khởi phát ở những người trẻ tuổi. Ngược lại, bệnh tiểu đường tuýp 2 thường khởi phát ở những người béo phì và trung tuổi. Tuy nhiên, cũng có những người bị bệnh tiểu đường tuýp 1 khi trung tuổi, do đó không thể phân loại bệnh tiểu đường theo độ tuổi.</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường có lộ trình điều trị khác nhau tùy thuộc vào đó là bệnh tiểu đường tuýp 1 hay tuýp 2. Ngoài ra, lối sống của bệnh nhân cũng sẽ thay đổi theo cách điều trị khác nhau. Khám bệnh tiểu đường và chẩn đoán xem đó là tuýp 1 hay tuýp 2 là rất quan trọng trong việc tiến hành điều trị và sống cùng với bệnh.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>3. Điều trị tiểu đường tuýp 1: tại sao insulin lại được ưu tiên?</strong></h2>\r\n<p dir=\"ltr\">* Những điểm cần chú ý:</p>\r\n<p dir=\"ltr\">+ Vì tiểu đường tuýp 1 thiếu insulin nên điều trị bằng tiêm insulin là bắt buộc.</p>\r\n<p dir=\"ltr\">+ Các phương pháp mới như tiêm insulin dưới da liên tục, vòng theo dõi đường huyết giúp kiểm soát đường huyết cũng đã khả dụng.</p>\r\n<p dir=\"ltr\">+ Nhờ các phương pháp điều trị tiên tiến hiện nay, người mắc tiểu đường tuýp 1 cũng có thể sống cuộc sống như người bình thường.</p>\r\n<p dir=\"ltr\">– Giải pháp duy nhất chữa tiểu đường tuýp 1 là tiêm bổ sung insulin</p>\r\n<p dir=\"ltr\">Mọi người thường nghĩ rằng nếu điều trị tiểu đường thì cố gắng chọn những phương pháp đơn giản và hiệu quả nhất. Chính vì vậy cũng đã có người tập trung vào thuốc uống điều trị tiểu đường tuýp 1. Tuy nhiên tiểu đường tuýp 1 là loại bệnh mà chắc chắn cơ thể không đủ insulin để dùng, cho nên mọi người không thể lựa chọn thuốc uống được. Căn bản của trị liệu tiểu đường loại 1 là tự tiêm insulin.</p>\r\n<p dir=\"ltr\">Hiện tại trong điều trị tiểu đường tuýp 2, các loại thuốc uống có tác dụng tăng khả năng tiết ra insulin của tụy, phân giải hoặc trì hoãn tác dụng của đường (glucose). Theo đó, tăng hiệu quả của insulin trong cơ thể và làm giảm các triệu chứng của bệnh tiểu đường. Nói 1 cách đơn giản hơn là giúp cho insulin được cơ thể tạo ra hoạt động một cách hiệu quả hơn.</p>\r\n<p dir=\"ltr\">Thế nhưng tiểu đường tuýp 1 là căn bệnh mà tế bào tụy tiết ra insulin bị phá hủy. Cơ thể không có insulin (không tạo ra insulin được) nên giải pháp duy nhất là phải tiêm bổ sung insulin.</p>\r\n<p dir=\"ltr\">Khi ăn, cơ thể sẽ tiết ra một lượng insulin thích hợp để giúp lượng đường huyết duy trì ở mức ổn định, làm cơ thể khỏe mạnh. Thuốc insulin trong điều trị tiểu đường lấy điều này làm căn bản, tức là đưa cơ thể về trạng thái tiết insulin giống như người khỏe mạnh bình thường.</p>\r\n<p dir=\"ltr\">– Tiêm insulin 1 ngày từ 1 đến 2 lần</p>\r\n<p dir=\"ltr\">Để cơ thể có thể tiết insulin ra từng chút một giống như bình thường, người ta sẽ sử dụng một loại thuốc insulin có hiệu quả tương đối dài (loại tác dụng trung gian hoặc loại tác dụng kéo dài) được tiêm 1 ngày từ 1 đến 2 lần. Mặt khác, khi ăn uống, để khôi phục khả năng tiết insulin ngay lập tức (tiết bổ sung) người ta sẽ tiêm một loại insulin tác dụng nhanh. Nó còn được gọi là liệu pháp insulin tích cực.</p>\r\n<p dir=\"ltr\">Một vấn đề gặp phải khi tiêm insulin là phần da chỗ tiêm sẽ trở nên cứng nếu tiêm lặp lại nhiều lần ở cùng một chỗ. Ngoài ra việc theo dõi một ngày tiêm bao nhiêu lần cũng rất khó. Để khắc phục những vấn đề này, có một phương pháp “truyền insulin dưới da liên tục (CSII)” mà không cần tiêm nhiều lần. Cách này cũng được gọi là “bơm insulin”, là một máy bơm gắn vào cơ thể và duy trì tiêm insulin dưới da tương đương với lượng tiết cơ bản từ một kim tiêm ở bụng. Hơn nữa, có thể tiêm insulin tương ứng với lượng tiết bổ sung bằng cách thay đổi tốc độ của máy bơm trước mỗi bữa ăn. Tần suất thay thế máy bơm chỉ vài lần một vài ngày, giúp bệnh nhân tránh được việc phải tiêm nhiều lần trong 1 ngày.</p>\r\n<p dir=\"ltr\">Cách bơm insulin này gần với nhịp độ tiết insulin tự nhiên của cơ thể, nên có điểm lợi là bệnh nhân có thể ăn uống và vận động thoải mái. Ngoài ra, người ta cho rằng việc sử dụng bơm insulin giúp làm giảm tỷ lệ bị các biến chứng tiểu đường như bệnh võng mạc tiểu đường, bệnh thần kinh tiểu đường, bệnh thận do tiểu đường.</p>\r\n<p dir=\"ltr\">Khi tự tiêm insulin, bệnh nhân cần tự đo lượng đường trong máu của mình. Việc đo này cũng rất khó khăn đối với bệnh nhân. Dùng cây kim nhỏ chích ở đầu ngón tay để máu chảy ra và đo lượng đường trong máu. Đây là cách đo đem lại đau đớn cho bệnh nhân khi phải làm nhiều lần trong ngày. Do đó, có thể đo đường huyết bằng cách đo lượng đường của dịch kẽ dưới da mà không cần lấy máu. Đeo thiết bị cảm biến trên cánh tay và lượng đường trong máu có thể được đo liên tục trong 14 ngày. Có thể đo lượng đường trong máu liên tục mà không cần đâm kim nhiều lần.</p>\r\n<p dir=\"ltr\">Ngoài ra còn có một phương pháp điều trị được gọi là liệu pháp SAP kết hợp truyền insulin dưới da bền liên tục với một màn hình quan sát đường huyết liên tục để kiểm soát đường huyết thích hợp hơn.</p>\r\n<p dir=\"ltr\">– Phương pháp ghép tụy</p>\r\n<p dir=\"ltr\">Bên cạnh đó còn có phương pháp ghép tụy cho bệnh nhân tiểu đường tuýp 1 mà không thể kiểm soát được đường huyết. Nhiều bệnh nhân còn bị rối loạn chức năng thận, vì vậy tụy và thận thường được cấy ghép cùng một lúc. Sau khi cấy ghép, bệnh nhân phải dùng thuốc ức chế miễn dịch, nhưng có báo cáo rằng tuổi thọ của bệnh nhân được cấy ghép dài hơn bệnh nhân không cấy ghép.</p>\r\n<p dir=\"ltr\">Ngoài cấy ghép tuyến tụy, còn có một phương pháp được gọi là cấy ghép đảo tụy. Trong tuyến tụy, các mô tạo insulin được gọi là đảo tụy và phần này chỉ chiếm 1% tuyến tụy. Phẫu thuật cấy ghép toàn bộ tuyến tụy đòi hỏi gây mê toàn thân và gánh nặng lên cơ thể tăng lên. Phương pháp chỉ loại bỏ các đảo tụy có vai trò tạo insulin từ trong tuyến tụy và cấy ghép từ mạch máu của gan được gọi là cấy ghép đảo tụy. So với ghép tụy, phương pháp này giúp giảm gánh nặng cho bệnh nhân.</p>\r\n<p dir=\"ltr\">Ghép tụy và ghép đảo tụy chỉ có thể được thực hiện tại một số các cơ sở y tế, và số lượng bệnh nhân được phẫu thuật cũng bị giới hạn.</p>\r\n<p dir=\"ltr\">Các nghiên cứu khác về tuyến tụy nhân tạo đang được tiến hành. Ví dụ: ngày 27 tháng 5 năm 2016 đã công bố chính sách phê duyệt trong trường hợp bệnh nhân thỏa mãn điều kiện ghép tế bào tuyến tụy của lợn với người mà Bộ Y tế, Lao động và Phúc lợi Nhật Bản đã cấm từ trước đến nay. Do đó, số lượng người có thể điều trị ghép cho đến nay có thể được cải thiện trong tương lai gần.</p>\r\n<p dir=\"ltr\">Việc phát triển thành công “cấy ghép tế bào đảo tụy lợn vi nang được bọc trong một bao nhỏ gọi là “vi nang” cũng đã được công bố. Bằng cách đó, người ta cho rằng sự viêm nhiễm và phản ứng loại thải miễn dịch gây ra bởi sự cấy ghép tế bào động vật lên con người có thể giảm đáng kể.</p>\r\n<p dir=\"ltr\">Ngoài ra, vì phần lớn bệnh tiểu đường tuýp 1 là do bất thường của hệ thống miễn dịch nên đã có “liệu pháp miễn dịch” nhằm điều trị những bất thường này của hệ thống miễn dịch. Mặc dù các loại thuốc có tác dụng trên một số hệ thống miễn dịch đã được thử nghiệm nhưng không đạt đến hiệu quả trong việc chữa trị bệnh tiểu đường tuýp 1.</p>\r\n<p dir=\"ltr\">Hiện tại mặc dù chưa công bố nhưng nghiên cứu về tái sinh chính tuyến tụy cũng được khuyến khích. Có những nghiên cứu về tạo và cấy chính tuyến tụy trong cơ thể heo bằng cách sử dụng tế bào ES và tế bào iPS và nghiên cứu để tái tạo tuyến tụy trong cơ thể bằng cách kích thích mạng nơ-ron.</p>\r\n\r\n<h2 dir=\"ltr\">4. Tổng kết</h2>\r\n<p dir=\"ltr\">Trong tổng số người bị tiểu đường chỉ có dưới 5% là thuộc tiểu đường tuýp 1 hiếm gặp, các triệu chứng, nguyên nhân, chẩn đoán và điều trị đã được nêu trong bài. Tiểu đường tuýp 1 có thể xảy ra nhanh chóng ở những người trẻ, nhưng cũng có thể phát triển rất chậm ở người già. Dù là đối tượng người bệnh nào thì việc thiếu hụt insulin là không thay đổi, tiểu đường tuýp 1 không phải bệnh có thể trị liệu bằng ăn uống, tập luyện được mà cần điều trị bằng insulin. Khi bệnh càng phát triển thì việc tiêm insulin và theo dõi đường huyết càng cần diễn ra liên tục hơn, điều này có thể gây khó khăn cho người bệnh. Tuy nhiên hiện nay, y học càng ngày càng hiện đại với các dụng cụ tiêm insulin và đo đường huyết liên tục đã giúp cho người bệnh có thể tự kiểm soát, chữa trị bệnh dễ dàng hơn.</p>', 'Tiểu đường tuýp 1 là gì?', '', 'publish', 'closed', 'closed', '', 'tieu-duong-tuyp-1-la-gi', '', '', '2020-06-15 03:03:10', '2020-06-15 03:03:10', '', 0, 'http://localhost:8080/yentheme/?p=110', 0, 'post', '', 0),
(111, 1, '2020-06-10 04:34:55', '2020-06-10 04:34:55', '', 'Tong-quan-kien-thuc-ve-benh-tieu-duong-02', '', 'inherit', 'open', 'closed', '', 'tong-quan-kien-thuc-ve-benh-tieu-duong-02', '', '', '2020-06-10 04:34:55', '2020-06-10 04:34:55', '', 110, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/Tong-quan-kien-thuc-ve-benh-tieu-duong-02.jpg', 0, 'attachment', 'image/jpeg', 0),
(112, 1, '2020-06-10 04:34:59', '2020-06-10 04:34:59', '', 'Tiểu đường tuýp 1 là gì?', '', 'inherit', 'closed', 'closed', '', '110-revision-v1', '', '', '2020-06-10 04:34:59', '2020-06-10 04:34:59', '', 110, 'http://localhost:8080/yentheme/2020/06/10/110-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2020-06-10 04:38:58', '2020-06-10 04:38:58', '<p dir=\"ltr\">Tiểu đường tuýp 2 là căn bệnh khá phổ biến tại Việt Nam. Bệnh này nếu không được chẩn đoán sớm để điều trị kịp thời sẽ để lại biến chứng nghiêm trọng. Bạn cần nắm rõ tiểu đường tuýp 2 là gì , nguyên nhân gây ra tiểu đường tuýp 2, triệu chứng bệnh để bảo vệ bản thân và người thân xung quanh.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>1. Tiểu đường tuýp 2 là gì?</strong></h2>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 (tiểu đường type 2) là loại bệnh tiểu đường trong đó lượng đường trong máu bệnh nhân luôn cao do thiếu tác dụng của insulin. Bệnh tiểu đường được chia thành tiểu đường tuýp 1, tuýp 2, tiểu đường thai kỳ và các loại khác (tính đến tháng 3 năm 2018). Trong số đó, bệnh tiểu đường tuýp 2 là loại bệnh phổ biến nhất và chiếm hơn 90% trong bệnh tiểu đường nói chung. Phần lớn bệnh tiểu đường tuýp 2 khởi phát do chế độ ăn uống không lành mạnh, thiếu vận động hoặc do tình trạng béo phì và còn do các yếu tố môi trường.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-01-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Bệnh tiểu đường là một căn bệnh âm thầm tiến triển mà bệnh nhân thường không biết và không có triệu chứng ở giai đoạn đầu. Tuy nhiên, nếu tình trạng bệnh tiểu đường tiếp tục tiến triển có thể dẫn đến xuất hiện 3 biến chứng lớn là bệnh võng mạc tiểu đường, bệnh thận tiểu đường, bệnh thần kinh ngoại biên (bệnh thần kinh tiểu đường). Bên cạnh đó, bệnh còn tăng nguy cơ dẫn đến các bệnh do tổn thương đại mạch như nhồi máu cơ tim và nhồi máu não.</p>\r\n<p dir=\"ltr\">Ngoài ra, một số bệnh khác như bệnh nha chu, suy giảm trí nhớ, ung thư, bệnh loãng xương cũng dễ xảy ra do bệnh tiểu đường. Bệnh nha chu được cho là có liên quan đến sự chuyển biến xấu đi của bệnh tiểu đường.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>2. Nguyên nhân gây ra tiểu đường tuýp 2 </strong></h2>\r\n<p dir=\"ltr\">Trong bệnh tiểu đường loại 2, có những trường hợp là do lượng tiết insulin bị giảm và cũng có trường hợp do chức năng insulin bị suy giảm (kháng insulin). Nguyên nhân của bệnh là do yếu tố di truyền và các yếu tố môi trường.</p>\r\n<p dir=\"ltr\">+ Yếu tố di truyền</p>\r\n<p dir=\"ltr\">Đó là gen di truyền liên quan đến việc tiết insulin hoặc chức năng tuyến tụy và người ta nói rằng khi những bất thường về gen di truyền này chồng lên nhau, bệnh tiểu đường tuýp 2 có nhiều khả năng khởi phát hơn.</p>\r\n<p dir=\"ltr\">+ Yếu tố môi trường</p>\r\n<p dir=\"ltr\">Yếu tố môi trường là sự gia tăng lượng hấp thụ chất béo do béo phì, thiếu vận động và thói quen ăn uống theo phong cách Tây u. Ngoài ra còn liên quan đến chế độ ăn uống không cân bằng dinh dưỡng và thói quen ăn uống không lành mạnh.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-02-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Trong những năm gần đây, người ta đã chỉ ra rằng việc thiếu lượng hấp thụ chất xơ và magie do sự thay đổi chế độ ăn uống sau chiến tranh (như giảm mạnh lượng hấp thụ lúa mạch và ngũ cốc) có ảnh hưởng lớn đến sự khởi phát của bệnh tiểu đường tuýp 2.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>3. Triệu chứng của bệnh tiểu đường tuýp 2</strong></h2>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 hầu như không có triệu chứng rõ ràng và bệnh nhân cũng không nhận biết được bệnh. Khi bệnh chuyển biến xấu đi và bệnh nhân rơi vào tình trạng tăng đường huyết ketosis và nhiễm toan ceton, sẽ xuất hiện các triệu chứng như khát nước, uống nhiều nước, đa niệu, cảm giác mệt mỏi và sụt giảm cân nặng…</p>\r\n<p dir=\"ltr\">+ Đa niệu</p>\r\n<p dir=\"ltr\">Khi lượng đường huyết cao, cơ thể sẽ đào thải bớt đường qua nước tiểu. Khi đó thận sẽ kéo nước từ trong cơ thể để pha loãng nước tiểu, khiến khối lượng nước tiểu tăng lên. Đây là lý do làm cho người bệnh thường xuyên đi tiểu.</p>\r\n<p dir=\"ltr\">+ Khát nước</p>\r\n<p dir=\"ltr\">Việc tiểu nhiều sẽ làm cơ thể tăng nhu cầu sử dụng nước để bù lại lượng nước đã mất. Điều này sẽ kích thích làm người bệnh luôn cảm thấy khát nước và uống nhiều nước hơn.</p>\r\n<p dir=\"ltr\">+ Cảm thấy kiệt sức</p>\r\n<p dir=\"ltr\">Cơ thể người bệnh thường xuyên cảm thấy mệt mỏi</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-03-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Da khô và ngứa</p>\r\n<p dir=\"ltr\">+ Cảm giác của chân tay bị giảm, thỉnh thoảng thấy hơi nhói đau</p>\r\n<p dir=\"ltr\">+ Hay bị nhiễm trùng</p>\r\n<p dir=\"ltr\">+ Đi tiểu nhiều lần</p>\r\n<p dir=\"ltr\">+ Mờ mắt</p>\r\n<p dir=\"ltr\">+ Có vấn đề về chức năng tình dục</p>\r\n<p dir=\"ltr\">+ Khó lành sẹo hay các vết bầm</p>\r\n<p dir=\"ltr\">+ Hay có các cảm giác rất đói hoặc rất khát</p>\r\n<p dir=\"ltr\">Khi bất cứ ai trong gia đình có triệu chứng trên, có thể họ đã mắc bệnh tiểu đường hay tiền tiểu đường. Chính vì vậy bạn hãy đi khám ngay để kiểm soát bệnh tốt hơn nhé. Ngoài ra mọi người cũng nên chú ý tới chế độ ăn uống thích hợp, chăm chỉ tập luyện thể dục thể thao, và cũng nên chú ý tới nguy cơ béo phì nữa nhé.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>4. Biến chứng</strong></h2>\r\n<p dir=\"ltr\">Nếu bệnh tiểu đường tuýp 2 tiếp tục tiến triển sẽ tăng nguy cơ xuất hiện các biến chứng khác nhau. 3 biến chứng lớn là bệnh võng mạc tiểu đường, bệnh thận tiểu đường, tổn thương thần kinh (bệnh thần kinh tiểu đường), ngoài ra còn gây ra các bệnh lây nhiễm khác nhau do khả năng miễn dịch của cơ thể giảm và các bệnh như nhồi máu cơ tim, nhồi máu não, xơ vữa động mạch tắc nghẽn do xơ vữa động mạch.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>5. Xét nghiệm/Chẩn đoán</strong></h2>\r\n<p dir=\"ltr\">Kết quả xét nghiệm máu được sử dụng để chẩn đoán bệnh tiểu đường tuýp 2.</p>\r\n<p dir=\"ltr\">+ Lượng đường trong máu</p>\r\n<p dir=\"ltr\">Đầu tiên sẽ kiểm tra lượng đường trong máu của bệnh nhân. Lượng đường trong máu có thể được chia thành đường huyết lúc đói và đường huyết ngẫu nhiên tùy thuộc vào thời gian của bữa ăn. Đường huyết lúc đói ≥126 mg/dl hoặc đường huyết ngẫu nhiên ≥200 mg/dl là một trong những điều kiện để chẩn đoán bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">+ HbA1c</p>\r\n<p dir=\"ltr\">Giá trị của HbA1c (hemoglobin A1c (NGSP)) cũng quan trọng trong chẩn đoán bệnh tiểu đường và nếu giá trị này ≥6,5% (giá trị NGSP) thì bệnh nhân được chẩn đoán mắc bệnh tiểu đường. Giá trị HbA1c là giá trị phản ánh chỉ số đường huyết trung bình trong 1-2 tháng trước. Điều này là do glucose trong máu liên kết với moglobin và tuổi thọ trung bình của hồng cầu là 120 ngày (khoảng 4 tháng).</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-05-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Xét nghiệm dung nạp glucose 75g đường uống (75g OGTT)</p>\r\n<p dir=\"ltr\">Ngoài ra, xét nghiệm dung nạp glucose 75g đường uống (75g OGTT) có thể được thực hiện để kiểm tra sự thay đổi lượng đường trong máu. Trong xét nghiệm này, sau khi cho bệnh nhân hấp thụ 75g glucose, tiến hành lấy máu theo thời gian và đo lượng đường trong máu.</p>\r\n<p dir=\"ltr\">Đối với những người khỏe mạnh, lượng đường trong máu đạt đến giá trị tối đa khoảng 30 phút sau khi uống, và trở về mức dưới giá trị tiêu chuẩn hoặc sau khoảng 2 giờ. Tuy nhiên, trong trường hợp của bệnh tiểu đường (loại bệnh tiểu đường), lượng đường trong máu không giảm hoàn toàn và vẫn còn cao. Giá trị đường huyết sau 2 giờ từ ≥200 mg/dl là một trong những điều kiện chẩn đoán bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 được chẩn đoán toàn diện bằng cách kết hợp các kết quả của các xét nghiệm này.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>6. Điều trị bệnh tiểu đường tuýp 2</strong></h2>\r\n<p dir=\"ltr\">Cơ sở trong điều trị bệnh tiểu đường tuýp 2 là liệu pháp ăn uống và liệu pháp vận động. Việc điều trị như cải thiện chế độ ăn uống, vận động vừa phải và kết hợp dùng thuốc là rất quan trọng.</p>\r\n<p dir=\"ltr\">+ Liệu pháp ăn uống</p>\r\n<p dir=\"ltr\">Bệnh nhân cần biết lượng năng lượng thích hợp để hấp thụ dựa trên lượng hoạt động thể chất và cân nặng, từ đó cố gắng tạo một chế độ ăn uống cân bằng theo hướng dẫn. Đối với ngũ cốc nên ăn ngũ cốc nguyên hạt.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-06-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Liệu pháp vận động</p>\r\n<p dir=\"ltr\">Bệnh nhân cần thường xuyên vận động vừa phải theo hướng dẫn và đi bộ đầy đủ mỗi ngày. Điều quan trọng là cần vận động cơ thể thường xuyên.</p>\r\n<p dir=\"ltr\">+ Điều trị bằng thuốc</p>\r\n<p dir=\"ltr\">Trường hợp bệnh nhân không thể kiểm soát lượng đường trong máu bằng liệu pháp ăn uống và liệu pháp vận động, tiến hành điều trị bằng thuốc. Đầu tiên, sẽ sử dụng một hoặc nhiều loại thuốc uống điều trị tiểu đường khác với insulin tùy theo tình trạng bệnh nhân. Trường hợp sử dụng thuốc uống điều trị tiểu đường nhưng không có hiệu quả, bệnh nhân có thể kết hợp điều trị bằng insulin hoặc chuyển sang chỉ điều trị bằng insulin.</p>\r\n<p dir=\"ltr\">Dù là bệnh tiểu đường loại nào thì quan trọng nhất trong điều trị là ở chính người bệnh chứ không ai khác. Hãy tích lũy, thu thập thông tin để có thật nhiều hiểu biết về bệnh tiểu đường tuýp 2, và càng có nhiều kiến thức về tiểu đường thì bạn càng dễ dàng quản lý bệnh hơn.</p>', 'Tiểu đường tuýp 2 là gì?', '', 'publish', 'closed', 'closed', '', 'tieu-duong-tuyp-2-la-gi', '', '', '2020-06-12 11:59:49', '2020-06-12 11:59:49', '', 0, 'http://localhost:8080/yentheme/?p=113', 0, 'post', '', 0),
(114, 1, '2020-06-10 04:38:58', '2020-06-10 04:38:58', '', 'Tiểu đường tuýp 2 là gì?', '', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2020-06-10 04:38:58', '2020-06-10 04:38:58', '', 113, 'http://localhost:8080/yentheme/2020/06/10/113-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2020-06-10 04:39:22', '2020-06-10 04:39:22', '<p dir=\"ltr\">Về mặt lý thuyết, tất cả mọi người đều có thể mắc bệnh tiểu đường. Tuy nhiên, những đối tượng sau đây có nguy cơ mắc bệnh tiểu đường cao hơn:</p>\r\n<p dir=\"ltr\"><strong>Với bệnh tiểu đường tuýp 1: </strong></p>\r\n<p dir=\"ltr\">- Những người có bố, mẹ hoặc cả bố và mẹ bị tiểu đường tuýp 1(di truyền) - Yếu tố địa lý: các nghiên cứu cho thấy đi từ đường xích đạo, những người càng sống xa đường xích đạo tỷ lệ mắc tiểu đường tuýp 1 càng cao. Phần lan, Sardinia là những nước có tỷ lệ mắc TĐ tuýp 1 cao.</p>\r\n<p dir=\"ltr\">- Các yếu tố nguy cơ khác:</p>\r\n<p dir=\"ltr\">+ Nhiễm virus: nhiễm các virus như Epstein-Barr; Coxsackie virus; virus quai bị; cytomegalo virus…(virus gây tổn thương các tế bào bêta của tuỵ đảo, nơi sản xuất insulin)</p>\r\n<p dir=\"ltr\">+ Các nghiên cứu cho thấy: Những người bị thiếu vitamin D, omega 3 có tỷ lệ mắc tiểu đường tuýp 1 cao hơn.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/nhung-ai-co-nguy-co-mac-tieu-duong-01.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Những trẻ em có mẹ quá trẻ, trong quá trình mang thai có tiền sản giật, bị nhiễm trùng hô hấp cũng có tỷ lệ mắc tiểu đường tuýp 1 cao hơn.</p>\r\n<p dir=\"ltr\"><strong>Với bệnh tiểu đường tuýp 2: </strong></p>\r\n<p dir=\"ltr\">- Những người trên 40 tuổi</p>\r\n<p dir=\"ltr\">- Những người có thừa cân, béo phì, đặc biệt chỉ số BMI trên 30 và có tỷ lệ mỡ nội tạng cao, đặc biệt có gan nhiễm mỡ</p>\r\n<p dir=\"ltr\">- Những người trong gia đình có bố, mẹ anh chị em người bị tiểu đường (yếu tố gia đình)</p>\r\n<p dir=\"ltr\">- Những người bị bệnh cao huyết áp và các bệnh lý chuyển hoá khác như gout, rối loạn chuyển hóa mỡ máu.</p>\r\n<p dir=\"ltr\">-  Những người có thói quen trì trệ, lười vận động và lối sống không lành mạnh.</p>\r\n<p dir=\"ltr\">- Phụ nữ đã có tiền sử tiểu đường thai kỳ</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/nhung-ai-co-nguy-co-mac-tieu-duong-02.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\"><strong>Tiểu đường thai kỳ: </strong></p>\r\n<p dir=\"ltr\">- Những phụ nữ thừa cân, béo phì có thai</p>\r\n<p dir=\"ltr\">- Phụ nữ mang thai sau tuổi 35</p>\r\n<p dir=\"ltr\">- Những phụ nữ đã từng bị tiểu đường thai kỳ trong những lần mang thai trước</p>\r\n<p dir=\"ltr\">- Những phụ nữ mà bố, mẹ, anh chị em ruột trong gia đình bị mắc tiểu đường tuýp 2</p>\r\n<p dir=\"ltr\">- Yếu tố chủng tộc: những phụ nữ gốc latinh; người mỹ gốc phi, người mỹ bản địa…có tỷ lệ tiểu đường thai kỳ cao hơn.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/nhung-ai-co-nguy-co-mac-tieu-duong-03.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Đang có sự thay đổi !</p>\r\n<p dir=\"ltr\">Mặc dù những nhóm người trên thuộc nhóm nguy cơ cao mắc các loại tiểu đường khác nhau. Tuy nhiên, hiện nay do lối sống thay đổi, do tình trạng dinh dưỡng mất cân bằng, do tình trạng thừa cân béo phì, tình trạng rối loạn chuyển hóa mỡ máu gia tăng nhanh chóng, stress trong công việc và cuộc sống…đã làm thay đổi đối tượng mắc tiểu đường: Tỷ lệ mắc tiểu đường tuýp 2 càng ngày càng trẻ hơn (tỷ lệ người dưới 40 tuổi mắc tiểu đường tuýp 2 càng ngày càng nhiều, kể cả trẻ em dưới 18 tuổi). Tỷ lệ phụ nữ mang thai có tiểu đường thai kỳ cũng gia tăng.</p>\r\n<p dir=\"ltr\">.</p>', 'Những ai có nguy cơ mắc tiểu đường?', '', 'publish', 'closed', 'closed', '', 'tieu-duong-o-tre-em', '', '', '2020-06-15 02:25:06', '2020-06-15 02:25:06', '', 0, 'http://localhost:8080/yentheme/?p=115', 0, 'post', '', 0),
(116, 1, '2020-06-10 04:39:22', '2020-06-10 04:39:22', '', 'Tiểu đường ở trẻ em?', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2020-06-10 04:39:22', '2020-06-10 04:39:22', '', 115, 'http://localhost:8080/yentheme/2020/06/10/115-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2020-06-10 04:39:38', '2020-06-10 04:39:38', '', 'HAB-tieuduong', '', 'inherit', 'open', 'closed', '', 'hab-tieuduong-2', '', '', '2020-06-10 04:39:38', '2020-06-10 04:39:38', '', 115, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/HAB-tieuduong-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(118, 1, '2020-06-10 04:40:20', '2020-06-10 04:40:20', '', 'Những ai có nguy cơ mắc tiểu đường?', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2020-06-10 04:40:20', '2020-06-10 04:40:20', '', 115, 'http://localhost:8080/yentheme/2020/06/10/115-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2020-06-10 04:40:51', '2020-06-10 04:40:51', '<p dir=\"ltr\">Phần lớn tiểu đường gặp ở trẻ em chủ yếu là tiểu đường tuýp 1 (còn gọi là tiểu đường vị thành niên). Tuy nhiên, ngày nay xu hướng trẻ em mắc tiểu đường tuýp 2 càng ngày càng cao.</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 1 (còn gọi là tiểu đường vị thành niên): được xếp vào nhóm bệnh tự miễn, có nghĩa là do hệ thống miễn dịch tấn công nhầm vào các bộ phận của cơ thể. Cụ thể ở đây là hệ thống miễn dịch xác định sai mục tiêu và hướng đến các tế bào beta sản xuất insulin trong tuyến tụy. Do đó tuyến tụy không thể sản xuất được insulin. Vì không có insulin nên đường không thể đi từ máu vào tế bào và lượng đường trong máu sẽ tăng cao nếu không được điều trị.</p>\r\n<p dir=\"ltr\"><strong>Dấu hiệu của tiểu đường tuýp 1 ở trẻ em:</strong></p>\r\n<p dir=\"ltr\">Hay khát nước, đi tiểu nhiều; Hay đói; Sụt giảm cân không mong muốn; Mệt mỏi, ngại hoạt động; Yếu đuối; Khó chịu, hay cáu gắt, mè nheo, thay đổi tâm trạng; Giảm thị lực</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 2 : hiếm gặp ở trẻ em, xảy ra khi cơ thể mất khả năng đáp ứng lại insulin (kháng insulin). Trong tình trạng kháng insulin, gan, cơ và các tế bào mỡ giảm khả năng sử dụng insulin, tác động này làm cản trở mang đường vào trong các tế bào của cơ thể. Do đó, cơ thể sẽ phải sản xuất nhiều insulin hơn để thu nhận đường vào trong tế bào. Tuyến tụy cố gắng đáp ứng nhu cầu bằng cách gia tăng sản xuất thêm insulin. Tuy nhiên qua thời gian, tuyến tụy sẽ không thể đáp ứng được đầy đủ sản xuất insulin, khi có sự gia tăng mức đường trong máu.</p>\r\n<p dir=\"ltr\">Ngày nay, do lối sống, do dinh dưỡng thiếu hợp lý, tình trạng thừa cân béo phì càng ngày càng gia tăng ở trẻ em, do đó tỷ lệ tiểu đường ở trẻ em cũng có xu hướng tăng lên. Điều này cảnh báo các bậc cha mẹ cần kiểm soát tốt chế độ ăn uống, vận động và cân nặng của trẻ.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-01.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Dấu hiệu của tiểu đường tuýp 2 ở trẻ em: hay gặp ở những trẻ thừa cân, béo phì, ít hoạt động; Đi tiểu nhiều hơn đặc biệt vào ban đêm; Mệt mỏi; Giảm cân không rõ nguyên nhân; Khát nước, cơn khát tăng dần; Ngứa xung quanh bộ phận sinh dục (có thể do nhiễm nấm men); Các vết thương, vết loét khó liền, liền chậm; Mờ mắt, thị lực giảm.</p>\r\n<p dir=\"ltr\"><strong>Tiến triển, biến chứng của tiểu đường ở trẻ em: </strong></p>\r\n<p dir=\"ltr\">Tiểu đường ở trẻ em có thể ảnh hưởng đến nhiều cơ quan lớn trong cơ thể, bao gồm tim, mạch máu, dây thần kinh, mắt và thận. Giữ lượng đường trong máu gần mức bình thường hầu hết thời gian có thể làm giảm nguy cơ biến chứng rất nhiều.</p>\r\n<p dir=\"ltr\">Các biến chứng lâu dài của bệnh tiểu đường ở trẻ phát triển dần dần, qua nhiều năm. Nếu phát triển bệnh tiểu đường sớm, ít kiểm soát lượng đường trong máu thì nguy cơ biến chứng cao hơn. Cuối cùng, các biến chứng bệnh tiểu đường có thể vô hiệu hóa hoặc thậm chí đe dọa tính mạng.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-02.png\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tim và bệnh mạch máu. Bệnh tiểu đường làm tăng đáng kể nguy cơ tim mạch, bao gồm bệnh động mạch vành với đau ngực (đau thắt ngực), đau tim, đột quỵ, thu hẹp động mạch (xơ vữa động mạch) và huyết áp cao. Trong thực tế, khoảng 65 phần trăm những người đã chết vì bệnh tiểu đường do một số loại bệnh mạch máu hay tim, theo Hiệp hội tim mạch Mỹ.</p>\r\n<p dir=\"ltr\">Thần kinh hư hại (neuropathy). Dư thừa đường có thể làm tổn thương thành của các mạch máu nhỏ (mao mạch) nuôi dưỡng các dây thần kinh, đặc biệt là ở chân. Điều này có thể gây ngứa, tê, nóng hoặc bị đau thường bắt đầu các ngón chân hoặc ngón tay và dần dần lan lên trên. Khó kiểm soát lượng đường trong máu có thể làm cho mất cảm giác ở các chi bị ảnh hưởng. Thiệt hại các dây thần kinh kiểm soát sự tiêu hóa có thể gây ra vấn đề với buồn nôn, tiêu chảy, nôn mửa hoặc táo bón.</p>\r\n<p dir=\"ltr\">Tổn thương thận: Thận có chứa hàng triệu mạch máu nhỏ, cụm lọc chất thải khỏi máu. Bệnh tiểu đường có thể làm hỏng hệ thống lọc. Tổn thương nghiêm trọng có thể dẫn đến suy thận hoặc bệnh thận giai đoạn cuối không thể đảo ngược, chạy thận hoặc ghép thận được đòi hỏi.</p>\r\n<p dir=\"ltr\">Tổn thương mắt: Bệnh tiểu đường có thể làm hỏng các mạch máu của võng mạc (bệnh lý võng mạc tiểu đường), có khả năng dẫn đến mù lòa. Bệnh tiểu đường cũng làm tăng nguy cơ bị các vấn đề tầm nhìn nghiêm trọng khác, như đục thủy tinh thể và tăng nhãn áp.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-03.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tổn thương chân. Thiệt hại thần kinh ở bàn chân hoặc lưu lượng máu kém làm tăng nguy cơ biến chứng bàn chân. Nếu không điều trị, vết thương có thể trở nên nhiễm trùng nặng. Thiệt hại nghiêm trọng ngón chân, bàn chân có thể yêu cầu cắt cụt.</p>\r\n<p dir=\"ltr\">Tổn thương da và răng miệng: Bệnh tiểu đường có thể dễ bị vấn đề về da, kể cả nhiễm trùng do vi khuẩn và nấm. Nhiễm trùng miệng cũng có thể là một mối quan tâm, đặc biệt là nếu có lịch sử vệ sinh răng miệng kém.</p>\r\n<p dir=\"ltr\">Loãng xương, còi xương: Bệnh tiểu đường có thể dẫn đến mật độ xương thấp hơn so với bình thường, làm tăng nguy cơ loãng xương. Trẻ dễ bị còi xương, khó phát triển chiều cao.</p>\r\n<p dir=\"ltr\"><strong>Dự phòng và điều trị tiểu đường ở trẻ em:</strong></p>\r\n<p dir=\"ltr\">Vai trò của bố mẹ, người lớn trong việc giáo dục trẻ em, đồng hành cùng với trẻ, tạo ra một môi trường lành mạnh, một chế độ dinh dưỡng cân bằng khoa học cũng như hình thành ý thức chăm sóc sức khoẻ của trẻ là hết sức cần thiết và quan trọng. Cần thực hiện một số nội dung sau đây:</p>\r\n<p dir=\"ltr\">+ Áp dụng chiến lược “kiềng 3 chân” triệt để: xây dựng, điều chỉnh chế độ ăn hợp lý, khoa học giúp trẻ giảm bớt cân khi thừa cân; lựa chọn các thực phẩm thân thiện cho tiểu đường và phù hợp với trẻ; Động viên khích lệ trẻ hoạt động phù hợp theo đặc điểm tuổi, tình trạng sức khoẻ; Sử dụng các thuốc kiểm soát đường máu theo chỉ dẫn của bác sỹ.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-04.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Thường xuyên cho trẻ khám sức khỏe định kỳ, trong đó có xét</p>\r\n<p dir=\"ltr\">nghiệm tiểu đường cho trẻ em. Quá trình xét nghiệm tiểu đường cho trẻ em khá đơn giản và nhanh chóng. Để xét nghiệm tiểu đường cho trẻ em, bác sĩ sẽ tiến hành xét nghiệm đường huyết trong nước tiểu hay trong máu.</p>\r\n<p dir=\"ltr\">+ Đối với những trẻ đã bị mắc bệnh, cần giáo dục cho trẻ ý thức tự</p>\r\n<p dir=\"ltr\">bảo vệ bản thân khỏi những tác nhân gây hại như tránh để bị trầy xước, áp dụng chế độ ăn uống và tập luyện hợp lý.</p>\r\n<p dir=\"ltr\">+ Điều quan trọng là cha mẹ cần tạo cho trẻ một môi trường sống</p>\r\n<p dir=\"ltr\">bình thường như những đứa trẻ khác để trẻ được phát triển toàn diện. Việc này sẽ giúp nâng cao hiệu quả chữa trị bệnh đái tháo đường trẻ em.</p>\r\n<p dir=\"ltr\">Giống như nhiều bệnh lý khác, đái tháo đường trẻ em thường không có biểu hiện rõ rệt nên rất khó nhận biết. Do đó, nếu nghi ngờ trẻ mắc bệnh cha mẹ cần tiến hành xét nghiệm tiểu đường cho trẻ em để chẩn đoán và điều trị bệnh kịp thời.</p>', 'Tiểu đường ở trẻ em', '', 'publish', 'closed', 'closed', '', 'tieu-duong-o-tre-em-2', '', '', '2020-06-15 02:24:34', '2020-06-15 02:24:34', '', 0, 'http://localhost:8080/yentheme/?p=119', 0, 'post', '', 0),
(120, 1, '2020-06-10 04:40:51', '2020-06-10 04:40:51', '', 'Tiểu đường ở trẻ em', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-06-10 04:40:51', '2020-06-10 04:40:51', '', 119, 'http://localhost:8080/yentheme/2020/06/10/119-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2020-06-10 04:41:18', '2020-06-10 04:41:18', 'Kiến thức thai kỳ mà các bà bầu cần biết', 'Kiến thức tiểu đường thai kỳ', '', 'publish', 'open', 'open', '', 'kien-thuc-tieu-duong-thai-ky', '', '', '2020-06-10 05:56:48', '2020-06-10 05:56:48', '', 0, 'http://localhost:8080/yentheme/?p=121', 0, 'post', '', 0),
(122, 1, '2020-06-10 04:41:18', '2020-06-10 04:41:18', '', 'Kiến thức tiểu đường thai kỳ', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2020-06-10 04:41:18', '2020-06-10 04:41:18', '', 121, 'http://localhost:8080/yentheme/2020/06/10/121-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2020-06-10 05:41:59', '2020-06-10 05:41:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-cai-dat-chung\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Hiển thị list bài viết trên trang chủ', 'hien-thi-list-bai-viet-tren-trang-chu', 'trash', 'closed', 'closed', '', 'group_5ee071e6eacb5__trashed', '', '', '2020-06-15 02:46:58', '2020-06-15 02:46:58', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&#038;p=123', 0, 'acf-field-group', '', 0),
(124, 1, '2020-06-10 05:41:59', '2020-06-10 05:41:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID Danh muc theo list', 'id_danh_muc_theo_list', 'trash', 'closed', 'closed', '', 'field_5ee0720fdc1cb__trashed', '', '', '2020-06-15 02:46:58', '2020-06-15 02:46:58', '', 123, 'http://localhost:8080/yentheme/?post_type=acf-field&#038;p=124', 0, 'acf-field', '', 0),
(125, 1, '2020-06-10 05:41:59', '2020-06-10 05:41:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";i:1;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Giới hạn số lượng bài theo list', 'limit_post_category_home_theo_list', 'trash', 'closed', 'closed', '', 'field_5ee07247dc1cc__trashed', '', '', '2020-06-15 02:46:58', '2020-06-15 02:46:58', '', 123, 'http://localhost:8080/yentheme/?post_type=acf-field&#038;p=125', 1, 'acf-field', '', 0),
(126, 1, '2020-06-10 05:56:48', '2020-06-10 05:56:48', 'Kiến thức thai kỳ mà các bà bầu cần biết', 'Kiến thức tiểu đường thai kỳ', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2020-06-10 05:56:48', '2020-06-10 05:56:48', '', 121, 'http://localhost:8080/yentheme/2020/06/10/121-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2020-06-10 06:38:25', '2020-06-10 06:38:25', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]\nYen Theme \"[your-subject]\"\nYen Theme <hongyenhd97@gmail.com>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\nhongyenhd97@gmail.com\nReply-To: [your-email]\n\n0\n0\n\nYen Theme \"[your-subject]\"\nYen Theme <hongyenhd97@gmail.com>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\n[your-email]\nReply-To: hongyenhd97@gmail.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2020-06-10 06:38:25', '2020-06-10 06:38:25', '', 0, 'http://localhost:8080/yentheme/?post_type=wpcf7_contact_form&p=127', 0, 'wpcf7_contact_form', '', 0),
(130, 1, '2020-06-10 07:04:34', '2020-06-10 07:04:34', '<div class=\"col-xs-12 col-sm-6\">\r\n        <div class=\"form-group\" id=\"input-name\">\r\n            [text* your-name placeholder=\'Họ và tên\']\r\n        </div>\r\n    </div>\r\n    <div class=\"col-xs-12 col-sm-6\">\r\n        <div class=\"form-group\" id=\"input-phone\">\r\n          [tel* tel-966 placeholder=\"Số điện thoại\"]\r\n        </div>\r\n    </div>\r\n    <div class=\"col-xs-12 col-sm-6\">\r\n        <div class=\"form-group\" id=\"input-email\">\r\n            [email* your-email placeholder=\"Email\"]\r\n        </div>\r\n    </div>\r\n    <div class=\"col-xs-12 col-sm-6\">\r\n        <div class=\"form-group\" id=\"input-content\">\r\n            [text your-message placeholder=\"Nội dung cần tư vấn\"]\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"text-center hotline\">\r\n            <b>HOTLINE: <a href=\"tel:088.885.4313\">088.885.4313</a></b>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"btn-register\">\r\n[submit \"Đăng ký ngay\"]\r\n        </div>\r\n    </div>\n1\nYen Theme \"[your-subject]\"\nYen Theme <hongyenhd97@gmail.com>\nhongyenhd97@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\nReply-To: [your-email]\n\n1\n\n1\nYen Theme \"[your-subject]\"\nYen Theme <hongyenhd97@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Yen Theme (http://localhost:8080/yentheme)\nReply-To: hongyenhd97@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Form Register', '', 'publish', 'closed', 'closed', '', 'contact-form-1_copy', '', '', '2020-06-10 11:40:27', '2020-06-10 11:40:27', '', 0, 'http://localhost:8080/yentheme/?post_type=wpcf7_contact_form&#038;p=130', 0, 'wpcf7_contact_form', '', 0),
(131, 1, '2020-06-10 10:31:16', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 10:31:16', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=131', 1, 'nav_menu_item', '', 0),
(134, 1, '2020-06-10 10:46:16', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 10:46:16', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=134', 1, 'nav_menu_item', '', 0),
(140, 1, '2020-06-10 12:18:41', '2020-06-10 12:18:41', '', 'Trang chủ', '', 'trash', 'closed', 'closed', '', 'trang-chu__trashed', '', '', '2020-06-11 07:50:14', '2020-06-11 07:50:14', '', 0, 'http://localhost:8080/yentheme/?page_id=140', 0, 'page', '', 0),
(142, 1, '2020-06-10 12:18:41', '2020-06-10 12:18:41', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '140-revision-v1', '', '', '2020-06-10 12:18:41', '2020-06-10 12:18:41', '', 140, 'http://localhost:8080/yentheme/2020/06/10/140-revision-v1/', 0, 'revision', '', 0),
(143, 1, '2020-06-10 12:22:28', '2020-06-10 12:22:28', '', 'Chiến lược \"Kiềng 3 chân\"', '', 'trash', 'closed', 'closed', '', 'chien-luoc-kieng-3-chan__trashed', '', '', '2020-06-10 15:55:09', '2020-06-10 15:55:09', '', 140, 'http://localhost:8080/yentheme/?page_id=143', 0, 'page', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(144, 1, '2020-06-10 12:22:28', '2020-06-10 12:22:28', '', 'Chiến lược \"Kiềng 3 chân\"', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-06-10 12:22:28', '2020-06-10 12:22:28', '', 143, 'http://localhost:8080/yentheme/2020/06/10/143-revision-v1/', 0, 'revision', '', 0),
(145, 1, '2020-06-10 12:22:53', '0000-00-00 00:00:00', '', 'Chiến lược \"Kiềng 3 chân\"', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 12:22:53', '0000-00-00 00:00:00', '', 140, 'http://localhost:8080/yentheme/?p=145', 1, 'nav_menu_item', '', 0),
(147, 1, '2020-06-10 14:01:25', '0000-00-00 00:00:00', '', 'Góc chuyên gia', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 14:01:25', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=147', 1, 'nav_menu_item', '', 0),
(164, 1, '2020-06-10 14:24:23', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-10 14:24:23', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?page_id=164', 0, 'page', '', 0),
(166, 1, '2020-06-10 14:51:19', '2020-06-10 14:51:19', '', 'Hội đồng chuyên gia', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2020-06-10 14:51:19', '2020-06-10 14:51:19', '', 0, 'http://localhost:8080/yentheme/?page_id=166', 0, 'page', '', 0),
(167, 1, '2020-06-10 14:40:47', '2020-06-10 14:40:47', '', 'Dinh dưỡng', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2020-06-10 14:40:47', '2020-06-10 14:40:47', '', 11, 'http://localhost:8080/yentheme/2020/06/10/11-revision-v1/', 0, 'revision', '', 0),
(168, 1, '2020-06-10 14:46:42', '2020-06-10 14:46:42', '', 'Góc chuyên gia', '', 'publish', 'closed', 'closed', '', 'hoi-dong-chuyen-gia', '', '', '2020-06-10 15:52:48', '2020-06-10 15:52:48', '', 0, 'http://localhost:8080/yentheme/?page_id=168', 0, 'page', '', 0),
(170, 1, '2020-06-10 14:46:42', '2020-06-10 14:46:42', '', 'Hội đồng chuyên gia', '', 'inherit', 'closed', 'closed', '', '168-revision-v1', '', '', '2020-06-10 14:46:42', '2020-06-10 14:46:42', '', 168, 'http://localhost:8080/yentheme/2020/06/10/168-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2020-06-10 14:50:02', '2020-06-10 14:50:02', '', 'PGS. TS Thầy thuốc ưu tú Nguyễn Viết Lượng - Học Viện Quân Y', '', 'publish', 'closed', 'closed', '', 'pgs-le-van-truong', '', '', '2020-06-13 02:27:50', '2020-06-13 02:27:50', '', 0, 'http://localhost:8080/yentheme/?p=171', 0, 'post', '', 0),
(172, 1, '2020-06-10 14:50:02', '2020-06-10 14:50:02', '', 'PGS Lê Văn Trường', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2020-06-10 14:50:02', '2020-06-10 14:50:02', '', 171, 'http://localhost:8080/yentheme/2020/06/10/171-revision-v1/', 0, 'revision', '', 0),
(173, 1, '2020-06-10 14:51:19', '2020-06-10 14:51:19', '', 'Hội đồng chuyên gia', '', 'inherit', 'closed', 'closed', '', '166-revision-v1', '', '', '2020-06-10 14:51:19', '2020-06-10 14:51:19', '', 166, 'http://localhost:8080/yentheme/2020/06/10/166-revision-v1/', 0, 'revision', '', 0),
(174, 1, '2020-06-10 14:59:01', '2020-06-10 14:59:01', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-cai-dat-chung\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Danh sách Hội đồng chuyên gia', 'danh-sach-hoi-dong-chuyen-gia', 'publish', 'closed', 'closed', '', 'group_5ee0f44bc0a22', '', '', '2020-06-10 14:59:01', '2020-06-10 14:59:01', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&#038;p=174', 0, 'acf-field-group', '', 0),
(175, 1, '2020-06-10 14:59:01', '2020-06-10 14:59:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID Chuyên gia', 'id_chuyen_gia', 'publish', 'closed', 'closed', '', 'field_5ee0f4647c31b', '', '', '2020-06-10 14:59:01', '2020-06-10 14:59:01', '', 174, 'http://localhost:8080/yentheme/?post_type=acf-field&p=175', 0, 'acf-field', '', 0),
(176, 1, '2020-06-10 14:59:01', '2020-06-10 14:59:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";i:1;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Số lượng hiển thị', 'so_luong_hien_thi', 'publish', 'closed', 'closed', '', 'field_5ee0f4e17c31c', '', '', '2020-06-10 14:59:01', '2020-06-10 14:59:01', '', 174, 'http://localhost:8080/yentheme/?post_type=acf-field&p=176', 1, 'acf-field', '', 0),
(177, 1, '2020-06-10 15:52:30', '2020-06-10 15:52:30', '', 'Góc chuyên gia', '', 'inherit', 'closed', 'closed', '', '168-revision-v1', '', '', '2020-06-10 15:52:30', '2020-06-10 15:52:30', '', 168, 'http://localhost:8080/yentheme/2020/06/10/168-revision-v1/', 0, 'revision', '', 0),
(178, 1, '2020-06-10 15:56:19', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 15:56:19', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=178', 1, 'nav_menu_item', '', 0),
(179, 1, '2020-06-11 01:08:55', '2020-06-11 01:08:55', '', 'Chuyên gia dinh dưỡng', '', 'trash', 'closed', 'closed', '', '__trashed-2', '', '', '2020-06-11 01:08:55', '2020-06-11 01:08:55', '', 0, 'http://localhost:8080/yentheme/?page_id=179', 0, 'page', '', 0),
(180, 1, '2020-06-10 16:00:12', '2020-06-10 16:00:12', '', 'Chuyên gia dinh dưỡng', '', 'publish', 'closed', 'closed', '', 'chuyen-gia-dinh-duong', '', '', '2020-06-10 16:00:12', '2020-06-10 16:00:12', '', 0, 'http://localhost:8080/yentheme/?page_id=180', 0, 'page', '', 0),
(182, 1, '2020-06-10 16:00:12', '2020-06-10 16:00:12', '', 'Chuyên gia dinh dưỡng', '', 'inherit', 'closed', 'closed', '', '180-revision-v1', '', '', '2020-06-10 16:00:12', '2020-06-10 16:00:12', '', 180, 'http://localhost:8080/yentheme/2020/06/10/180-revision-v1/', 0, 'revision', '', 0),
(183, 1, '2020-06-10 16:00:40', '2020-06-10 16:00:40', '', 'Chuyên gia vận động', '', 'publish', 'closed', 'closed', '', 'chuyen-gia-van-dong', '', '', '2020-06-10 16:00:40', '2020-06-10 16:00:40', '', 0, 'http://localhost:8080/yentheme/?page_id=183', 0, 'page', '', 0),
(185, 1, '2020-06-10 16:00:40', '2020-06-10 16:00:40', '', 'Chuyên gia vận động', '', 'inherit', 'closed', 'closed', '', '183-revision-v1', '', '', '2020-06-10 16:00:40', '2020-06-10 16:00:40', '', 183, 'http://localhost:8080/yentheme/2020/06/10/183-revision-v1/', 0, 'revision', '', 0),
(186, 1, '2020-06-10 16:00:53', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 16:00:53', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=186', 1, 'nav_menu_item', '', 0),
(187, 1, '2020-06-10 16:00:53', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-10 16:00:53', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/yentheme/?p=187', 1, 'nav_menu_item', '', 0),
(197, 1, '2020-06-11 01:08:55', '2020-06-11 01:08:55', '', 'Chuyên gia dinh dưỡng', '', 'inherit', 'closed', 'closed', '', '179-revision-v1', '', '', '2020-06-11 01:08:55', '2020-06-11 01:08:55', '', 179, 'http://localhost:8080/yentheme/2020/06/11/179-revision-v1/', 0, 'revision', '', 0),
(198, 1, '2020-06-11 01:12:10', '2020-06-11 01:12:10', '', 'PGS. TS Thầy thuốc ưu tú Nguyễn Viết Lượng - Học Viện Quân Y', '', 'inherit', 'closed', 'closed', '', '171-autosave-v1', '', '', '2020-06-11 01:12:10', '2020-06-11 01:12:10', '', 171, 'http://localhost:8080/yentheme/2020/06/11/171-autosave-v1/', 0, 'revision', '', 0),
(199, 1, '2020-06-11 01:12:38', '2020-06-11 01:12:38', '', 'quang7', '', 'inherit', 'open', 'closed', '', 'quang7', '', '', '2020-06-11 01:12:38', '2020-06-11 01:12:38', '', 171, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/quang7.png', 0, 'attachment', 'image/png', 0),
(200, 1, '2020-06-11 01:12:43', '2020-06-11 01:12:43', '', 'PGS. TS Thầy thuốc ưu tú Nguyễn Viết Lượng - Học Viện Quân Y', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2020-06-11 01:12:43', '2020-06-11 01:12:43', '', 171, 'http://localhost:8080/yentheme/2020/06/11/171-revision-v1/', 0, 'revision', '', 0),
(201, 1, '2020-06-11 01:13:56', '2020-06-11 01:13:56', '', 'TS Phạm Đức Minh chủ nhiệm Bộ môn dinh dưỡng, Học Viện Quân y', '', 'publish', 'closed', 'closed', '', 'ts-pham-duc-minh-chu-nhiem-bo-mon-dinh-duong-hoc-vien-quan-y', '', '', '2020-06-13 02:28:43', '2020-06-13 02:28:43', '', 0, 'http://localhost:8080/yentheme/?p=201', 0, 'post', '', 0),
(202, 1, '2020-06-11 01:13:44', '2020-06-11 01:13:44', '', 'bichnga', '', 'inherit', 'open', 'closed', '', 'bichnga', '', '', '2020-06-11 01:13:44', '2020-06-11 01:13:44', '', 201, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/bichnga.png', 0, 'attachment', 'image/png', 0),
(203, 1, '2020-06-11 01:13:56', '2020-06-11 01:13:56', '', 'TS PHạm Đức Minh chủ nhiệm Bộ môn dinh dưỡng, Học Viện Quân y', '', 'inherit', 'closed', 'closed', '', '201-revision-v1', '', '', '2020-06-11 01:13:56', '2020-06-11 01:13:56', '', 201, 'http://localhost:8080/yentheme/2020/06/11/201-revision-v1/', 0, 'revision', '', 0),
(204, 1, '2020-06-11 01:14:04', '2020-06-11 01:14:04', '', 'TS Phạm Đức Minh chủ nhiệm Bộ môn dinh dưỡng, Học Viện Quân y', '', 'inherit', 'closed', 'closed', '', '201-revision-v1', '', '', '2020-06-11 01:14:04', '2020-06-11 01:14:04', '', 201, 'http://localhost:8080/yentheme/2020/06/11/201-revision-v1/', 0, 'revision', '', 0),
(205, 1, '2020-06-11 01:15:09', '2020-06-11 01:15:09', '', 'PGS. TS Nguyễn Ngọc Châu - Chủ nhiệm khoa khớp nội tiết bệnh viện 103, Học Viện Quân Y', '', 'publish', 'closed', 'closed', '', 'pgs-ts-nguyen-ngoc-chau-chu-nhiem-khoa-khop-noi-tiet-benh-vien-103-hoc-vien-quan-y', '', '', '2020-06-13 02:28:10', '2020-06-13 02:28:10', '', 0, 'http://localhost:8080/yentheme/?p=205', 0, 'post', '', 0),
(206, 1, '2020-06-11 01:15:06', '2020-06-11 01:15:06', '', 'dieuvan', '', 'inherit', 'open', 'closed', '', 'dieuvan', '', '', '2020-06-11 01:15:06', '2020-06-11 01:15:06', '', 205, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/dieuvan.png', 0, 'attachment', 'image/png', 0),
(207, 1, '2020-06-11 01:15:09', '2020-06-11 01:15:09', '', 'PGS. TS Nguyễn Ngọc Châu - Chủ nhiệm khoa khớp nội tiết bệnh viện 103, Học Viện Quân Y', '', 'inherit', 'closed', 'closed', '', '205-revision-v1', '', '', '2020-06-11 01:15:09', '2020-06-11 01:15:09', '', 205, 'http://localhost:8080/yentheme/2020/06/11/205-revision-v1/', 0, 'revision', '', 0),
(208, 1, '2020-06-11 01:15:58', '2020-06-11 01:15:58', '', 'GS. TS. Thầy thuốc nhận dân Đồng khắc Hưng - Học Viện Quân Y', '', 'publish', 'open', 'open', '', 'gs-ts-thay-thuoc-nhan-dan-dong-khac-hung-hoc-vien-quan-y', '', '', '2020-06-11 01:17:54', '2020-06-11 01:17:54', '', 0, 'http://localhost:8080/yentheme/?p=208', 0, 'post', '', 0),
(209, 1, '2020-06-11 01:15:58', '2020-06-11 01:15:58', '', 'GS. TS> Thầy thuốc nhận dân Đồng khắc Hưng - Học Viện Quân Y', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-06-11 01:15:58', '2020-06-11 01:15:58', '', 208, 'http://localhost:8080/yentheme/2020/06/11/208-revision-v1/', 0, 'revision', '', 0),
(210, 1, '2020-06-11 01:16:07', '2020-06-11 01:16:07', '', 'quang', '', 'inherit', 'open', 'closed', '', 'quang', '', '', '2020-06-11 01:16:07', '2020-06-11 01:16:07', '', 208, 'http://localhost:8080/yentheme/wp-content/uploads/2020/06/quang.png', 0, 'attachment', 'image/png', 0),
(211, 1, '2020-06-11 01:17:54', '2020-06-11 01:17:54', '', 'GS. TS. Thầy thuốc nhận dân Đồng khắc Hưng - Học Viện Quân Y', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-06-11 01:17:54', '2020-06-11 01:17:54', '', 208, 'http://localhost:8080/yentheme/2020/06/11/208-revision-v1/', 0, 'revision', '', 0),
(212, 1, '2020-06-11 03:34:22', '2020-06-11 03:34:22', '', 'Nghiên cứu & tin tức', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-06-11 03:34:22', '2020-06-11 03:34:22', '', 51, 'http://localhost:8080/yentheme/2020/06/11/51-revision-v1/', 0, 'revision', '', 0),
(213, 1, '2020-06-11 03:52:59', '2020-06-11 03:52:59', 'Biến chứng bệnh tiểu đường thường phát triển dần dần. Bạn mắc bệnh tiểu đường càng lâu và lượng đường trong máu càng không ổn định, bạn càng có nguy cơ mắc biến chứng cao. Cuối cùng, biến chứng tiểu đường có thể không điều trị được hoặc thậm chí đe dọa đến tính mạng.', 'Biến chứng của tiểu đường', '', 'publish', 'closed', 'closed', '', 'bien-chung-cua-tieu-duong-2', '', '', '2020-06-13 03:01:27', '2020-06-13 03:01:27', '', 0, 'http://localhost:8080/yentheme/?p=213', 0, 'post', '', 0),
(214, 1, '2020-06-11 03:52:59', '2020-06-11 03:52:59', '', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2020-06-11 03:52:59', '2020-06-11 03:52:59', '', 213, 'http://localhost:8080/yentheme/2020/06/11/213-revision-v1/', 0, 'revision', '', 0),
(215, 1, '2020-06-11 03:53:57', '2020-06-11 03:53:57', '\r\nBy Danh\r\nBiến chứng bệnh tiểu đường thường phát triển dần dần. Bạn mắc bệnh tiểu đường càng lâu và lượng đường trong máu càng không ổn định, bạn càng có nguy cơ mắc biến chứng cao. Cuối cùng, biến chứng tiểu đường có thể không điều trị được hoặc thậm chí đe dọa đến tính mạng. \r\n\r\nBiến chứng tiểu đường có thể xảy ra bao gồm:\r\nTổn thương tim mạch: Bệnh tiểu đường làm tăng đáng kể nguy cơ mắc các vấn đề về tim mạch khác, bao gồm bệnh động mạch vành kèm đau ngực (đau thắt ngực), đau tim, đột quỵ và hẹp động mạch (xơ vữa động mạch). Nếu bị tiểu đường, bạn có nhiều khả năng mắc bệnh tim hoặc đột quỵ.\r\n\r\nNguyên nhân: Khi đường tăng cao trong máu, máu sẽ tăng độ sánh, “dính” do đường bám vào hồng cầu, các hồng cầu có xu hướng kết dính với nhau tạo thành các mảng, các cục máu đông gây tắc mạch (đặc biệt các mạch máu nhỏ). Ngoài ra trên bệnh nhân tiểu đường, nguy cơ có rối loạn chuyển hoá mỡ máu là rất cao, khi mỡ Triglycerid (TG) và cholesterol xấu (LDL) tăng cao gây xơ vữa động mạch, từ đó khởi phát nhiều vấn đề về tim mạch như cao huyết áp, suy tim, nhồi máu cơ tim, nhồi máu não, xuất huyết não.\r\n\r\n\r\n\r\nTổn thương thần kinh: Mức đường máu cao có thể làm tổn thương các thành mạch máu nhỏ (mao mạch) nuôi dưỡng dây thần kinh, đặc biệt là ở chân. Điều này có thể gây ngứa, tê, rát hoặc đau thường bắt đầu ở đầu ngón chân hoặc ngón tay và dần dần lan rộng lên trên. Nếu không được điều trị, bạn có thể mất cảm giác hoàn toàn ở chân tay bị ảnh hưởng. Thiệt hại cho các dây thần kinh liên quan đến tiêu hóa có thể gây ra vấn đề với buồn nôn, nôn mửa, tiêu chảy hoặc táo bón. Đối với nam giới, bệnh có thể dẫn đến rối loạn cương dương.  \r\n\r\n\r\n\r\nTổn thương thận: Thận chứa hàng triệu cụm mạch máu nhỏ (tiểu cầu) để lọc chất thải ra khỏi máu của bạn. Bệnh tiểu đường có thể làm hỏng hệ thống lọc tinh tế này. Tổn thương thận nghiêm trọng có thể dẫn đến suy thận hoặc bệnh thận giai đoạn cuối không hồi phục, cần phải chạy thận hoặc ghép thận.\r\n\r\n\r\n\r\nTổn thương mắt: Bệnh tiểu đường có thể làm tổn thương các mạch máu của võng mạc (bệnh võng mạc tiểu đường), có khả năng dẫn đến mù lòa. Bệnh tiểu đường cũng làm tăng nguy cơ mắc các tình trạng thị lực nghiêm trọng khác, chẳng hạn như đục thủy tinh thể và bệnh tăng nhãn áp.\r\n\r\nTổn thương bàn chân: Tổn thương dây thần kinh ở bàn chân hoặc lưu thông máu kém đến chân làm tăng nguy cơ mắc  biến chứng chân khác nhau. Nếu không được điều trị, vết cắt và mụn nước có thể phát triển thành nhiễm trùng nghiêm trọng, thường rất khó lành và có thể phải đoạn chi.\r\n\r\n\r\n\r\nCác tình trạng tổn thương da: Bệnh tiểu đường có thể khiến bạn dễ bị các vấn đề về da hơn, bao gồm nhiễm trùng do vi khuẩn và nấm. Sự lão hoá da cũng da tăng (da sẫm màu thâm đen, đồi mồi, rối loạn sắc tố…)\r\n\r\n\r\n\r\nSuy giảm chức năng tình dục: Bệnh tiểu đường gây tắc các mạch máu ở cơ quan sinh dục: Với nam giới máu không bơm vào mô cương của dương vật, dẫn đến không có khả năng cương hoặc giảm mức độ cương của dương vật. Với phụ nữ mạch máu nuôi âm đạo, tử cung bị xơ cứng, tắc nghẽn dẫn đến niêm mạc bị thiểu dưỡng, tổn thương gây teo khô bề mặt, sinh ra chứng khô hạn, lâu dài dẫn đến lãnh cảm.   \r\n\r\n  \r\n\r\nKhiếm thính: Các vấn đề thính giác thường gặp hơn ở những người mắc bệnh tiểu đường.\r\n\r\nBệnh Alzheimer: Bệnh tiểu đường tuýp 2 có thể làm tăng nguy cơ mắc bệnh Alzheimer. Việc kiểm soát lượng đường trong máu của bạn càng kém thì nguy cơ mắc biến chứng bệnh tiểu đường càng lớn.\r\n\r\n\r\n\r\nBiến chứng tiểu đường thai kỳ\r\nHầu hết phụ nữ bị tiểu đường thai kỳ đều sinh con khỏe mạnh. Tuy nhiên, lượng đường trong máu không được điều trị hoặc không kiểm soát được có thể gây ra vấn đề cho bạn và con.\r\n\r\nCác biến chứng tiểu đường ở trẻ bao gồm:\r\nThai nhi phát triển hơn so với tuổi. Lượng đường dư trong cơ thể người mẹ có thể đi qua nhau thai, làm cho tuyến tụy của bé phát triển thêm insulin. Điều này có thể làm cho thai nhi phát triển lớn hơn so với tuổi và bạn phải sinh mổ.\r\n\r\nLượng đường trong máu thấp. Đôi khi, trẻ sẽ có lượng đường trong máu thấp (hạ đường huyết) ngay sau khi sinh vì quá trình sản xuất insulin của trẻ tăng cao \r\n\r\nTrẻ dễ bị bệnh tiểu đường tuýp 2 trong tương lai. Trẻ sơ sinh có nguy cơ cao mắc bệnh béo phì và tiểu đường tuýp 2 khi trẻ lớn lên.\r\n\r\nTử vong. Bệnh tiểu đường thai kỳ không được điều trị có thể dẫn đến trẻ tử vong trước hoặc ngay sau khi sinh.\r\n\r\nCác biến chứng bệnh tiểu đường thai kỳ ở người mẹ gồm:\r\n\r\nTiền sản giật. Tình trạng này đặc trưng bởi huyết áp cao, dư protein trong nước tiểu, sưng ở chân và bàn chân. Tiền sản giật có thể dẫn đến các biến chứng nghiêm trọng hoặc thậm chí đe dọa tính mạng cho cả mẹ và con.\r\n\r\nTiểu đường thai kỳ ở lần mang thai tiếp theo. Một khi đã mắc bệnh tiểu đường thai kỳ trong một lần mang thai, bạn có nhiều khả năng mắc bệnh tiểu đường hơn với lần mang thai tiếp theo. Bạn cũng có nhiều khả năng phát triển bệnh tiểu đường – điển hình là bệnh tiểu đường loại 2 – khi bạn già đi.\r\n\r\n ', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2020-06-11 03:53:57', '2020-06-11 03:53:57', '', 213, 'http://localhost:8080/yentheme/2020/06/11/213-revision-v1/', 0, 'revision', '', 0),
(216, 1, '2020-06-11 03:57:06', '2020-06-11 03:57:06', '\r\nBiến chứng bệnh tiểu đường thường phát triển dần dần. Bạn mắc bệnh tiểu đường càng lâu và lượng đường trong máu càng không ổn định, bạn càng có nguy cơ mắc biến chứng cao. Cuối cùng, biến chứng tiểu đường có thể không điều trị được hoặc thậm chí đe dọa đến tính mạng. \r\n\r\n\r\n ', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2020-06-11 03:57:06', '2020-06-11 03:57:06', '', 213, 'http://localhost:8080/yentheme/2020/06/11/213-revision-v1/', 0, 'revision', '', 0),
(219, 1, '2020-06-11 07:44:01', '2020-06-11 07:44:01', '', 'Chiến lược \"Kiềng 3 chân\"', '', 'publish', 'closed', 'closed', '', 'chien-luoc-kieng-3-chan', '', '', '2020-06-11 07:44:01', '2020-06-11 07:44:01', '', 0, 'http://localhost:8080/yentheme/?page_id=219', 0, 'page', '', 0),
(221, 1, '2020-06-11 07:44:01', '2020-06-11 07:44:01', '', 'Chiến lược \"Kiềng 3 chân\"', '', 'inherit', 'closed', 'closed', '', '219-revision-v1', '', '', '2020-06-11 07:44:01', '2020-06-11 07:44:01', '', 219, 'http://localhost:8080/yentheme/2020/06/11/219-revision-v1/', 0, 'revision', '', 0),
(223, 1, '2020-06-11 08:28:55', '2020-06-11 08:28:55', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-cai-dat-chung\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Danh sách bài viết theo category', 'danh-sach-bai-viet-theo-category', 'publish', 'closed', 'closed', '', 'group_5ee1ea8478d38', '', '', '2020-06-11 08:55:41', '2020-06-11 08:55:41', '', 0, 'http://localhost:8080/yentheme/?post_type=acf-field-group&#038;p=223', 0, 'acf-field-group', '', 0),
(224, 1, '2020-06-11 08:28:56', '2020-06-11 08:28:56', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID bài viết theo category', 'id_post_for_category', 'publish', 'closed', 'closed', '', 'field_5ee1eb04e5a1a', '', '', '2020-06-11 08:55:41', '2020-06-11 08:55:41', '', 223, 'http://localhost:8080/yentheme/?post_type=acf-field&#038;p=224', 0, 'acf-field', '', 0),
(225, 1, '2020-06-11 08:55:00', '2020-06-11 08:55:00', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";i:1;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Số lượng hiển thị tối đa', 'show_quantity_max', 'publish', 'closed', 'closed', '', 'field_5ee1f131ceeff', '', '', '2020-06-11 08:55:00', '2020-06-11 08:55:00', '', 223, 'http://localhost:8080/yentheme/?post_type=acf-field&p=225', 1, 'acf-field', '', 0),
(226, 1, '2020-06-11 09:30:27', '2020-06-11 09:30:27', '', 'Tiến triển của bệnh tiểu đường', '', 'publish', 'closed', 'closed', '', 'tien-trien-cua-benh-tieu-duong-2', '', '', '2020-06-12 11:12:34', '2020-06-12 11:12:34', '', 0, 'http://localhost:8080/yentheme/?p=226', 0, 'post', '', 0),
(227, 1, '2020-06-11 09:30:27', '2020-06-11 09:30:27', '', 'Tiến triển của bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '226-revision-v1', '', '', '2020-06-11 09:30:27', '2020-06-11 09:30:27', '', 226, 'http://localhost:8080/yentheme/2020/06/11/226-revision-v1/', 0, 'revision', '', 0),
(228, 1, '2020-06-11 09:31:31', '2020-06-11 09:31:31', '', 'Các biến chứng của bệnh tiểu đường', '', 'publish', 'open', 'open', '', 'cac-bien-chung-cua-benh-tieu-duong-2', '', '', '2020-06-11 09:31:31', '2020-06-11 09:31:31', '', 0, 'http://localhost:8080/yentheme/?p=228', 0, 'post', '', 0),
(229, 1, '2020-06-11 09:31:31', '2020-06-11 09:31:31', '', 'Các biến chứng của bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '228-revision-v1', '', '', '2020-06-11 09:31:31', '2020-06-11 09:31:31', '', 228, 'http://localhost:8080/yentheme/2020/06/11/228-revision-v1/', 0, 'revision', '', 0),
(230, 1, '2020-06-11 09:32:54', '2020-06-11 09:32:54', '', 'Chăm sóc bản thân', '', 'publish', 'closed', 'closed', '', 'cham-soc-ban-than', '', '', '2020-06-12 11:13:35', '2020-06-12 11:13:35', '', 0, 'http://localhost:8080/yentheme/?p=230', 0, 'post', '', 0),
(231, 1, '2020-06-11 09:32:54', '2020-06-11 09:32:54', '', 'Chăm sóc bản thân', '', 'inherit', 'closed', 'closed', '', '230-revision-v1', '', '', '2020-06-11 09:32:54', '2020-06-11 09:32:54', '', 230, 'http://localhost:8080/yentheme/2020/06/11/230-revision-v1/', 0, 'revision', '', 0),
(232, 1, '2020-06-11 09:33:45', '2020-06-11 09:33:45', '', 'Theo dõi và kiểm soát bệnh tiểu đường', '', 'publish', 'closed', 'closed', '', 'theo-doi-va-kiem-soat-benh-tieu-duong-2', '', '', '2020-06-12 12:00:43', '2020-06-12 12:00:43', '', 0, 'http://localhost:8080/yentheme/?p=232', 0, 'post', '', 0),
(233, 1, '2020-06-11 09:33:45', '2020-06-11 09:33:45', '', 'Theo dõi và kiểm soát bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '232-revision-v1', '', '', '2020-06-11 09:33:45', '2020-06-11 09:33:45', '', 232, 'http://localhost:8080/yentheme/2020/06/11/232-revision-v1/', 0, 'revision', '', 0),
(234, 1, '2020-06-11 09:34:09', '2020-06-11 09:34:09', '', 'Tiểu đường và stress', '', 'publish', 'closed', 'closed', '', 'tieu-duong-va-stress-2', '', '', '2020-06-12 12:00:32', '2020-06-12 12:00:32', '', 0, 'http://localhost:8080/yentheme/?p=234', 0, 'post', '', 0),
(235, 1, '2020-06-11 09:34:09', '2020-06-11 09:34:09', '', 'Tiểu đường và stress', '', 'inherit', 'closed', 'closed', '', '234-revision-v1', '', '', '2020-06-11 09:34:09', '2020-06-11 09:34:09', '', 234, 'http://localhost:8080/yentheme/2020/06/11/234-revision-v1/', 0, 'revision', '', 0),
(236, 1, '2020-06-11 09:34:45', '2020-06-11 09:34:45', '', 'Tự kiểm tra đường máu tại nhà', '', 'publish', 'closed', 'closed', '', 'tu-kiem-tra-duong-mau-tai-nha', '', '', '2020-06-12 12:00:14', '2020-06-12 12:00:14', '', 0, 'http://localhost:8080/yentheme/?p=236', 0, 'post', '', 0),
(237, 1, '2020-06-11 09:34:45', '2020-06-11 09:34:45', '', 'Tự kiểm tra đường máu tại nhà', '', 'inherit', 'closed', 'closed', '', '236-revision-v1', '', '', '2020-06-11 09:34:45', '2020-06-11 09:34:45', '', 236, 'http://localhost:8080/yentheme/2020/06/11/236-revision-v1/', 0, 'revision', '', 0),
(240, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '240', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=240', 8, 'nav_menu_item', '', 0),
(241, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '241', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=241', 10, 'nav_menu_item', '', 0),
(242, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '242', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=242', 11, 'nav_menu_item', '', 0),
(243, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '243', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=243', 14, 'nav_menu_item', '', 0),
(244, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '244', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=244', 2, 'nav_menu_item', '', 0),
(245, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '245', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 3, 'http://yentheme.test:8080/?p=245', 4, 'nav_menu_item', '', 0),
(246, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '246', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 3, 'http://yentheme.test:8080/?p=246', 5, 'nav_menu_item', '', 0),
(247, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '247', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 3, 'http://yentheme.test:8080/?p=247', 3, 'nav_menu_item', '', 0),
(248, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '248', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 3, 'http://yentheme.test:8080/?p=248', 6, 'nav_menu_item', '', 0),
(249, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '249', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 3, 'http://yentheme.test:8080/?p=249', 7, 'nav_menu_item', '', 0),
(250, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '250', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=250', 15, 'nav_menu_item', '', 0),
(253, 1, '2020-06-12 03:26:45', '2020-06-12 03:26:45', ' ', '', '', 'publish', 'closed', 'closed', '', '253', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=253', 9, 'nav_menu_item', '', 0),
(254, 1, '2020-06-12 05:01:15', '2020-06-12 05:01:15', '', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=254', 1, 'nav_menu_item', '', 0),
(255, 1, '2020-06-12 06:58:44', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-12 06:58:44', '0000-00-00 00:00:00', '', 0, 'http://yentheme.test:8080/?post_type=acf-field-group&p=255', 0, 'acf-field-group', '', 0),
(256, 1, '2020-06-12 10:23:51', '2020-06-12 10:23:51', '<p dir=\"ltr\">Bài viết dưới đây sẽ khái quát những điểm cơ bản như “bệnh tiểu đường là gì?”, các triệu chứng, phân loại, nguyên nhân, cách điều trị, chi phí điều trị bệnh tiểu đường. Thông qua bài viết này, mọi người sẽ có kiến thức cơ bản về bệnh tiểu đường!</p>\n\n<h2 dir=\"ltr\">1. Bệnh tiểu đường là bệnh như thế nào?</h2>\n<h3 dir=\"ltr\">Tìm hiểu bệnh tiểu đường là gì?</h3>\n<p dir=\"ltr\">Chúng ta ăn uống hàng ngày. Lượng carbohydrates từ bữa ăn được hấp thu vào đường ruột như glucose và hòa tan trong máu. Khi đó, một hormone gọi là insulin được tiết ra, nhờ insulin tiết ra, glucose được đưa vào các tế bào và trở thành nguồn năng lượng của cơ thể.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-01(1).jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Nếu sự hoạt động của insulin không hiệu quả và nếu glucose tăng lên đến mức mà việc xử lý của insulin không thể đáp ứng được, thì sẽ có một lượng lớn glucose không được chuyển hóa thành nguồn năng lượng của cơ thể và bị dư thừa trong máu. “Tình trạng lượng glucose trong máu (lượng đường trong máu) vượt quá tỷ lệ nhất định” được gọi là “bệnh tiểu đường”.</p>\n<p dir=\"ltr\">Do bệnh tiểu đường có một số loại khác nhau nên không thể gộp chung vào một loại, nhưng nhìn chung các triệu chứng cơ năng điển hình của bệnh tiểu đường cụ thể như sau:</p>\n<p dir=\"ltr\">– Đa niệu (lượng nước tiểu nhiều)</p>\n<p dir=\"ltr\">– Khô miệng, uống nhiều (hay khát nước và uống nhiều nước)</p>\n<p dir=\"ltr\">– Cân nặng giảm</p>\n<p dir=\"ltr\">– Trở nên dễ mệt mỏi</p>\n\n<h3 dir=\"ltr\">Bệnh tiểu đường có gì đáng sợ?</h3>\n<p dir=\"ltr\">Khi bị tiểu đường (tình trạng lượng đường trong máu vượt quá tỷ lệ nhất định), người bệnh sẽ gặp khó khăn gì?</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-02.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Nếu lượng đường trong máu tăng lên, máu trở nên đặc hơn, tạo áp lực lớn lên mạch máu. Hiện tượng này lâu dài sẽ gây ra nhiều biến chứng và đây chính là điểm đáng sợ của bệnh tiểu đường. Đặc biệt phần lớn các biến chứng của bệnh tiểu đường đều liên quan đến mao mạch và tùy từng trường hợp, bệnh nhân có thể bị mất thị giác, thậm chí các chi của chân tay có thể bị hoại tử.</p>\n\n<h3 dir=\"ltr\">Ba loại bệnh tiểu đường</h3>\n<p dir=\"ltr\">Như đã đề cập trước đó, bệnh tiểu đường là “Trạng thái lượng glucose trong máu (lượng đường trong máu) vượt quá tỷ lệ nhất định”, nhưng có rất nhiều nguyên nhân dẫn đến tình trạng này.</p>\n<p dir=\"ltr\">Tùy thuộc vào từng nguyên nhân, bệnh tiểu đường có thể được chia thành 3 loại sau đây.</p>\n<p dir=\"ltr\">– Bệnh tiểu đường tuýp là bệnh tiểu đường do các tế bào β của tuyến tụy tiết ra insulin bị phá hủy. Đây là bệnh có tỷ lệ khởi phát cao ở đối tượng trẻ em và vị thành niên, có đặc trưng là thường đột nhiên phát triển và tiến triển nhanh chóng. Ngoài ra, các triệu chứng ban đầu của bệnh rất giống với triệu chứng bệnh cảm lạnh, nhưng sau đó xuất hiện thêm các triệu chứng như khát nước, đa niệu, đột nhiên giảm cân.</p>\n<p dir=\"ltr\">Về phương pháp điều trị, khi tế bào β của tuyến tụy phá hủy, insulin không thể được tiết ra, vì vậy nếu bệnh nhân không thường xuyên cung cấp insulin từ bên ngoài vào cơ thể thì bệnh có thể gây nguy hiểm đến tính mạng. Do đó biện pháp tiêm insulin là rất cần thiết.</p>\n<p dir=\"ltr\">– Bệnh tiểu đường tuýp 2</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-03.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Tiểu đường tuýp 2 (type 2) là một loại bệnh tiểu đường khởi phát ở những người có yếu tố di truyền về bệnh tiểu đường và có “vấn đề lối sống” như béo phì, phàm ăn tục uống, thiếu vận động, căng thẳng. Bệnh tiểu đường tuýp 2 được coi là một trong “7 bệnh lối sống lớn”.</p>\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 khởi phát chủ yếu ở những người sau độ tuổi trung niên. Đặc trưng của loại bệnh tiểu đường này là gần như không có triệu chứng cơ năng, thường khởi phát mà bệnh nhân không biết. Không ít các trường hợp bệnh nhân ngạc nhiên khi lần đầu tiên phát hiện bệnh trong kỳ khám sức khỏe tại nơi làm việc và nơi sinh sống mặc dù nghĩ rằng bản thân khỏe mạnh.</p>\n<p dir=\"ltr\">Tuy nhiên, không thể không quan tâm đến bệnh tiểu đường vì cho rằng bệnh không gây đau đớn cho cơ thể. Bởi nếu để bệnh tiến triển, có thể gây ra các biến chứng như mù lòa, hoại tử bàn chân.</p>\n<p dir=\"ltr\">Tất nhiên, không thể khẳng định một cách chắc chắn về các biến chứng bởi bệnh sẽ tiến triển khác nhau tùy thuộc vào tình trạng bệnh và quá trình, nhưng ở giai đoạn đầu, bệnh nhân có thể điều chỉnh lượng đường trong máu bằng cách cải thiện lối sống như chế độ ăn uống và vận động. Tuy nhiên, theo thời gian, việc kiểm soát lượng đường trong máu chỉ bằng cách cải thiện lối sống trở nên khó khăn hơn và trong một số trường hợp, bệnh nhân có thể chuyển sang điều trị bằng thuốc như thuốc uống hoặc tiêm insulin.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-04.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Ngoài ra, có thể nói bệnh tiểu đường là một căn bệnh khó chữa khỏi. Ngay cả khi chỉ số đường huyết đã ổn định nhưng nếu bệnh nhân ngừng điều trị, chỉ số này sẽ lại tăng lên rất nhanh. Vì vậy, trường hợp bệnh nhân phát hiện bị bệnh tiểu đường type 2, hãy đến tư vấn tại một cơ sở y tế chuyên khoa để được duy trì điều trị và tiến hành xét nghiệm thích hợp.</p>\n<p dir=\"ltr\">– Bệnh tiểu đường thai kỳ</p>\n<p dir=\"ltr\">Bệnh tiểu đường thai kỳ là một tình trạng bất thường trong trao đổi chất đường do ảnh hưởng của việc mang thai và được phát hiện hoặc khởi phát lần đầu khi mang thai. Đây là một bệnh tiêu biểu ở phụ nữ mang thai, khi phụ nữ mang thai, hormone được tạo ra bởi nhau thai ức chế chức năng insulin, do đó gây ra sự tăng lượng đường trong máu.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-05.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Tất nhiên, không phải tất cả phụ nữ mang thai đều bị tiểu đường, nhưng trường hợp thai phụ béo phì, mang thai khi lớn tuổi, tiền sử gia đình thai phụ có người bị tiểu đường thường sẽ có xu hướng dễ bị tiểu đường thai kỳ hơn.</p>\n\n<h2 dir=\"ltr\">2. Biến chứng của bệnh tiểu đường là gì?</h2>\n<p dir=\"ltr\">Điều đáng lo ngại trong bệnh tiểu đường là những biến chứng khác nhau xuất hiện do lượng đường trong máu tăng lên tạo áp lực lớn lên mạch máu. Có lẽ mọi người thường nghe những câu chuyện nói rằng “Nếu bị bệnh tiểu đường, mắt sẽ trở nên không thể nhìn thấy, thận trở nên tồi tệ hơn, hoặc đầu chân tay bị hoại tử.” Thực tế, đây được coi là những triệu chứng tiêu biểu gây ra bởi 3 biến chứng chính của bệnh tiểu đường.</p>\n<p dir=\"ltr\">Phần này sẽ giới thiệu khái quát về 3 biến chứng tiểu đường chính:</p>\n\n<h3 dir=\"ltr\">Bệnh võng mạc tiểu đường</h3>\n<p dir=\"ltr\">Cơ chế con người nhìn thấy mọi thứ bằng mắt của mình có thể được mô phỏng như cấu trúc của một bộ phim. “Việc nhìn thấy mọi thứ” có thể nói rằng đó là quá trình ánh sáng đi vào từ tròng mắt được khúc xạ bởi giác mạc hoặc thủy tinh thể (thấu kính của máy chiếu), liên kết các hình ảnh được chiếu lên võng mạc (màn hình) và não ghi nhận hình ảnh đó. Việc bạn đọc ghi nhận dòng chữ được viết ở đây ngay lúc này cũng chính là từ việc cảm nhận ánh sáng trên võng mạc thông qua giác mạc và thủy tinh thể.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-06.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Võng mạc là một màng mỏng với vô số dây thần kinh ở đáy mắt và mao mạch căng xung quanh. “Bệnh võng mạc tiểu đường” là một biến chứng dẫn đến giảm thị lực và mù lòa do áp lực lên mạch máu gây ra bởi bệnh tiểu đường, máu và oxy không đủ để truyền tới mao mạch của võng mạc.</p>\n\n<h3 dir=\"ltr\">Bệnh thận do tiểu đường</h3>\n<p dir=\"ltr\">Vai trò quan trọng của thận trong cơ thể là lọc chất thải và các chất không cần thiết trong máu rồi thải chúng ra khỏi cơ thể dưới dạng nước tiểu. Có thể nói máu của cơ thể chúng ta được duy trì ở trạng thái ổn định chính là nhờ chức năng lọc của thận.</p>\n<p dir=\"ltr\">Tuy nhiên, khi lượng đường trong máu tăng lên do bệnh tiểu đường, bộ lọc thận bị tắc nghẽn, không thể xử lý được chất thải và các chất không cần thiết, khiến cho việc giữ ổn định tình trạng của máu trở nên khó khăn. “Bệnh thận do tiểu đường” là rối loạn chức năng thận do bệnh tiểu đường gây ra.</p>\n<p dir=\"ltr\">Trong trường hợp xấu nhất, có thể dẫn đến các triệu chứng đe dọa tính mạng nghiêm trọng như tăng ure máu, suy thận và có thể cần phải chạy thận nhân tạo bằng thiết bị y tế.</p>\n\n<h3 dir=\"ltr\">Bệnh thần kinh tiểu đường</h3>\n<p dir=\"ltr\">“Bệnh thần kinh tiểu đường” là một trong ba biến chứng lớn của bệnh tiểu đường cùng với bệnh võng mạc tiểu đường và bệnh thận do tiểu đường, nhưng có vẻ như phần lớn mọi người vẫn chưa hiểu rõ tại sao bệnh tiểu đường gây tổn thương thần kinh. Có rất nhiều giả thuyết khác nhau về nguyên nhân của biến chứng thần kinh tiểu đường như “Bởi vì nếu bệnh tiểu đường càng tiến triển, các tế bào thần kinh sẽ tích lũy nhiều vật chất gây tổn thương”, “Tiểu đường khiến việc lưu thông máu trong mao mạch trở nên kém đi, dẫn đến thiếu oxy và chất dinh dưỡng cần thiết cho tế bào thần kinh”, nhưng có vẻ như để nói rằng trong các giả thuyết trên, giả thuyết nào đúng thì chưa có kết luận cuối cùng.</p>\n<p dir=\"ltr\">Người ta nói rằng các triệu chứng ban đầu của bệnh thần kinh tiểu đường thường là bị tê và đau chủ yếu ở các ngón chân và bàn chân. Về cảm giác, các triệu chứng thường được biểu hiện ở trạng thái “châm chích”, “đau rát”,…, khi các triệu chứng tiến triển, đau và tê sẽ dần dần xuất hiện ở các ngón tay. Hơn nữa, khi thần kinh tiếp tục bị tổn thương, cảm giác của tứ chi trở nên chậm hơn và bệnh nhân thường bị thương trên tay chân mà không biết.</p>\n<p dir=\"ltr\">Điều phải chú ý đặc biệt trong bệnh thần kinh tiểu đường là khi bệnh nhân không chú ý đến một vết trầy xước rất nhỏ do đi giày bị chật hoặc bị cứa, vết thương có thể trở nên tồi tệ hơn như bị nhiễm khuẩn và thời điểm bệnh nhân đi khám tại bệnh viện thì tay và chân đã bị hư hỏng nặng không thể sử dụng được nữa, cách chữa trị duy nhất là cắt bỏ chân tay. Bệnh nhân tiểu đường nên xác nhận trực quan các chi của chân tay trong khi tắm.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-07.jpg\" alt=\"\" /></p>\n\n<h2 dir=\"ltr\">3. Phương pháp điều trị bệnh tiểu đường</h2>\n<p dir=\"ltr\">Có ba phương pháp điều trị bệnh tiểu đường tiêu biểu. Liệu pháp ăn uống, liệu pháp vận động, điều trị bằng thuốc. Cả ba đều là phương pháp điều trị nhằm kiểm soát mức đường trong máu và giảm áp lực lên các mạch máu, có thể áp dụng tùy theo loại và triệu chứng của bệnh tiểu đường.</p>\n<p dir=\"ltr\">Hãy cùng tìm hiểu cụ thể về ba phương pháp điều trị này.</p>\n\n<h3 dir=\"ltr\">Ăn uống</h3>\n<p dir=\"ltr\">Liệu pháp ăn uống là liệu pháp nhằm điều chỉnh lượng đường trong máu bằng thói quen ăn uống lành mạnh có cân bằng. Các điểm chung cần lưu ý được đưa ra là “ăn từ từ và nhai kỹ”, “ăn uống lành mạnh, điều độ vào buổi sáng, buổi trưa và buổi tối”, “không ăn vào đêm muộn hoặc trước khi đi ngủ”, “không ăn quá no, chỉ nên ăn vừa đủ no”, “Cố gắng cân bằng dinh dưỡng với nhận thức đầy đủ về khoáng chất và vitamin, đặc biệt là ba chất dinh dưỡng chính carbohydrate, protein và chất béo”.</p>\n<p dir=\"ltr\">Tuy nhiên, vì sự thèm ăn là một trong 3 ham muốn tuyệt vời của con người, ngay cả khi nhận thức điều đó trong đầu, đôi khi bệnh nhân vẫn bị rối loạn trong thói quen ăn uống như “muốn ăn đồ ăn có dầu mỡ!”, “muốn ăn đến no căng bụng”. Để giải quyết vấn đề này, đôi khi có thể thử tạo “ngày đặc biệt để ăn đồ bản thân thích”.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-08.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Dù bằng cách nào, do đây là “điều trị” bằng chế độ ăn uống nên vẫn cần tham khảo ý kiến bác sĩ phụ trách.</p>\n\n<h3 dir=\"ltr\">Vận động</h3>\n<p dir=\"ltr\">Liệu pháp vận động là một phương pháp điều trị thúc đẩy glucose trong máu được đưa vào tế bào để trở thành năng lượng và làm giảm lượng đường trong máu thông qua việc vận động. Tuy nhiên, trong điều trị tiểu đường, không phải loại vận động nào cũng tốt, nhưng nhìn chung thì tập thể dục nhịp điệu vừa phải với mức độ cảm thấy “hơi khó khăn…” và rèn luyện cơ bắp dường như hiệu quả hơn vận động nặng như thi đấu của vận động viên thể thao,.</p>\n<p dir=\"ltr\">Ngoài ra, khi bệnh nhân bắt đầu tăng cường vận động nặng nhưng bỏ cuộc giữa chừng do cảm thấy khó hoặc làm tổn thương cơ thể thì việc tập tăng cường cũng không mang lại hiệu quả gì. Hãy tham khảo ý kiến bác sĩ phụ trách trước và lập một chương trình tập luyện ổn định, hiệu quả và phù hợp với khả năng của bản thân.</p>\n\n<h3 dir=\"ltr\">Điều trị bằng thuốc</h3>\n<p dir=\"ltr\">Điều trị bằng thuốc là một phương pháp điều trị kiểm soát lượng đường trong máu trong phạm vi bình thường với sự hỗ trợ của thuốc. Có hai phương pháp chính của việc điều trị bằng thuốc là “tiêm insulin” và “uống thuốc hạ đường huyết”, mỗi phương pháp có cách tiến hành khác nhau.</p>\n<p dir=\"ltr\">Như đã đề cập trước đó, bệnh tiểu đường là một căn bệnh do lượng đường trong máu tăng lên khi tuyến tụy không tiết insulin hoặc lượng insulin tiết ra bị giảm.</p>\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-09.jpg\" alt=\"\" /></p>\n<p dir=\"ltr\">Về cơ bản, có vẻ như điều trị bằng loại thuốc nào phụ thuộc vào “lượng insulin tiết ra từ tuyến tụy”. Thông thường, trường hợp lượng insulin tiết ra từ tuyến tụy rất thấp/gần như không có, biện pháp tiêm insulin được áp dụng, trường hợp việc tiết insulin từ tuyến tụy được duy trì ở một mức độ nào đó, biện pháp “uống thuốc hạ đường huyết” được áp dụng.</p>\n\n<h2 dir=\"ltr\">4. Chi phí điều trị bệnh tiểu đường</h2>\n<p dir=\"ltr\">Bệnh tiểu đường thường được ví là “bệnh nhà giàu” bởi thời gian điều trị lâu dài và chi phí điều trị vô cùng tốn kém. Tuy nhiên chi phí điều trị bệnh tiểu đường khác nhau tùy thuộc vào sự tiến triển của bệnh và tình trạng của bệnh nhân.</p>\n<p dir=\"ltr\">Bài viết này đã giới thiệu khái quát về triệu chứng, nguyên nhân, biến chứng, phương pháp điều trị, chi phí điều trị,…của bệnh tiểu đường nói chung. Sau khi đọc xong bài viết, hy vọng mọi người đã có thể hình dung rõ hơn về bệnh tiểu đường nói chung.</p>', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '90-autosave-v1', '', '', '2020-06-12 10:23:51', '2020-06-12 10:23:51', '', 90, 'http://yentheme.test:8080/90-autosave-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(257, 1, '2020-06-12 10:23:59', '2020-06-12 10:23:59', '<p dir=\"ltr\">Bài viết dưới đây sẽ khái quát những điểm cơ bản như “bệnh tiểu đường là gì?”, các triệu chứng, phân loại, nguyên nhân, cách điều trị, chi phí điều trị bệnh tiểu đường. Thông qua bài viết này, mọi người sẽ có kiến thức cơ bản về bệnh tiểu đường!</p>\r\n\r\n<h2 dir=\"ltr\">1. Bệnh tiểu đường là bệnh như thế nào?</h2>\r\n<h3 dir=\"ltr\">Tìm hiểu bệnh tiểu đường là gì?</h3>\r\n<p dir=\"ltr\">Chúng ta ăn uống hàng ngày. Lượng carbohydrates từ bữa ăn được hấp thu vào đường ruột như glucose và hòa tan trong máu. Khi đó, một hormone gọi là insulin được tiết ra, nhờ insulin tiết ra, glucose được đưa vào các tế bào và trở thành nguồn năng lượng của cơ thể.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-01(1).jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Nếu sự hoạt động của insulin không hiệu quả và nếu glucose tăng lên đến mức mà việc xử lý của insulin không thể đáp ứng được, thì sẽ có một lượng lớn glucose không được chuyển hóa thành nguồn năng lượng của cơ thể và bị dư thừa trong máu. “Tình trạng lượng glucose trong máu (lượng đường trong máu) vượt quá tỷ lệ nhất định” được gọi là “bệnh tiểu đường”.</p>\r\n<p dir=\"ltr\">Do bệnh tiểu đường có một số loại khác nhau nên không thể gộp chung vào một loại, nhưng nhìn chung các triệu chứng cơ năng điển hình của bệnh tiểu đường cụ thể như sau:</p>\r\n<p dir=\"ltr\">– Đa niệu (lượng nước tiểu nhiều)</p>\r\n<p dir=\"ltr\">– Khô miệng, uống nhiều (hay khát nước và uống nhiều nước)</p>\r\n<p dir=\"ltr\">– Cân nặng giảm</p>\r\n<p dir=\"ltr\">– Trở nên dễ mệt mỏi</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh tiểu đường có gì đáng sợ?</h3>\r\n<p dir=\"ltr\">Khi bị tiểu đường (tình trạng lượng đường trong máu vượt quá tỷ lệ nhất định), người bệnh sẽ gặp khó khăn gì?</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-02.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Nếu lượng đường trong máu tăng lên, máu trở nên đặc hơn, tạo áp lực lớn lên mạch máu. Hiện tượng này lâu dài sẽ gây ra nhiều biến chứng và đây chính là điểm đáng sợ của bệnh tiểu đường. Đặc biệt phần lớn các biến chứng của bệnh tiểu đường đều liên quan đến mao mạch và tùy từng trường hợp, bệnh nhân có thể bị mất thị giác, thậm chí các chi của chân tay có thể bị hoại tử.</p>\r\n\r\n<h3 dir=\"ltr\">Ba loại bệnh tiểu đường</h3>\r\n<p dir=\"ltr\">Như đã đề cập trước đó, bệnh tiểu đường là “Trạng thái lượng glucose trong máu (lượng đường trong máu) vượt quá tỷ lệ nhất định”, nhưng có rất nhiều nguyên nhân dẫn đến tình trạng này.</p>\r\n<p dir=\"ltr\">Tùy thuộc vào từng nguyên nhân, bệnh tiểu đường có thể được chia thành 3 loại sau đây.</p>\r\n<p dir=\"ltr\">– Bệnh tiểu đường tuýp là bệnh tiểu đường do các tế bào β của tuyến tụy tiết ra insulin bị phá hủy. Đây là bệnh có tỷ lệ khởi phát cao ở đối tượng trẻ em và vị thành niên, có đặc trưng là thường đột nhiên phát triển và tiến triển nhanh chóng. Ngoài ra, các triệu chứng ban đầu của bệnh rất giống với triệu chứng bệnh cảm lạnh, nhưng sau đó xuất hiện thêm các triệu chứng như khát nước, đa niệu, đột nhiên giảm cân.</p>\r\n<p dir=\"ltr\">Về phương pháp điều trị, khi tế bào β của tuyến tụy phá hủy, insulin không thể được tiết ra, vì vậy nếu bệnh nhân không thường xuyên cung cấp insulin từ bên ngoài vào cơ thể thì bệnh có thể gây nguy hiểm đến tính mạng. Do đó biện pháp tiêm insulin là rất cần thiết.</p>\r\n<p dir=\"ltr\">– Bệnh tiểu đường tuýp 2</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-03.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 2 (type 2) là một loại bệnh tiểu đường khởi phát ở những người có yếu tố di truyền về bệnh tiểu đường và có “vấn đề lối sống” như béo phì, phàm ăn tục uống, thiếu vận động, căng thẳng. Bệnh tiểu đường tuýp 2 được coi là một trong “7 bệnh lối sống lớn”.</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 khởi phát chủ yếu ở những người sau độ tuổi trung niên. Đặc trưng của loại bệnh tiểu đường này là gần như không có triệu chứng cơ năng, thường khởi phát mà bệnh nhân không biết. Không ít các trường hợp bệnh nhân ngạc nhiên khi lần đầu tiên phát hiện bệnh trong kỳ khám sức khỏe tại nơi làm việc và nơi sinh sống mặc dù nghĩ rằng bản thân khỏe mạnh.</p>\r\n<p dir=\"ltr\">Tuy nhiên, không thể không quan tâm đến bệnh tiểu đường vì cho rằng bệnh không gây đau đớn cho cơ thể. Bởi nếu để bệnh tiến triển, có thể gây ra các biến chứng như mù lòa, hoại tử bàn chân.</p>\r\n<p dir=\"ltr\">Tất nhiên, không thể khẳng định một cách chắc chắn về các biến chứng bởi bệnh sẽ tiến triển khác nhau tùy thuộc vào tình trạng bệnh và quá trình, nhưng ở giai đoạn đầu, bệnh nhân có thể điều chỉnh lượng đường trong máu bằng cách cải thiện lối sống như chế độ ăn uống và vận động. Tuy nhiên, theo thời gian, việc kiểm soát lượng đường trong máu chỉ bằng cách cải thiện lối sống trở nên khó khăn hơn và trong một số trường hợp, bệnh nhân có thể chuyển sang điều trị bằng thuốc như thuốc uống hoặc tiêm insulin.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-04.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Ngoài ra, có thể nói bệnh tiểu đường là một căn bệnh khó chữa khỏi. Ngay cả khi chỉ số đường huyết đã ổn định nhưng nếu bệnh nhân ngừng điều trị, chỉ số này sẽ lại tăng lên rất nhanh. Vì vậy, trường hợp bệnh nhân phát hiện bị bệnh tiểu đường type 2, hãy đến tư vấn tại một cơ sở y tế chuyên khoa để được duy trì điều trị và tiến hành xét nghiệm thích hợp.</p>\r\n<p dir=\"ltr\">– Bệnh tiểu đường thai kỳ</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường thai kỳ là một tình trạng bất thường trong trao đổi chất đường do ảnh hưởng của việc mang thai và được phát hiện hoặc khởi phát lần đầu khi mang thai. Đây là một bệnh tiêu biểu ở phụ nữ mang thai, khi phụ nữ mang thai, hormone được tạo ra bởi nhau thai ức chế chức năng insulin, do đó gây ra sự tăng lượng đường trong máu.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-05.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tất nhiên, không phải tất cả phụ nữ mang thai đều bị tiểu đường, nhưng trường hợp thai phụ béo phì, mang thai khi lớn tuổi, tiền sử gia đình thai phụ có người bị tiểu đường thường sẽ có xu hướng dễ bị tiểu đường thai kỳ hơn.</p>\r\n\r\n<h2 dir=\"ltr\">2. Biến chứng của bệnh tiểu đường là gì?</h2>\r\n<p dir=\"ltr\">Điều đáng lo ngại trong bệnh tiểu đường là những biến chứng khác nhau xuất hiện do lượng đường trong máu tăng lên tạo áp lực lớn lên mạch máu. Có lẽ mọi người thường nghe những câu chuyện nói rằng “Nếu bị bệnh tiểu đường, mắt sẽ trở nên không thể nhìn thấy, thận trở nên tồi tệ hơn, hoặc đầu chân tay bị hoại tử.” Thực tế, đây được coi là những triệu chứng tiêu biểu gây ra bởi 3 biến chứng chính của bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">Phần này sẽ giới thiệu khái quát về 3 biến chứng tiểu đường chính:</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh võng mạc tiểu đường</h3>\r\n<p dir=\"ltr\">Cơ chế con người nhìn thấy mọi thứ bằng mắt của mình có thể được mô phỏng như cấu trúc của một bộ phim. “Việc nhìn thấy mọi thứ” có thể nói rằng đó là quá trình ánh sáng đi vào từ tròng mắt được khúc xạ bởi giác mạc hoặc thủy tinh thể (thấu kính của máy chiếu), liên kết các hình ảnh được chiếu lên võng mạc (màn hình) và não ghi nhận hình ảnh đó. Việc bạn đọc ghi nhận dòng chữ được viết ở đây ngay lúc này cũng chính là từ việc cảm nhận ánh sáng trên võng mạc thông qua giác mạc và thủy tinh thể.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-06.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Võng mạc là một màng mỏng với vô số dây thần kinh ở đáy mắt và mao mạch căng xung quanh. “Bệnh võng mạc tiểu đường” là một biến chứng dẫn đến giảm thị lực và mù lòa do áp lực lên mạch máu gây ra bởi bệnh tiểu đường, máu và oxy không đủ để truyền tới mao mạch của võng mạc.</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh thận do tiểu đường</h3>\r\n<p dir=\"ltr\">Vai trò quan trọng của thận trong cơ thể là lọc chất thải và các chất không cần thiết trong máu rồi thải chúng ra khỏi cơ thể dưới dạng nước tiểu. Có thể nói máu của cơ thể chúng ta được duy trì ở trạng thái ổn định chính là nhờ chức năng lọc của thận.</p>\r\n<p dir=\"ltr\">Tuy nhiên, khi lượng đường trong máu tăng lên do bệnh tiểu đường, bộ lọc thận bị tắc nghẽn, không thể xử lý được chất thải và các chất không cần thiết, khiến cho việc giữ ổn định tình trạng của máu trở nên khó khăn. “Bệnh thận do tiểu đường” là rối loạn chức năng thận do bệnh tiểu đường gây ra.</p>\r\n<p dir=\"ltr\">Trong trường hợp xấu nhất, có thể dẫn đến các triệu chứng đe dọa tính mạng nghiêm trọng như tăng ure máu, suy thận và có thể cần phải chạy thận nhân tạo bằng thiết bị y tế.</p>\r\n\r\n<h3 dir=\"ltr\">Bệnh thần kinh tiểu đường</h3>\r\n<p dir=\"ltr\">“Bệnh thần kinh tiểu đường” là một trong ba biến chứng lớn của bệnh tiểu đường cùng với bệnh võng mạc tiểu đường và bệnh thận do tiểu đường, nhưng có vẻ như phần lớn mọi người vẫn chưa hiểu rõ tại sao bệnh tiểu đường gây tổn thương thần kinh. Có rất nhiều giả thuyết khác nhau về nguyên nhân của biến chứng thần kinh tiểu đường như “Bởi vì nếu bệnh tiểu đường càng tiến triển, các tế bào thần kinh sẽ tích lũy nhiều vật chất gây tổn thương”, “Tiểu đường khiến việc lưu thông máu trong mao mạch trở nên kém đi, dẫn đến thiếu oxy và chất dinh dưỡng cần thiết cho tế bào thần kinh”, nhưng có vẻ như để nói rằng trong các giả thuyết trên, giả thuyết nào đúng thì chưa có kết luận cuối cùng.</p>\r\n<p dir=\"ltr\">Người ta nói rằng các triệu chứng ban đầu của bệnh thần kinh tiểu đường thường là bị tê và đau chủ yếu ở các ngón chân và bàn chân. Về cảm giác, các triệu chứng thường được biểu hiện ở trạng thái “châm chích”, “đau rát”,…, khi các triệu chứng tiến triển, đau và tê sẽ dần dần xuất hiện ở các ngón tay. Hơn nữa, khi thần kinh tiếp tục bị tổn thương, cảm giác của tứ chi trở nên chậm hơn và bệnh nhân thường bị thương trên tay chân mà không biết.</p>\r\n<p dir=\"ltr\">Điều phải chú ý đặc biệt trong bệnh thần kinh tiểu đường là khi bệnh nhân không chú ý đến một vết trầy xước rất nhỏ do đi giày bị chật hoặc bị cứa, vết thương có thể trở nên tồi tệ hơn như bị nhiễm khuẩn và thời điểm bệnh nhân đi khám tại bệnh viện thì tay và chân đã bị hư hỏng nặng không thể sử dụng được nữa, cách chữa trị duy nhất là cắt bỏ chân tay. Bệnh nhân tiểu đường nên xác nhận trực quan các chi của chân tay trong khi tắm.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-07.jpg\" alt=\"\" /></p>\r\n\r\n<h2 dir=\"ltr\">3. Phương pháp điều trị bệnh tiểu đường</h2>\r\n<p dir=\"ltr\">Có ba phương pháp điều trị bệnh tiểu đường tiêu biểu. Liệu pháp ăn uống, liệu pháp vận động, điều trị bằng thuốc. Cả ba đều là phương pháp điều trị nhằm kiểm soát mức đường trong máu và giảm áp lực lên các mạch máu, có thể áp dụng tùy theo loại và triệu chứng của bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">Hãy cùng tìm hiểu cụ thể về ba phương pháp điều trị này.</p>\r\n\r\n<h3 dir=\"ltr\">Ăn uống</h3>\r\n<p dir=\"ltr\">Liệu pháp ăn uống là liệu pháp nhằm điều chỉnh lượng đường trong máu bằng thói quen ăn uống lành mạnh có cân bằng. Các điểm chung cần lưu ý được đưa ra là “ăn từ từ và nhai kỹ”, “ăn uống lành mạnh, điều độ vào buổi sáng, buổi trưa và buổi tối”, “không ăn vào đêm muộn hoặc trước khi đi ngủ”, “không ăn quá no, chỉ nên ăn vừa đủ no”, “Cố gắng cân bằng dinh dưỡng với nhận thức đầy đủ về khoáng chất và vitamin, đặc biệt là ba chất dinh dưỡng chính carbohydrate, protein và chất béo”.</p>\r\n<p dir=\"ltr\">Tuy nhiên, vì sự thèm ăn là một trong 3 ham muốn tuyệt vời của con người, ngay cả khi nhận thức điều đó trong đầu, đôi khi bệnh nhân vẫn bị rối loạn trong thói quen ăn uống như “muốn ăn đồ ăn có dầu mỡ!”, “muốn ăn đến no căng bụng”. Để giải quyết vấn đề này, đôi khi có thể thử tạo “ngày đặc biệt để ăn đồ bản thân thích”.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-08.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Dù bằng cách nào, do đây là “điều trị” bằng chế độ ăn uống nên vẫn cần tham khảo ý kiến bác sĩ phụ trách.</p>\r\n\r\n<h3 dir=\"ltr\">Vận động</h3>\r\n<p dir=\"ltr\">Liệu pháp vận động là một phương pháp điều trị thúc đẩy glucose trong máu được đưa vào tế bào để trở thành năng lượng và làm giảm lượng đường trong máu thông qua việc vận động. Tuy nhiên, trong điều trị tiểu đường, không phải loại vận động nào cũng tốt, nhưng nhìn chung thì tập thể dục nhịp điệu vừa phải với mức độ cảm thấy “hơi khó khăn…” và rèn luyện cơ bắp dường như hiệu quả hơn vận động nặng như thi đấu của vận động viên thể thao,.</p>\r\n<p dir=\"ltr\">Ngoài ra, khi bệnh nhân bắt đầu tăng cường vận động nặng nhưng bỏ cuộc giữa chừng do cảm thấy khó hoặc làm tổn thương cơ thể thì việc tập tăng cường cũng không mang lại hiệu quả gì. Hãy tham khảo ý kiến bác sĩ phụ trách trước và lập một chương trình tập luyện ổn định, hiệu quả và phù hợp với khả năng của bản thân.</p>\r\n\r\n<h3 dir=\"ltr\">Điều trị bằng thuốc</h3>\r\n<p dir=\"ltr\">Điều trị bằng thuốc là một phương pháp điều trị kiểm soát lượng đường trong máu trong phạm vi bình thường với sự hỗ trợ của thuốc. Có hai phương pháp chính của việc điều trị bằng thuốc là “tiêm insulin” và “uống thuốc hạ đường huyết”, mỗi phương pháp có cách tiến hành khác nhau.</p>\r\n<p dir=\"ltr\">Như đã đề cập trước đó, bệnh tiểu đường là một căn bệnh do lượng đường trong máu tăng lên khi tuyến tụy không tiết insulin hoặc lượng insulin tiết ra bị giảm.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tong-quan-kien-thuc-ve-benh-tieu-duong-09.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Về cơ bản, có vẻ như điều trị bằng loại thuốc nào phụ thuộc vào “lượng insulin tiết ra từ tuyến tụy”. Thông thường, trường hợp lượng insulin tiết ra từ tuyến tụy rất thấp/gần như không có, biện pháp tiêm insulin được áp dụng, trường hợp việc tiết insulin từ tuyến tụy được duy trì ở một mức độ nào đó, biện pháp “uống thuốc hạ đường huyết” được áp dụng.</p>\r\n\r\n<h2 dir=\"ltr\">4. Chi phí điều trị bệnh tiểu đường</h2>\r\n<p dir=\"ltr\">Bệnh tiểu đường thường được ví là “bệnh nhà giàu” bởi thời gian điều trị lâu dài và chi phí điều trị vô cùng tốn kém. Tuy nhiên chi phí điều trị bệnh tiểu đường khác nhau tùy thuộc vào sự tiến triển của bệnh và tình trạng của bệnh nhân.</p>\r\n<p dir=\"ltr\">Bài viết này đã giới thiệu khái quát về triệu chứng, nguyên nhân, biến chứng, phương pháp điều trị, chi phí điều trị,…của bệnh tiểu đường nói chung. Sau khi đọc xong bài viết, hy vọng mọi người đã có thể hình dung rõ hơn về bệnh tiểu đường nói chung.</p>', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2020-06-12 10:23:59', '2020-06-12 10:23:59', '', 90, 'http://yentheme.test:8080/90-revision-v1/', 0, 'revision', '', 0),
(258, 1, '2020-06-12 11:57:08', '2020-06-12 11:57:08', '<p dir=\"ltr\">Phần lớn tiểu đường gặp ở trẻ em chủ yếu là tiểu đường tuýp 1 (còn gọi là tiểu đường vị thành niên). Tuy nhiên, ngày nay xu hướng trẻ em mắc tiểu đường tuýp 2 càng ngày càng cao.</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 1 (còn gọi là tiểu đường vị thành niên): được xếp vào nhóm bệnh tự miễn, có nghĩa là do hệ thống miễn dịch tấn công nhầm vào các bộ phận của cơ thể. Cụ thể ở đây là hệ thống miễn dịch xác định sai mục tiêu và hướng đến các tế bào beta sản xuất insulin trong tuyến tụy. Do đó tuyến tụy không thể sản xuất được insulin. Vì không có insulin nên đường không thể đi từ máu vào tế bào và lượng đường trong máu sẽ tăng cao nếu không được điều trị.</p>\r\n<p dir=\"ltr\"><strong>Dấu hiệu của tiểu đường tuýp 1 ở trẻ em:</strong></p>\r\n<p dir=\"ltr\">Hay khát nước, đi tiểu nhiều; Hay đói; Sụt giảm cân không mong muốn; Mệt mỏi, ngại hoạt động; Yếu đuối; Khó chịu, hay cáu gắt, mè nheo, thay đổi tâm trạng; Giảm thị lực</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 2 : hiếm gặp ở trẻ em, xảy ra khi cơ thể mất khả năng đáp ứng lại insulin (kháng insulin). Trong tình trạng kháng insulin, gan, cơ và các tế bào mỡ giảm khả năng sử dụng insulin, tác động này làm cản trở mang đường vào trong các tế bào của cơ thể. Do đó, cơ thể sẽ phải sản xuất nhiều insulin hơn để thu nhận đường vào trong tế bào. Tuyến tụy cố gắng đáp ứng nhu cầu bằng cách gia tăng sản xuất thêm insulin. Tuy nhiên qua thời gian, tuyến tụy sẽ không thể đáp ứng được đầy đủ sản xuất insulin, khi có sự gia tăng mức đường trong máu.</p>\r\n<p dir=\"ltr\">Ngày nay, do lối sống, do dinh dưỡng thiếu hợp lý, tình trạng thừa cân béo phì càng ngày càng gia tăng ở trẻ em, do đó tỷ lệ tiểu đường ở trẻ em cũng có xu hướng tăng lên. Điều này cảnh báo các bậc cha mẹ cần kiểm soát tốt chế độ ăn uống, vận động và cân nặng của trẻ.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-01.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Dấu hiệu của tiểu đường tuýp 2 ở trẻ em: hay gặp ở những trẻ thừa cân, béo phì, ít hoạt động; Đi tiểu nhiều hơn đặc biệt vào ban đêm; Mệt mỏi; Giảm cân không rõ nguyên nhân; Khát nước, cơn khát tăng dần; Ngứa xung quanh bộ phận sinh dục (có thể do nhiễm nấm men); Các vết thương, vết loét khó liền, liền chậm; Mờ mắt, thị lực giảm.</p>\r\n<p dir=\"ltr\"><strong>Tiến triển, biến chứng của tiểu đường ở trẻ em: </strong></p>\r\n<p dir=\"ltr\">Tiểu đường ở trẻ em có thể ảnh hưởng đến nhiều cơ quan lớn trong cơ thể, bao gồm tim, mạch máu, dây thần kinh, mắt và thận. Giữ lượng đường trong máu gần mức bình thường hầu hết thời gian có thể làm giảm nguy cơ biến chứng rất nhiều.</p>\r\n<p dir=\"ltr\">Các biến chứng lâu dài của bệnh tiểu đường ở trẻ phát triển dần dần, qua nhiều năm. Nếu phát triển bệnh tiểu đường sớm, ít kiểm soát lượng đường trong máu thì nguy cơ biến chứng cao hơn. Cuối cùng, các biến chứng bệnh tiểu đường có thể vô hiệu hóa hoặc thậm chí đe dọa tính mạng.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-02.png\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tim và bệnh mạch máu. Bệnh tiểu đường làm tăng đáng kể nguy cơ tim mạch, bao gồm bệnh động mạch vành với đau ngực (đau thắt ngực), đau tim, đột quỵ, thu hẹp động mạch (xơ vữa động mạch) và huyết áp cao. Trong thực tế, khoảng 65 phần trăm những người đã chết vì bệnh tiểu đường do một số loại bệnh mạch máu hay tim, theo Hiệp hội tim mạch Mỹ.</p>\r\n<p dir=\"ltr\">Thần kinh hư hại (neuropathy). Dư thừa đường có thể làm tổn thương thành của các mạch máu nhỏ (mao mạch) nuôi dưỡng các dây thần kinh, đặc biệt là ở chân. Điều này có thể gây ngứa, tê, nóng hoặc bị đau thường bắt đầu các ngón chân hoặc ngón tay và dần dần lan lên trên. Khó kiểm soát lượng đường trong máu có thể làm cho mất cảm giác ở các chi bị ảnh hưởng. Thiệt hại các dây thần kinh kiểm soát sự tiêu hóa có thể gây ra vấn đề với buồn nôn, tiêu chảy, nôn mửa hoặc táo bón.</p>\r\n<p dir=\"ltr\">Tổn thương thận: Thận có chứa hàng triệu mạch máu nhỏ, cụm lọc chất thải khỏi máu. Bệnh tiểu đường có thể làm hỏng hệ thống lọc. Tổn thương nghiêm trọng có thể dẫn đến suy thận hoặc bệnh thận giai đoạn cuối không thể đảo ngược, chạy thận hoặc ghép thận được đòi hỏi.</p>\r\n<p dir=\"ltr\">Tổn thương mắt: Bệnh tiểu đường có thể làm hỏng các mạch máu của võng mạc (bệnh lý võng mạc tiểu đường), có khả năng dẫn đến mù lòa. Bệnh tiểu đường cũng làm tăng nguy cơ bị các vấn đề tầm nhìn nghiêm trọng khác, như đục thủy tinh thể và tăng nhãn áp.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-03.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Tổn thương chân. Thiệt hại thần kinh ở bàn chân hoặc lưu lượng máu kém làm tăng nguy cơ biến chứng bàn chân. Nếu không điều trị, vết thương có thể trở nên nhiễm trùng nặng. Thiệt hại nghiêm trọng ngón chân, bàn chân có thể yêu cầu cắt cụt.</p>\r\n<p dir=\"ltr\">Tổn thương da và răng miệng: Bệnh tiểu đường có thể dễ bị vấn đề về da, kể cả nhiễm trùng do vi khuẩn và nấm. Nhiễm trùng miệng cũng có thể là một mối quan tâm, đặc biệt là nếu có lịch sử vệ sinh răng miệng kém.</p>\r\n<p dir=\"ltr\">Loãng xương, còi xương: Bệnh tiểu đường có thể dẫn đến mật độ xương thấp hơn so với bình thường, làm tăng nguy cơ loãng xương. Trẻ dễ bị còi xương, khó phát triển chiều cao.</p>\r\n<p dir=\"ltr\"><strong>Dự phòng và điều trị tiểu đường ở trẻ em:</strong></p>\r\n<p dir=\"ltr\">Vai trò của bố mẹ, người lớn trong việc giáo dục trẻ em, đồng hành cùng với trẻ, tạo ra một môi trường lành mạnh, một chế độ dinh dưỡng cân bằng khoa học cũng như hình thành ý thức chăm sóc sức khoẻ của trẻ là hết sức cần thiết và quan trọng. Cần thực hiện một số nội dung sau đây:</p>\r\n<p dir=\"ltr\">+ Áp dụng chiến lược “kiềng 3 chân” triệt để: xây dựng, điều chỉnh chế độ ăn hợp lý, khoa học giúp trẻ giảm bớt cân khi thừa cân; lựa chọn các thực phẩm thân thiện cho tiểu đường và phù hợp với trẻ; Động viên khích lệ trẻ hoạt động phù hợp theo đặc điểm tuổi, tình trạng sức khoẻ; Sử dụng các thuốc kiểm soát đường máu theo chỉ dẫn của bác sỹ.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-o-tre-em-04.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Thường xuyên cho trẻ khám sức khỏe định kỳ, trong đó có xét</p>\r\n<p dir=\"ltr\">nghiệm tiểu đường cho trẻ em. Quá trình xét nghiệm tiểu đường cho trẻ em khá đơn giản và nhanh chóng. Để xét nghiệm tiểu đường cho trẻ em, bác sĩ sẽ tiến hành xét nghiệm đường huyết trong nước tiểu hay trong máu.</p>\r\n<p dir=\"ltr\">+ Đối với những trẻ đã bị mắc bệnh, cần giáo dục cho trẻ ý thức tự</p>\r\n<p dir=\"ltr\">bảo vệ bản thân khỏi những tác nhân gây hại như tránh để bị trầy xước, áp dụng chế độ ăn uống và tập luyện hợp lý.</p>\r\n<p dir=\"ltr\">+ Điều quan trọng là cha mẹ cần tạo cho trẻ một môi trường sống</p>\r\n<p dir=\"ltr\">bình thường như những đứa trẻ khác để trẻ được phát triển toàn diện. Việc này sẽ giúp nâng cao hiệu quả chữa trị bệnh đái tháo đường trẻ em.</p>\r\n<p dir=\"ltr\">Giống như nhiều bệnh lý khác, đái tháo đường trẻ em thường không có biểu hiện rõ rệt nên rất khó nhận biết. Do đó, nếu nghi ngờ trẻ mắc bệnh cha mẹ cần tiến hành xét nghiệm tiểu đường cho trẻ em để chẩn đoán và điều trị bệnh kịp thời.</p>', 'Tiểu đường ở trẻ em', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-06-12 11:57:08', '2020-06-12 11:57:08', '', 119, 'http://yentheme.test:8080/uncategorized/119-revision-v1/', 0, 'revision', '', 0),
(259, 1, '2020-06-12 11:57:41', '2020-06-12 11:57:41', '<p dir=\"ltr\">Về mặt lý thuyết, tất cả mọi người đều có thể mắc bệnh tiểu đường. Tuy nhiên, những đối tượng sau đây có nguy cơ mắc bệnh tiểu đường cao hơn:</p>\r\n<p dir=\"ltr\"><strong>Với bệnh tiểu đường tuýp 1: </strong></p>\r\n<p dir=\"ltr\">- Những người có bố, mẹ hoặc cả bố và mẹ bị tiểu đường tuýp 1(di truyền) - Yếu tố địa lý: các nghiên cứu cho thấy đi từ đường xích đạo, những người càng sống xa đường xích đạo tỷ lệ mắc tiểu đường tuýp 1 càng cao. Phần lan, Sardinia là những nước có tỷ lệ mắc TĐ tuýp 1 cao.</p>\r\n<p dir=\"ltr\">- Các yếu tố nguy cơ khác:</p>\r\n<p dir=\"ltr\">+ Nhiễm virus: nhiễm các virus như Epstein-Barr; Coxsackie virus; virus quai bị; cytomegalo virus…(virus gây tổn thương các tế bào bêta của tuỵ đảo, nơi sản xuất insulin)</p>\r\n<p dir=\"ltr\">+ Các nghiên cứu cho thấy: Những người bị thiếu vitamin D, omega 3 có tỷ lệ mắc tiểu đường tuýp 1 cao hơn.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/nhung-ai-co-nguy-co-mac-tieu-duong-01.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Những trẻ em có mẹ quá trẻ, trong quá trình mang thai có tiền sản giật, bị nhiễm trùng hô hấp cũng có tỷ lệ mắc tiểu đường tuýp 1 cao hơn.</p>\r\n<p dir=\"ltr\"><strong>Với bệnh tiểu đường tuýp 2: </strong></p>\r\n<p dir=\"ltr\">- Những người trên 40 tuổi</p>\r\n<p dir=\"ltr\">- Những người có thừa cân, béo phì, đặc biệt chỉ số BMI trên 30 và có tỷ lệ mỡ nội tạng cao, đặc biệt có gan nhiễm mỡ</p>\r\n<p dir=\"ltr\">- Những người trong gia đình có bố, mẹ anh chị em người bị tiểu đường (yếu tố gia đình)</p>\r\n<p dir=\"ltr\">- Những người bị bệnh cao huyết áp và các bệnh lý chuyển hoá khác như gout, rối loạn chuyển hóa mỡ máu.</p>\r\n<p dir=\"ltr\">-  Những người có thói quen trì trệ, lười vận động và lối sống không lành mạnh.</p>\r\n<p dir=\"ltr\">- Phụ nữ đã có tiền sử tiểu đường thai kỳ</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/nhung-ai-co-nguy-co-mac-tieu-duong-02.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\"><strong>Tiểu đường thai kỳ: </strong></p>\r\n<p dir=\"ltr\">- Những phụ nữ thừa cân, béo phì có thai</p>\r\n<p dir=\"ltr\">- Phụ nữ mang thai sau tuổi 35</p>\r\n<p dir=\"ltr\">- Những phụ nữ đã từng bị tiểu đường thai kỳ trong những lần mang thai trước</p>\r\n<p dir=\"ltr\">- Những phụ nữ mà bố, mẹ, anh chị em ruột trong gia đình bị mắc tiểu đường tuýp 2</p>\r\n<p dir=\"ltr\">- Yếu tố chủng tộc: những phụ nữ gốc latinh; người mỹ gốc phi, người mỹ bản địa…có tỷ lệ tiểu đường thai kỳ cao hơn.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/nhung-ai-co-nguy-co-mac-tieu-duong-03.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Đang có sự thay đổi !</p>\r\n<p dir=\"ltr\">Mặc dù những nhóm người trên thuộc nhóm nguy cơ cao mắc các loại tiểu đường khác nhau. Tuy nhiên, hiện nay do lối sống thay đổi, do tình trạng dinh dưỡng mất cân bằng, do tình trạng thừa cân béo phì, tình trạng rối loạn chuyển hóa mỡ máu gia tăng nhanh chóng, stress trong công việc và cuộc sống…đã làm thay đổi đối tượng mắc tiểu đường: Tỷ lệ mắc tiểu đường tuýp 2 càng ngày càng trẻ hơn (tỷ lệ người dưới 40 tuổi mắc tiểu đường tuýp 2 càng ngày càng nhiều, kể cả trẻ em dưới 18 tuổi). Tỷ lệ phụ nữ mang thai có tiểu đường thai kỳ cũng gia tăng.</p>\r\n<p dir=\"ltr\">.</p>', 'Những ai có nguy cơ mắc tiểu đường?', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2020-06-12 11:57:41', '2020-06-12 11:57:41', '', 115, 'http://yentheme.test:8080/uncategorized/115-revision-v1/', 0, 'revision', '', 0),
(260, 1, '2020-06-12 11:58:49', '2020-06-12 11:58:49', '<p dir=\"ltr\">Mọi người thường nghĩ rằng tiểu đường là căn bệnh mà khi ta già đi, béo lên sẽ mắc phải. Tuy nhiên vẫn có những người trẻ bị tiểu đường. Hơn nửa số người mắc tiểu đường thuộc loại tiểu đường tuýp 2. Tuy nhiên bệnh tiểu đường tuýp 1 thường gặp ở những người vừa trẻ, vừa gầy là căn bệnh như thế nào? Qua bài viết này chúng ta hãy cùng tìm hiểu về loại tiểu đường này nhé.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>1. Tiểu đường tuýp 1 là căn bệnh như thế nào?</strong></h2>\r\n<p dir=\"ltr\">* Những điểm cần chú ý:</p>\r\n<p dir=\"ltr\">+ Hầu hết mọi người mắc tiểu đường là thuộc tuýp 2, nhưng vẫn có người mắc tuýp 1.</p>\r\n<p dir=\"ltr\">+ Tiểu đường tuýp 1 là bệnh mà các tế bào tụy bị phá hủy nên không thể tiết ra insulin.</p>\r\n<p dir=\"ltr\">+ 80% người mắc tiểu đường tuýp 1 có nguyên nhân do hệ miễn dịch bất thường dẫn tới tự sinh ra các kháng thể.</p>\r\n<p dir=\"ltr\">+ Tiểu đường tuýp 1 hầu hết khởi phát ở người trẻ tuổi nhưng không có nghĩa chỉ có mỗi người trẻ bị mắc bệnh.</p>\r\n<p dir=\"ltr\">Mọi người có nghĩ rằng “Tiểu đường là căn bệnh khởi phát ở người bị béo phì lúc trung tuổi” không? Thực ra suy nghĩ này cũng không hẳn là sai. Phần lớn những người mắc tiểu đường (trên 95%) có nguyên nhân là do béo phì bởi việc ăn uống quá độ, ít vận động, dần lớn tuổi, Những người mắc tiểu đường do nguyên nhân này sẽ được phân loại vào tiểu đường tuýp 2.</p>\r\n<p dir=\"ltr\">Chắc hẳn sẽ có người từng nghĩ có tuýp 2 thì hẳn phải có tuýp 1. Và đúng như vậy, so với bệnh tiểu đường tuýp 2, tiểu đường tuýp 1 là căn bệnh hầu hết được phát hiện ở những người khá trẻ.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-1-la-gi-01-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">* Trước hết thì mọi người cần phân biệt tiểu đường tuýp 1 và tuýp 2.</p>\r\n<p dir=\"ltr\">Tiểu đường là căn bệnh mà lượng đường huyết quá cao gây ra những thay đổi bất thường trong cơ thể.</p>\r\n<p dir=\"ltr\">Đường hay nói cách khác là glucose là một chất cần thiết để duy trì hoạt động của cơ thể người. Bởi vì não bộ chỉ có thể sử dụng glucose như một chất dinh dưỡng giúp điều khiển các chức năng của con người. Do đó cơ thể sẽ phải tự điều chỉnh bằng nhiều cách khác nhau sao cho glucose trong máu, hay lượng đường huyết luôn ở mức cân bằng, không quá cao hoặc không quá thấp.</p>\r\n<p dir=\"ltr\">Lượng glucose mà cơ thể cần được cung cấp hầu hết qua việc ăn uống. Bằng hoạt động tiêu hóa thức ăn mà lượng glucose này sẽ được hấp thụ và chuyển thành dinh dưỡng cho não bộ. Hầu hết người mắc tiểu đường tuýp 2 có chế độ ăn uống quá mức nên lượng dinh dưỡng hấp thụ vào cơ thể cũng quá nhiều, dẫn tới lượng đường huyết luôn duy trì ở mức quá cao và lâu dần thành bệnh.</p>\r\n<p dir=\"ltr\">Tuy vậy, cơ thể người sẽ có cơ chế tự cân bằng đường huyết, tức là đường huyết cao cơ thể sẽ điều chỉnh cho nó giảm bớt xuống để cơ thể trở lại bình thường, cho nên để nói dễ dàng mắc tiểu đường thì không có, vì bệnh tiến triển rất từ từ.</p>\r\n<p dir=\"ltr\">Khi cơ thể có mức đường huyết cao, sẽ xuất hiện một loại hormone có vai trò giảm lượng đường huyết xuống có tên là insulin được tiết ra từ tụy. Ở tiểu đường tuýp 2 thì cơ thể sẽ giảm chức năng của insulin khiến cho đường huyết luôn ở mức cao. Còn có một cách gọi khác của tiểu đường tuýp 2 là “mất khả năng dung nạp glucose”.</p>\r\n<p dir=\"ltr\">Mặt khác sẽ có trường hợp không phải là “giảm hiệu quả insulin” mà là “không thể tiết insulin”. Do insulin không thể tiết ra dẫn tới tình trạng đường huyết cao và bị tiểu đường thì gọi chung là “tiểu đường tuýp 1”. Tiểu đường tuýp 1 có thể do tế bào tụy bị hỏng nên không thể tiết ra insulin. Đây chính là điểm để phân biệt tuýp 1 và tuýp 2.</p>\r\n<p dir=\"ltr\">Mọi người cần phân biệt rõ tiểu đường tuýp 1 và tiểu đường tuýp 2 vì cơ chế khác nhau nên cách điều trị như ăn uống, tập luyện và sử dụng thuốc, những thói quen hàng ngày,v.v… cũng khác nhau.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>2. Các triệu chứng, đặc trưng phát bệnh của tiểu đường tuýp 1. Những xét nghiệm cần thiết cho việc chẩn đoán.</strong></h2>\r\n<p dir=\"ltr\">* Những điều cần chú ý:</p>\r\n<p dir=\"ltr\">+ Tiểu đường tuýp 1 được chia ra làm 3 loại bệnh.</p>\r\n<p dir=\"ltr\">+ Chẩn đoán chính xác tuýp 1 hoặc tuýp 2, từ đó có những lộ trình trị liệu cụ thể, rõ ràng cho từng loại là rất quan trọng.</p>\r\n<p dir=\"ltr\">Mọi người cần hiểu rõ tiểu đường tuýp 1 được chia ra làm 3 loại : loại phát bệnh cấp tính, loại phát bệnh từ từ, loại nguy cấp. Dựa vào thông tin từ trạng thái trước khi phát bệnh, thời gian của các triệu chứng mà có thể phân biệt với tiểu đường tuýp 2. Sau đây chúng ta sẽ cùng tìm hiểu kỹ từng loại.</p>\r\n<p dir=\"ltr\">– Tiểu đường tuýp 1 – loại phát bệnh cấp tính</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 1 – loại phát bệnh cấp tính là loại hay gặp nhất trong tiểu đường tuýp 1. Có thể chỉ vài tháng hoặc vài tuần là đã tiến triển thành bệnh.</p>\r\n<p dir=\"ltr\">Sau khi xuất hiện các triệu chứng như hay khát nước, đi tiểu nhiều lần (lượng nước tiểu mỗi lần nhiều), sụt cân, chỉ trong vài tháng chức năng tiết insulin sẽ hoàn toàn biến mất. Trường hợp đột nhiên sụt giảm khả năng tiết insulin, các triệu chứng đi kèm với rối loạn thần kinh ngoại biên (ketoacidosis tiểu đường) cũng có thể xảy ra. Tiểu đường tuýp 1 – loại cấp tính có đặc trưng là do tính tự miễn dịch và dương tính với “các tự kháng thể liên quan tới đảo tụy”. Các kháng thể này tồn tại dưới dạng protein trong cơ thể sẽ tự tấn công các tế bào tụy, khiến tụy bị phá hủy.</p>\r\n<p dir=\"ltr\">– Tiểu đường tuýp 1 – loại phát bệnh từ từ</p>\r\n<p dir=\"ltr\">Tiểu đường tuýp 1 – loại phát bệnh từ từ là bệnh phần lớn xuất hiện ở người từ 30-50 tuổi, và bệnh tiến triển một cách chậm rãi. Do đó cũng có trường hợp bị nhầm lẫn với tiểu đường tuýp 2. Loại này cũng có tính tự miễn dịch, các tế bào tụy cũng bị hệ miễn dịch tấn công, phân biệt rõ với tiểu đường tuýp 2. Việc phát hiện sớm và phân biệt rõ ràng là cực kỳ cần thiết, vì tiểu đường tuýp 1 – loại phát bệnh từ từ và tiểu đường tuýp 2 sẽ có những cách điều trị khác nhau vào những giai đoạn khác nhau.</p>\r\n<p dir=\"ltr\">– Tiểu đường tuýp 1 – loại nguy cấp</p>\r\n<p dir=\"ltr\">Đây là loại khác hoàn toàn so với 2 loại trên, không hề liên quan tới tính tự miễn dịch, và hiện cũng chưa rõ nguyên nhân. Như tên gọi – loại nguy cấp, loại này cũng là loại có thời gian phát bệnh nhanh nhất. Chỉ sau vài ngày với các triệu chứng của cảm cúm như đau họng, sốt, đau bụng,v.v.. là bệnh đã khởi phát. Chỉ sau đó vài ngày, insulin sẽ biến mất và kèm theo rối loạn ý thức có thể xuất hiện.</p>\r\n<p dir=\"ltr\">Đối với những người được xác định là bị bệnh tiểu đường, việc tiếp theo là “chẩn đoán loại bệnh”, cụ thể là dựa vào nguyên nhân bệnh để khám và chẩn đoán phân biệt bệnh tiểu đường tuýp 1 hay tuýp 2.</p>\r\n<p dir=\"ltr\">Mặc dù có nhiều phương pháp để kiểm tra đó có phải tiểu đường tuýp 1 không, nhưng yếu tố quyết định cuối cùng để chẩn đoán tuýp 1 là xem trong cơ thể có tồn tại protein được gọi là “tự kháng thể” sẽ nhầm lẫn tự tấn công và phá hủy tế bào. Các tự kháng thể phổ biến là “kháng thể GAD”, “kháng thể tế bào đảo tụy ”, “tự kháng thể insulin ”, “kháng thể IA-2”,…. Một khi các kháng thể này được tìm thấy, khả năng mắc bệnh tiểu đường tuýp 1 rất cao.</p>\r\n<p dir=\"ltr\">Mặt khác, có 20% bệnh tiểu đường tuýp 1 không phải do mất khả năng kiểm soát miễn dịch và tự kháng thể không được tạo ra. Nói cách khác, trong số 5 người bị bệnh tiểu đường tuýp 1 thì có 1 người mắc bệnh do tế bào tuyến tụy bị phá hủy không rõ nguyên nhân và insulin không được tiết ra. Theo tiêu chuẩn chẩn đoán bệnh tiểu đường tuýp 1 khởi phát cấp tính (2012), trường hợp dù không tìm thấy tự kháng thể nhưng insulin không được tiết ra từ tuyến tụy hoặc có tiết ra nhưng lượng không đủ thì có khả năng cao là bệnh tiểu đường tuýp 1.</p>\r\n<p dir=\"ltr\">Insulin tiết ra từ tuyến tụy được chuyển hóa ngay sau khi được tiết ra, do đó không thể lấy mẫu máu và đo được. Khi insulin được tiết ra từ tuyến tụy, lượng insulin tiết ra có thể được ước tính bằng cách đo thành phần “C-peptide” đồng thời tiết ra với lượng giống nhau. Chất này thường được sử dụng vì nó có thể đo được trong nước tiểu. Đo lượng C-peptide trong nước tiểu và máu, nếu kết quả cho giá trị cực kỳ thấp, bệnh nhân có thể được chẩn đoán là bệnh tiểu đường tuýp 1.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/tieu-duong-tuyp-1-la-gi-02-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Để kiểm tra xem insulin có được tiết ra từ tuyến tụy hay không, tiến hành xét nghiệm gọi là “xét nghiệm dung nạp glucose đường uống (OGTT)”. Các bác sĩ sử dụng kết quả của xét nghiệm này như là một căn cứ để chẩn đoán đó có phải là bệnh tiểu đường loại 1 không.</p>\r\n<p dir=\"ltr\">Khi kiểm tra để xác nhận bệnh tiểu đường tuýp 1, tuổi tác cũng là yếu tố cần tham khảo. Nhiều người bị bệnh tiểu đường tuýp 1 được cho biết bệnh đã khởi phát từ nhỏ đến khi trưởng thành. Cầu thủ Sugiyama Arata của giải J League đã bị bệnh tiểu đường tuýp 1 khi 23 tuổi và là cầu thủ được biết đến vẫn hoạt động như một cầu thủ bóng đá chuyên nghiệp dù bị bệnh tiểu đường. Người ta nói rằng nhiều bệnh tiểu đường tuýp 1 thường khởi phát ở những người trẻ tuổi. Ngược lại, bệnh tiểu đường tuýp 2 thường khởi phát ở những người béo phì và trung tuổi. Tuy nhiên, cũng có những người bị bệnh tiểu đường tuýp 1 khi trung tuổi, do đó không thể phân loại bệnh tiểu đường theo độ tuổi.</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường có lộ trình điều trị khác nhau tùy thuộc vào đó là bệnh tiểu đường tuýp 1 hay tuýp 2. Ngoài ra, lối sống của bệnh nhân cũng sẽ thay đổi theo cách điều trị khác nhau. Khám bệnh tiểu đường và chẩn đoán xem đó là tuýp 1 hay tuýp 2 là rất quan trọng trong việc tiến hành điều trị và sống cùng với bệnh.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>3. Điều trị tiểu đường tuýp 1: tại sao insulin lại được ưu tiên?</strong></h2>\r\n<p dir=\"ltr\">* Những điểm cần chú ý:</p>\r\n<p dir=\"ltr\">+ Vì tiểu đường tuýp 1 thiếu insulin nên điều trị bằng tiêm insulin là bắt buộc.</p>\r\n<p dir=\"ltr\">+ Các phương pháp mới như tiêm insulin dưới da liên tục, vòng theo dõi đường huyết giúp kiểm soát đường huyết cũng đã khả dụng.</p>\r\n<p dir=\"ltr\">+ Nhờ các phương pháp điều trị tiên tiến hiện nay, người mắc tiểu đường tuýp 1 cũng có thể sống cuộc sống như người bình thường.</p>\r\n<p dir=\"ltr\">– Giải pháp duy nhất chữa tiểu đường tuýp 1 là tiêm bổ sung insulin</p>\r\n<p dir=\"ltr\">Mọi người thường nghĩ rằng nếu điều trị tiểu đường thì cố gắng chọn những phương pháp đơn giản và hiệu quả nhất. Chính vì vậy cũng đã có người tập trung vào thuốc uống điều trị tiểu đường tuýp 1. Tuy nhiên tiểu đường tuýp 1 là loại bệnh mà chắc chắn cơ thể không đủ insulin để dùng, cho nên mọi người không thể lựa chọn thuốc uống được. Căn bản của trị liệu tiểu đường loại 1 là tự tiêm insulin.</p>\r\n<p dir=\"ltr\">Hiện tại trong điều trị tiểu đường tuýp 2, các loại thuốc uống có tác dụng tăng khả năng tiết ra insulin của tụy, phân giải hoặc trì hoãn tác dụng của đường (glucose). Theo đó, tăng hiệu quả của insulin trong cơ thể và làm giảm các triệu chứng của bệnh tiểu đường. Nói 1 cách đơn giản hơn là giúp cho insulin được cơ thể tạo ra hoạt động một cách hiệu quả hơn.</p>\r\n<p dir=\"ltr\">Thế nhưng tiểu đường tuýp 1 là căn bệnh mà tế bào tụy tiết ra insulin bị phá hủy. Cơ thể không có insulin (không tạo ra insulin được) nên giải pháp duy nhất là phải tiêm bổ sung insulin.</p>\r\n<p dir=\"ltr\">Khi ăn, cơ thể sẽ tiết ra một lượng insulin thích hợp để giúp lượng đường huyết duy trì ở mức ổn định, làm cơ thể khỏe mạnh. Thuốc insulin trong điều trị tiểu đường lấy điều này làm căn bản, tức là đưa cơ thể về trạng thái tiết insulin giống như người khỏe mạnh bình thường.</p>\r\n<p dir=\"ltr\">– Tiêm insulin 1 ngày từ 1 đến 2 lần</p>\r\n<p dir=\"ltr\">Để cơ thể có thể tiết insulin ra từng chút một giống như bình thường, người ta sẽ sử dụng một loại thuốc insulin có hiệu quả tương đối dài (loại tác dụng trung gian hoặc loại tác dụng kéo dài) được tiêm 1 ngày từ 1 đến 2 lần. Mặt khác, khi ăn uống, để khôi phục khả năng tiết insulin ngay lập tức (tiết bổ sung) người ta sẽ tiêm một loại insulin tác dụng nhanh. Nó còn được gọi là liệu pháp insulin tích cực.</p>\r\n<p dir=\"ltr\">Một vấn đề gặp phải khi tiêm insulin là phần da chỗ tiêm sẽ trở nên cứng nếu tiêm lặp lại nhiều lần ở cùng một chỗ. Ngoài ra việc theo dõi một ngày tiêm bao nhiêu lần cũng rất khó. Để khắc phục những vấn đề này, có một phương pháp “truyền insulin dưới da liên tục (CSII)” mà không cần tiêm nhiều lần. Cách này cũng được gọi là “bơm insulin”, là một máy bơm gắn vào cơ thể và duy trì tiêm insulin dưới da tương đương với lượng tiết cơ bản từ một kim tiêm ở bụng. Hơn nữa, có thể tiêm insulin tương ứng với lượng tiết bổ sung bằng cách thay đổi tốc độ của máy bơm trước mỗi bữa ăn. Tần suất thay thế máy bơm chỉ vài lần một vài ngày, giúp bệnh nhân tránh được việc phải tiêm nhiều lần trong 1 ngày.</p>\r\n<p dir=\"ltr\">Cách bơm insulin này gần với nhịp độ tiết insulin tự nhiên của cơ thể, nên có điểm lợi là bệnh nhân có thể ăn uống và vận động thoải mái. Ngoài ra, người ta cho rằng việc sử dụng bơm insulin giúp làm giảm tỷ lệ bị các biến chứng tiểu đường như bệnh võng mạc tiểu đường, bệnh thần kinh tiểu đường, bệnh thận do tiểu đường.</p>\r\n<p dir=\"ltr\">Khi tự tiêm insulin, bệnh nhân cần tự đo lượng đường trong máu của mình. Việc đo này cũng rất khó khăn đối với bệnh nhân. Dùng cây kim nhỏ chích ở đầu ngón tay để máu chảy ra và đo lượng đường trong máu. Đây là cách đo đem lại đau đớn cho bệnh nhân khi phải làm nhiều lần trong ngày. Do đó, có thể đo đường huyết bằng cách đo lượng đường của dịch kẽ dưới da mà không cần lấy máu. Đeo thiết bị cảm biến trên cánh tay và lượng đường trong máu có thể được đo liên tục trong 14 ngày. Có thể đo lượng đường trong máu liên tục mà không cần đâm kim nhiều lần.</p>\r\n<p dir=\"ltr\">Ngoài ra còn có một phương pháp điều trị được gọi là liệu pháp SAP kết hợp truyền insulin dưới da bền liên tục với một màn hình quan sát đường huyết liên tục để kiểm soát đường huyết thích hợp hơn.</p>\r\n<p dir=\"ltr\">– Phương pháp ghép tụy</p>\r\n<p dir=\"ltr\">Bên cạnh đó còn có phương pháp ghép tụy cho bệnh nhân tiểu đường tuýp 1 mà không thể kiểm soát được đường huyết. Nhiều bệnh nhân còn bị rối loạn chức năng thận, vì vậy tụy và thận thường được cấy ghép cùng một lúc. Sau khi cấy ghép, bệnh nhân phải dùng thuốc ức chế miễn dịch, nhưng có báo cáo rằng tuổi thọ của bệnh nhân được cấy ghép dài hơn bệnh nhân không cấy ghép.</p>\r\n<p dir=\"ltr\">Ngoài cấy ghép tuyến tụy, còn có một phương pháp được gọi là cấy ghép đảo tụy. Trong tuyến tụy, các mô tạo insulin được gọi là đảo tụy và phần này chỉ chiếm 1% tuyến tụy. Phẫu thuật cấy ghép toàn bộ tuyến tụy đòi hỏi gây mê toàn thân và gánh nặng lên cơ thể tăng lên. Phương pháp chỉ loại bỏ các đảo tụy có vai trò tạo insulin từ trong tuyến tụy và cấy ghép từ mạch máu của gan được gọi là cấy ghép đảo tụy. So với ghép tụy, phương pháp này giúp giảm gánh nặng cho bệnh nhân.</p>\r\n<p dir=\"ltr\">Ghép tụy và ghép đảo tụy chỉ có thể được thực hiện tại một số các cơ sở y tế, và số lượng bệnh nhân được phẫu thuật cũng bị giới hạn.</p>\r\n<p dir=\"ltr\">Các nghiên cứu khác về tuyến tụy nhân tạo đang được tiến hành. Ví dụ: ngày 27 tháng 5 năm 2016 đã công bố chính sách phê duyệt trong trường hợp bệnh nhân thỏa mãn điều kiện ghép tế bào tuyến tụy của lợn với người mà Bộ Y tế, Lao động và Phúc lợi Nhật Bản đã cấm từ trước đến nay. Do đó, số lượng người có thể điều trị ghép cho đến nay có thể được cải thiện trong tương lai gần.</p>\r\n<p dir=\"ltr\">Việc phát triển thành công “cấy ghép tế bào đảo tụy lợn vi nang được bọc trong một bao nhỏ gọi là “vi nang” cũng đã được công bố. Bằng cách đó, người ta cho rằng sự viêm nhiễm và phản ứng loại thải miễn dịch gây ra bởi sự cấy ghép tế bào động vật lên con người có thể giảm đáng kể.</p>\r\n<p dir=\"ltr\">Ngoài ra, vì phần lớn bệnh tiểu đường tuýp 1 là do bất thường của hệ thống miễn dịch nên đã có “liệu pháp miễn dịch” nhằm điều trị những bất thường này của hệ thống miễn dịch. Mặc dù các loại thuốc có tác dụng trên một số hệ thống miễn dịch đã được thử nghiệm nhưng không đạt đến hiệu quả trong việc chữa trị bệnh tiểu đường tuýp 1.</p>\r\n<p dir=\"ltr\">Hiện tại mặc dù chưa công bố nhưng nghiên cứu về tái sinh chính tuyến tụy cũng được khuyến khích. Có những nghiên cứu về tạo và cấy chính tuyến tụy trong cơ thể heo bằng cách sử dụng tế bào ES và tế bào iPS và nghiên cứu để tái tạo tuyến tụy trong cơ thể bằng cách kích thích mạng nơ-ron.</p>\r\n\r\n<h2 dir=\"ltr\">4. Tổng kết</h2>\r\n<p dir=\"ltr\">Trong tổng số người bị tiểu đường chỉ có dưới 5% là thuộc tiểu đường tuýp 1 hiếm gặp, các triệu chứng, nguyên nhân, chẩn đoán và điều trị đã được nêu trong bài. Tiểu đường tuýp 1 có thể xảy ra nhanh chóng ở những người trẻ, nhưng cũng có thể phát triển rất chậm ở người già. Dù là đối tượng người bệnh nào thì việc thiếu hụt insulin là không thay đổi, tiểu đường tuýp 1 không phải bệnh có thể trị liệu bằng ăn uống, tập luyện được mà cần điều trị bằng insulin. Khi bệnh càng phát triển thì việc tiêm insulin và theo dõi đường huyết càng cần diễn ra liên tục hơn, điều này có thể gây khó khăn cho người bệnh. Tuy nhiên hiện nay, y học càng ngày càng hiện đại với các dụng cụ tiêm insulin và đo đường huyết liên tục đã giúp cho người bệnh có thể tự kiểm soát, chữa trị bệnh dễ dàng hơn.</p>', 'Tiểu đường tuýp 1 là gì?', '', 'inherit', 'closed', 'closed', '', '110-revision-v1', '', '', '2020-06-12 11:58:49', '2020-06-12 11:58:49', '', 110, 'http://yentheme.test:8080/uncategorized/110-revision-v1/', 0, 'revision', '', 0),
(261, 1, '2020-06-12 11:59:10', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-12 11:59:10', '0000-00-00 00:00:00', '', 0, 'http://yentheme.test:8080/?p=261', 0, 'post', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(262, 1, '2020-06-12 11:59:49', '2020-06-12 11:59:49', '<p dir=\"ltr\">Tiểu đường tuýp 2 là căn bệnh khá phổ biến tại Việt Nam. Bệnh này nếu không được chẩn đoán sớm để điều trị kịp thời sẽ để lại biến chứng nghiêm trọng. Bạn cần nắm rõ tiểu đường tuýp 2 là gì , nguyên nhân gây ra tiểu đường tuýp 2, triệu chứng bệnh để bảo vệ bản thân và người thân xung quanh.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>1. Tiểu đường tuýp 2 là gì?</strong></h2>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 (tiểu đường type 2) là loại bệnh tiểu đường trong đó lượng đường trong máu bệnh nhân luôn cao do thiếu tác dụng của insulin. Bệnh tiểu đường được chia thành tiểu đường tuýp 1, tuýp 2, tiểu đường thai kỳ và các loại khác (tính đến tháng 3 năm 2018). Trong số đó, bệnh tiểu đường tuýp 2 là loại bệnh phổ biến nhất và chiếm hơn 90% trong bệnh tiểu đường nói chung. Phần lớn bệnh tiểu đường tuýp 2 khởi phát do chế độ ăn uống không lành mạnh, thiếu vận động hoặc do tình trạng béo phì và còn do các yếu tố môi trường.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-01-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Bệnh tiểu đường là một căn bệnh âm thầm tiến triển mà bệnh nhân thường không biết và không có triệu chứng ở giai đoạn đầu. Tuy nhiên, nếu tình trạng bệnh tiểu đường tiếp tục tiến triển có thể dẫn đến xuất hiện 3 biến chứng lớn là bệnh võng mạc tiểu đường, bệnh thận tiểu đường, bệnh thần kinh ngoại biên (bệnh thần kinh tiểu đường). Bên cạnh đó, bệnh còn tăng nguy cơ dẫn đến các bệnh do tổn thương đại mạch như nhồi máu cơ tim và nhồi máu não.</p>\r\n<p dir=\"ltr\">Ngoài ra, một số bệnh khác như bệnh nha chu, suy giảm trí nhớ, ung thư, bệnh loãng xương cũng dễ xảy ra do bệnh tiểu đường. Bệnh nha chu được cho là có liên quan đến sự chuyển biến xấu đi của bệnh tiểu đường.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>2. Nguyên nhân gây ra tiểu đường tuýp 2 </strong></h2>\r\n<p dir=\"ltr\">Trong bệnh tiểu đường loại 2, có những trường hợp là do lượng tiết insulin bị giảm và cũng có trường hợp do chức năng insulin bị suy giảm (kháng insulin). Nguyên nhân của bệnh là do yếu tố di truyền và các yếu tố môi trường.</p>\r\n<p dir=\"ltr\">+ Yếu tố di truyền</p>\r\n<p dir=\"ltr\">Đó là gen di truyền liên quan đến việc tiết insulin hoặc chức năng tuyến tụy và người ta nói rằng khi những bất thường về gen di truyền này chồng lên nhau, bệnh tiểu đường tuýp 2 có nhiều khả năng khởi phát hơn.</p>\r\n<p dir=\"ltr\">+ Yếu tố môi trường</p>\r\n<p dir=\"ltr\">Yếu tố môi trường là sự gia tăng lượng hấp thụ chất béo do béo phì, thiếu vận động và thói quen ăn uống theo phong cách Tây u. Ngoài ra còn liên quan đến chế độ ăn uống không cân bằng dinh dưỡng và thói quen ăn uống không lành mạnh.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-02-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">Trong những năm gần đây, người ta đã chỉ ra rằng việc thiếu lượng hấp thụ chất xơ và magie do sự thay đổi chế độ ăn uống sau chiến tranh (như giảm mạnh lượng hấp thụ lúa mạch và ngũ cốc) có ảnh hưởng lớn đến sự khởi phát của bệnh tiểu đường tuýp 2.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>3. Triệu chứng của bệnh tiểu đường tuýp 2</strong></h2>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 hầu như không có triệu chứng rõ ràng và bệnh nhân cũng không nhận biết được bệnh. Khi bệnh chuyển biến xấu đi và bệnh nhân rơi vào tình trạng tăng đường huyết ketosis và nhiễm toan ceton, sẽ xuất hiện các triệu chứng như khát nước, uống nhiều nước, đa niệu, cảm giác mệt mỏi và sụt giảm cân nặng…</p>\r\n<p dir=\"ltr\">+ Đa niệu</p>\r\n<p dir=\"ltr\">Khi lượng đường huyết cao, cơ thể sẽ đào thải bớt đường qua nước tiểu. Khi đó thận sẽ kéo nước từ trong cơ thể để pha loãng nước tiểu, khiến khối lượng nước tiểu tăng lên. Đây là lý do làm cho người bệnh thường xuyên đi tiểu.</p>\r\n<p dir=\"ltr\">+ Khát nước</p>\r\n<p dir=\"ltr\">Việc tiểu nhiều sẽ làm cơ thể tăng nhu cầu sử dụng nước để bù lại lượng nước đã mất. Điều này sẽ kích thích làm người bệnh luôn cảm thấy khát nước và uống nhiều nước hơn.</p>\r\n<p dir=\"ltr\">+ Cảm thấy kiệt sức</p>\r\n<p dir=\"ltr\">Cơ thể người bệnh thường xuyên cảm thấy mệt mỏi</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-03-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Da khô và ngứa</p>\r\n<p dir=\"ltr\">+ Cảm giác của chân tay bị giảm, thỉnh thoảng thấy hơi nhói đau</p>\r\n<p dir=\"ltr\">+ Hay bị nhiễm trùng</p>\r\n<p dir=\"ltr\">+ Đi tiểu nhiều lần</p>\r\n<p dir=\"ltr\">+ Mờ mắt</p>\r\n<p dir=\"ltr\">+ Có vấn đề về chức năng tình dục</p>\r\n<p dir=\"ltr\">+ Khó lành sẹo hay các vết bầm</p>\r\n<p dir=\"ltr\">+ Hay có các cảm giác rất đói hoặc rất khát</p>\r\n<p dir=\"ltr\">Khi bất cứ ai trong gia đình có triệu chứng trên, có thể họ đã mắc bệnh tiểu đường hay tiền tiểu đường. Chính vì vậy bạn hãy đi khám ngay để kiểm soát bệnh tốt hơn nhé. Ngoài ra mọi người cũng nên chú ý tới chế độ ăn uống thích hợp, chăm chỉ tập luyện thể dục thể thao, và cũng nên chú ý tới nguy cơ béo phì nữa nhé.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>4. Biến chứng</strong></h2>\r\n<p dir=\"ltr\">Nếu bệnh tiểu đường tuýp 2 tiếp tục tiến triển sẽ tăng nguy cơ xuất hiện các biến chứng khác nhau. 3 biến chứng lớn là bệnh võng mạc tiểu đường, bệnh thận tiểu đường, tổn thương thần kinh (bệnh thần kinh tiểu đường), ngoài ra còn gây ra các bệnh lây nhiễm khác nhau do khả năng miễn dịch của cơ thể giảm và các bệnh như nhồi máu cơ tim, nhồi máu não, xơ vữa động mạch tắc nghẽn do xơ vữa động mạch.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>5. Xét nghiệm/Chẩn đoán</strong></h2>\r\n<p dir=\"ltr\">Kết quả xét nghiệm máu được sử dụng để chẩn đoán bệnh tiểu đường tuýp 2.</p>\r\n<p dir=\"ltr\">+ Lượng đường trong máu</p>\r\n<p dir=\"ltr\">Đầu tiên sẽ kiểm tra lượng đường trong máu của bệnh nhân. Lượng đường trong máu có thể được chia thành đường huyết lúc đói và đường huyết ngẫu nhiên tùy thuộc vào thời gian của bữa ăn. Đường huyết lúc đói ≥126 mg/dl hoặc đường huyết ngẫu nhiên ≥200 mg/dl là một trong những điều kiện để chẩn đoán bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">+ HbA1c</p>\r\n<p dir=\"ltr\">Giá trị của HbA1c (hemoglobin A1c (NGSP)) cũng quan trọng trong chẩn đoán bệnh tiểu đường và nếu giá trị này ≥6,5% (giá trị NGSP) thì bệnh nhân được chẩn đoán mắc bệnh tiểu đường. Giá trị HbA1c là giá trị phản ánh chỉ số đường huyết trung bình trong 1-2 tháng trước. Điều này là do glucose trong máu liên kết với moglobin và tuổi thọ trung bình của hồng cầu là 120 ngày (khoảng 4 tháng).</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-05-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Xét nghiệm dung nạp glucose 75g đường uống (75g OGTT)</p>\r\n<p dir=\"ltr\">Ngoài ra, xét nghiệm dung nạp glucose 75g đường uống (75g OGTT) có thể được thực hiện để kiểm tra sự thay đổi lượng đường trong máu. Trong xét nghiệm này, sau khi cho bệnh nhân hấp thụ 75g glucose, tiến hành lấy máu theo thời gian và đo lượng đường trong máu.</p>\r\n<p dir=\"ltr\">Đối với những người khỏe mạnh, lượng đường trong máu đạt đến giá trị tối đa khoảng 30 phút sau khi uống, và trở về mức dưới giá trị tiêu chuẩn hoặc sau khoảng 2 giờ. Tuy nhiên, trong trường hợp của bệnh tiểu đường (loại bệnh tiểu đường), lượng đường trong máu không giảm hoàn toàn và vẫn còn cao. Giá trị đường huyết sau 2 giờ từ ≥200 mg/dl là một trong những điều kiện chẩn đoán bệnh tiểu đường.</p>\r\n<p dir=\"ltr\">Bệnh tiểu đường tuýp 2 được chẩn đoán toàn diện bằng cách kết hợp các kết quả của các xét nghiệm này.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>6. Điều trị bệnh tiểu đường tuýp 2</strong></h2>\r\n<p dir=\"ltr\">Cơ sở trong điều trị bệnh tiểu đường tuýp 2 là liệu pháp ăn uống và liệu pháp vận động. Việc điều trị như cải thiện chế độ ăn uống, vận động vừa phải và kết hợp dùng thuốc là rất quan trọng.</p>\r\n<p dir=\"ltr\">+ Liệu pháp ăn uống</p>\r\n<p dir=\"ltr\">Bệnh nhân cần biết lượng năng lượng thích hợp để hấp thụ dựa trên lượng hoạt động thể chất và cân nặng, từ đó cố gắng tạo một chế độ ăn uống cân bằng theo hướng dẫn. Đối với ngũ cốc nên ăn ngũ cốc nguyên hạt.</p>\r\n<p dir=\"ltr\"><img src=\"https://songkhoecungtieuduong.vn/public/files/upload/default/images/bai-viet/Tieu-duong-tuyp-2-la-gi-06-copy.jpg\" alt=\"\" /></p>\r\n<p dir=\"ltr\">+ Liệu pháp vận động</p>\r\n<p dir=\"ltr\">Bệnh nhân cần thường xuyên vận động vừa phải theo hướng dẫn và đi bộ đầy đủ mỗi ngày. Điều quan trọng là cần vận động cơ thể thường xuyên.</p>\r\n<p dir=\"ltr\">+ Điều trị bằng thuốc</p>\r\n<p dir=\"ltr\">Trường hợp bệnh nhân không thể kiểm soát lượng đường trong máu bằng liệu pháp ăn uống và liệu pháp vận động, tiến hành điều trị bằng thuốc. Đầu tiên, sẽ sử dụng một hoặc nhiều loại thuốc uống điều trị tiểu đường khác với insulin tùy theo tình trạng bệnh nhân. Trường hợp sử dụng thuốc uống điều trị tiểu đường nhưng không có hiệu quả, bệnh nhân có thể kết hợp điều trị bằng insulin hoặc chuyển sang chỉ điều trị bằng insulin.</p>\r\n<p dir=\"ltr\">Dù là bệnh tiểu đường loại nào thì quan trọng nhất trong điều trị là ở chính người bệnh chứ không ai khác. Hãy tích lũy, thu thập thông tin để có thật nhiều hiểu biết về bệnh tiểu đường tuýp 2, và càng có nhiều kiến thức về tiểu đường thì bạn càng dễ dàng quản lý bệnh hơn.</p>', 'Tiểu đường tuýp 2 là gì?', '', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2020-06-12 11:59:49', '2020-06-12 11:59:49', '', 113, 'http://yentheme.test:8080/uncategorized/113-revision-v1/', 0, 'revision', '', 0),
(263, 1, '2020-06-13 02:12:19', '2020-06-13 02:12:19', '', 'Nghiên cứu & kiến thức', '', 'publish', 'closed', 'closed', '', 'nghien-cuu-kien-thuc', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 0, 'http://yentheme.test:8080/?p=263', 16, 'nav_menu_item', '', 0),
(264, 1, '2020-06-13 02:32:15', '2020-06-13 02:32:15', ' ', '', '', 'publish', 'closed', 'closed', '', '264', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 34, 'http://yentheme.test:8080/?p=264', 12, 'nav_menu_item', '', 0),
(265, 1, '2020-06-13 02:32:15', '2020-06-13 02:32:15', ' ', '', '', 'publish', 'closed', 'closed', '', '265', '', '', '2020-06-13 02:32:15', '2020-06-13 02:32:15', '', 34, 'http://yentheme.test:8080/?p=265', 13, 'nav_menu_item', '', 0),
(266, 1, '2020-06-13 03:16:19', '2020-06-13 03:16:19', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-cai-dat-chung\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Hiển thị các category trên trang chủ', 'hien-thi-cac-category-tren-trang-chu', 'trash', 'closed', 'closed', '', 'group_5ee44442d2624__trashed', '', '', '2020-06-15 02:47:07', '2020-06-15 02:47:07', '', 0, 'http://yentheme.test:8080/?post_type=acf-field-group&#038;p=266', 0, 'acf-field-group', '', 0),
(267, 1, '2020-06-13 03:16:19', '2020-06-13 03:16:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID category cha', 'id_category_cha', 'trash', 'closed', 'closed', '', 'field_5ee4446cc89c0__trashed', '', '', '2020-06-15 02:47:07', '2020-06-15 02:47:07', '', 266, 'http://yentheme.test:8080/?post_type=acf-field&#038;p=267', 0, 'acf-field', '', 0),
(268, 1, '2020-06-13 03:16:19', '2020-06-13 03:16:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '', '', 'trash', 'closed', 'closed', '', 'field_5ee444fcc89c1__trashed', '', '', '2020-06-15 02:47:07', '2020-06-15 02:47:07', '', 266, 'http://yentheme.test:8080/?post_type=acf-field&#038;p=268', 1, 'acf-field', '', 0),
(269, 1, '2020-06-15 02:32:16', '2020-06-15 02:32:16', '', 'Tiểu đường tuýp 2 là gì?', '', 'publish', 'closed', 'closed', '', 'tieu-duong-tuyp-2-la-gi-2', '', '', '2020-06-15 02:32:59', '2020-06-15 02:32:59', '', 0, 'http://yentheme.test:8080/?p=269', 0, 'post', '', 0),
(270, 1, '2020-06-15 02:32:16', '2020-06-15 02:32:16', '', 'Tiểu đường tuýp 2 là gì?', '', 'inherit', 'closed', 'closed', '', '269-revision-v1', '', '', '2020-06-15 02:32:16', '2020-06-15 02:32:16', '', 269, 'http://yentheme.test:8080/uncategorized/269-revision-v1/', 0, 'revision', '', 0),
(271, 1, '2020-06-15 02:34:11', '2020-06-15 02:34:11', '', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'publish', 'closed', 'closed', '', 'tong-quan-kien-thuc-ve-benh-tieu-duong', '', '', '2020-06-15 02:34:11', '2020-06-15 02:34:11', '', 0, 'http://yentheme.test:8080/?p=271', 0, 'post', '', 0),
(272, 1, '2020-06-15 02:34:11', '2020-06-15 02:34:11', '', 'Tổng quan kiến thức về bệnh tiểu đường', '', 'inherit', 'closed', 'closed', '', '271-revision-v1', '', '', '2020-06-15 02:34:11', '2020-06-15 02:34:11', '', 271, 'http://yentheme.test:8080/uncategorized/271-revision-v1/', 0, 'revision', '', 0),
(273, 1, '2020-06-15 02:53:27', '2020-06-15 02:53:27', '', 'Biến chứng của tiểu đường', '', 'publish', 'closed', 'closed', '', 'bien-chung-cua-tieu-duong-3', '', '', '2020-06-15 02:53:27', '2020-06-15 02:53:27', '', 0, 'http://yentheme.test:8080/?p=273', 0, 'post', '', 0),
(274, 1, '2020-06-15 02:53:27', '2020-06-15 02:53:27', '', 'Biến chứng của tiểu đường', '', 'inherit', 'closed', 'closed', '', '273-revision-v1', '', '', '2020-06-15 02:53:27', '2020-06-15 02:53:27', '', 273, 'http://yentheme.test:8080/uncategorized/273-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_termmeta`
--

TRUNCATE TABLE `wp_termmeta`;
-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_terms`
--

TRUNCATE TABLE `wp_terms`;
--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(3, 'Kiến thức cơ bản', 'kien-thuc-co-ban', 0),
(7, 'Vận động', 'van-dong', 0),
(8, 'Điều trị', 'dieu-tri', 0),
(11, 'Kiểm soát cân nặng', 'kiem-soat-can-nang', 0),
(12, 'Ngân hàng câu hỏi', 'ngan-hang-cau-hoi', 0),
(15, 'Tin tức', 'tin-tuc', 0),
(20, 'Chiến lược \"Kiềng 3 chân\"', 'chien-luoc-kieng-3-chan', 0),
(23, 'Hội đồng chuyên gia', 'hoi-dong-chuyen-gia', 0),
(24, 'Biến chứng', 'bien-chung', 0),
(25, 'Kiến thức', 'kien-thuc', 0),
(26, 'Những ai có nguy cơ mắc tiểu đường', 'nhung-ai-co-nguy-co-mac-tieu-duong', 0),
(27, 'Tiểu đường', 'tieu-duong', 0),
(28, 'Tiểu đường là gì', 'tieu-duong-la-gi', 0),
(29, 'Tiền tiểu đường', 'tien-tieu-duong', 0),
(30, 'Tiểu đường tuýp 1', 'tieu-duong-tuyp-1', 0),
(31, 'Tiểu đường tuýp 2', 'tieu-duong-tuyp-2', 0),
(32, 'Dinh dưỡng', 'dinh-duong', 0),
(33, 'Trang chủ', 'trang-chu', 0),
(34, 'Góc chuyên gia', 'goc-chuyen-gia', 0),
(35, 'Menu', 'menu', 0),
(36, 'Nghiên cứu &amp; tin-tức', 'kien-thuc-2', 0),
(37, 'Chuyên gia dinh dưỡng', 'chuyen-gia-dinh-duong', 0),
(38, 'Chuyên gia vận động', 'chuyen-gia-van-dong', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_term_relationships`
--

TRUNCATE TABLE `wp_term_relationships`;
--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(54, 24, 0),
(56, 1, 0),
(59, 1, 0),
(65, 1, 0),
(68, 25, 0),
(71, 8, 0),
(84, 1, 0),
(87, 15, 0),
(90, 28, 0),
(110, 15, 0),
(113, 15, 0),
(115, 28, 0),
(119, 28, 0),
(121, 15, 0),
(171, 34, 0),
(201, 34, 0),
(205, 34, 0),
(208, 23, 0),
(213, 24, 0),
(226, 27, 0),
(228, 27, 0),
(230, 27, 0),
(232, 27, 0),
(234, 27, 0),
(236, 27, 0),
(240, 35, 0),
(241, 35, 0),
(242, 35, 0),
(243, 35, 0),
(244, 35, 0),
(245, 35, 0),
(246, 35, 0),
(247, 35, 0),
(248, 35, 0),
(249, 35, 0),
(250, 35, 0),
(253, 35, 0),
(254, 35, 0),
(263, 35, 0),
(264, 35, 0),
(265, 35, 0),
(269, 15, 0),
(271, 15, 0),
(273, 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_term_taxonomy`
--

TRUNCATE TABLE `wp_term_taxonomy`;
--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 4),
(3, 3, 'category', '', 0, 0),
(7, 7, 'category', '', 20, 0),
(8, 8, 'category', '', 20, 1),
(11, 11, 'category', '', 0, 0),
(12, 12, 'category', '', 0, 0),
(15, 15, 'category', '', 0, 6),
(20, 20, 'category', '', 0, 0),
(23, 23, 'category', '', 0, 1),
(24, 24, 'category', '', 0, 2),
(25, 25, 'category', '', 0, 1),
(26, 26, 'category', '', 3, 0),
(27, 27, 'category', '', 0, 6),
(28, 28, 'category', '', 3, 3),
(29, 29, 'category', '', 3, 0),
(30, 30, 'category', '', 3, 0),
(31, 31, 'category', '', 3, 0),
(32, 32, 'category', '', 20, 0),
(33, 33, 'category', '', 0, 0),
(34, 34, 'category', '', 0, 3),
(35, 35, 'nav_menu', '', 0, 16),
(36, 36, 'category', '', 0, 0),
(37, 37, 'category', '', 34, 0),
(38, 38, 'category', '', 34, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_usermeta`
--

TRUNCATE TABLE `wp_usermeta`;
--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'hongyen'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'blue'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:3:{s:64:\"99a320bf3a35de1186d018075838e168dccdd633265faf8e351b5aa50f6d3737\";a:4:{s:10:\"expiration\";i:1592010109;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36\";s:5:\"login\";i:1591837309;}s:64:\"610dad2c2f0e9bf73647508e73dc332623c0ca4604cc8d6ba8f783af7d99fe24\";a:4:{s:10:\"expiration\";i:1592098672;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36\";s:5:\"login\";i:1591925872;}s:64:\"ec3fcca6d1a434b63346c05c43dc00578f134cda9362fea44664901e7f429a08\";a:4:{s:10:\"expiration\";i:1593135693;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36\";s:5:\"login\";i:1591926093;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(20, 1, 'wp_user-settings', 'editor=tinymce&libraryContent=browse&posts_list_mode=list&mfold=o'),
(21, 1, 'wp_user-settings-time', '1591957435'),
(22, 1, 'nav_menu_recently_edited', '35'),
(23, 1, 'closedpostboxes_post', 'a:1:{i:0;s:16:\"tagsdiv-post_tag\";}'),
(24, 1, 'metaboxhidden_post', 'a:0:{}'),
(25, 1, 'closedpostboxes_yentheme_page_acf-options-cai-dat-chung', 'a:0:{}'),
(26, 1, 'metaboxhidden_yentheme_page_acf-options-cai-dat-chung', 'a:0:{}'),
(27, 1, 'edit_post_per_page', '20'),
(28, 1, 'edit_category_per_page', '20'),
(29, 1, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(30, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(31, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_users`
--

TRUNCATE TABLE `wp_users`;
--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'hongyen', '$P$BSOVcvVh4VmjF8c50LCWYLFknAoJXG.', 'hongyen', 'hongyenhd97@gmail.com', 'http://localhost:8080/yentheme', '2020-06-05 09:15:59', '1591926037:$P$BJJm8SY7b6mQPTBhD982AmcU05MUYg0', 0, 'hongyen');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpmailsmtp_tasks_meta`
--

CREATE TABLE `wp_wpmailsmtp_tasks_meta` (
  `id` bigint(20) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpmailsmtp_tasks_meta`
--

TRUNCATE TABLE `wp_wpmailsmtp_tasks_meta`;
--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wpmailsmtp_tasks_meta`
--
ALTER TABLE `wp_wpmailsmtp_tasks_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1410;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2031;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1203;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=275;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wpmailsmtp_tasks_meta`
--
ALTER TABLE `wp_wpmailsmtp_tasks_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
