<!-- <?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?> -->
	<div class="book-block bg-gray box_form">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="section-title ">Đăng kí nhận tư vấn miễn phí</h2>
						<div class="section-content">
							<div class="row">
							<div class="col-sm-8">
								<div class="row">
									<?php echo do_shortcode('[contact-form-7 id="130" title="Form Register"]'); ?>
								</div>
							</div>
							<div class="col-sm-4 hidden-xs">
								<div class="form_info">
									<h2 class="title">Thông tin <br>đăng ký</h2>
									<p class="text_info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse </p>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>