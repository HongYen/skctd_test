
<?php 
get_header('alternative');
?>
<div class="container">
	<div class="row">
		<?php
		$category = get_category( get_query_var( 'cat' ) );
		$category_name = $category->name;
		$category_id = $category->term_id;
		$parent = get_category($category->category_parent);
		$parent_name = is_array($parent) ? $parent->name : '';
			// echo '<pre>';
			// print_r($category);
			// echo '</pre>';
		$catquery = new WP_Query( 'cat='. $category_id .'&posts_per_page='. get_field('show_quantity_max', 'option') .'' ); 
		?>
		
		<div class="col-sm-12">
			<div>
				<?php
				dimox_breadcrumbs();
				?>
			</div>
		</div>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-12">

					<h3 class="box_title">
						<a href="<?php echo get_permalink($category_name); ?>" title='<?php echo $category->cat_name ?>' > <?php echo $category_name?></a>
					</h3>
					<div class="animate-box"><span class="animate-border"></span></div>
				</div>
				<?php
				$index = 0;
				while($catquery->have_posts()) : $catquery->the_post();
					?>
					<div class="col-sm-4">
						<div class="box_content">
							<div class="item">
								<div class="image">
									<img src="<?php the_post_thumbnail_url(); ?>">
									<a href="<?php echo get_permalink($post); ?>" title='<?php echo $post->post_title ?>' class="view"><i class="fas fa-eye"></i></a>
								</div>

								<div class="info">
									<h3 class="title">
										<a href="<?php echo get_permalink($post->ID); ?>" title='<?php echo $post->post_title ?>'>
											<?php echo $post->post_title; ?>
										</a>
									</h3>
									<!-- <p dir="ltr"><?php echo  $post->post_content; ?></p> -->
									<p class="category-post">
										<a href="<?php echo get_permalink($category_name); ?>" class="txt-uppercase"><?php echo $category_name; ?></a>
										<span>/</span>
										<span><?php echo date("d/m/Y", strtotime($post->post_date)); ?></span>

									</p>
								</div>


							</div>
						</div>

					</div>

				<?php endwhile;
				wp_reset_postdata();
				?>
				
			</div>
		</div>


		<div class="col-sm-3">
			<h3 class="box_title">
				<?php echo $category_name . ' mới nhất'?>
			</h3>
			<div class="animate-box"><span class="animate-border"></span></div>
			<?php
			$index = 0;
			while($catquery->have_posts()) : $catquery->the_post();
				?>

				<div class="box_content box_content-right">
					<div class="item">
						<div class="row">
							<div class="col-sm-4 custom-pr-5">
								<div class="image">
									<img src="<?php the_post_thumbnail_url(); ?>">
									<a href="<?php echo get_permalink($post->ID); ?>" title='<?php echo $post->post_title ?>' class="view"><i class="fas fa-eye"></i></a>
								</div>
							</div>
							<div class="col-sm-8 custom-pl-5">
								<div class="info">
									<h3 class="title">
										<a href="<?php echo get_permalink($post->ID); ?>" title='<?php echo $post->post_title ?>'>
											<?php echo $post->post_title; ?>
										</a>
									</h3>
									<!-- <p dir="ltr"><?php echo  $post->post_content; ?></p> -->
									<p class="category-post">
										<a href="<?php echo get_permalink($category_name); ?>" class=""><?php echo $category_name; ?></a>
										<span>/</span>
										<span><?php echo date("d/m/Y", strtotime($post->post_date)); ?></span>

									</p>
								</div>


							</div>
						</div>

					</div>
				</div>
			<?php endwhile;
			wp_reset_postdata();
			?>

		</div>
	</div>
</div>
<?php get_form_register();?>
<?php get_footer();?>