<!-- <?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?> -->
<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/nexa/font.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/Roboto/Roboto.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/custom_scss.css">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo get_template_directory_uri(); ?>/assets/lib/fontawesome/css/all.min.css" rel="stylesheet" />
	<?php wp_head(); ?> 

</head>

<body <?php body_class(); ?>> 
	<?php
	wp_body_open();
	?> 
	<section class="box_top">
		<div class="contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="traditional">
							<a href="tel:088.885.4313" class="hotline">
								<i class="fa fa-phone" aria-hidden="true"></i>
								088.885.4313
							</a>
							<a href="mail:088.885.4313" class="email hidden-xs">
								<i class="fa fa-paper-plane" aria-hidden="true"></i> tieuduong@hab.vn</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social">
							<a href="" target="_blank" class="item"><i class="fab fa-facebook-f"></i></a>
							<a href="" target="_blank" class="item"><i class="fab fa-youtube"></i></a>
							<a href="" target="_blank" class="item"><i class="fab fa-instagram"></i></a>
							<a href="" target="_blank" class="item"><i class="fab fa-google-plus-g"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="logo">
							<a href="/" title="Sống khỏe cùng tiểu đường">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/LogoHAB-congdong.png" alt="Sống khỏe cùng tiểu đường"></a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="slogan">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Song-khoe-cung-tieu-duong-2.png" alt="Sống khỏe cùng tiểu đường">
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="search">
							<div class="box_search">
								<form role="search"  method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
									<div class="form-group">
										<div class="input-group">
											<input type="search" id="<?php echo esc_attr( $twentytwenty_unique_id ); ?>" class="search-field" placeholder="Tìm kiếm" value="<?php echo get_search_query(); ?>" name="s"/>
											<div class="input-group-addon">
												<button type="submit" class="search-submit" >
													<span class="" aria-hidden="true">
														<i class="fas fa-search"></i>
													</span>
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>
	<header id="site-header" class="header-footer-group" role="banner">
	<div class="header-navigation-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php
					if ( has_nav_menu( 'primary' ) || ! has_nav_menu( 'expanded' ) ) {
						?>

						<nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'Horizontal', 'twentytwenty' ); ?>" role="navigation">

							<ul class="primary-menu reset-list-style">

								<?php
								if ( has_nav_menu( 'primary' ) ) {

									wp_nav_menu(
										array(
											'container'  => '',
											'items_wrap' => '%3$s',
											'theme_location' => 'primary',
										)
									);

								} elseif ( ! has_nav_menu( 'expanded' ) ) {

									wp_list_pages(
										array(
											'match_menu_classes' => true,
											'show_sub_menu_icons' => true,
											'title_li' => false,
											'walker'   => new TwentyTwenty_Walker_Page(),
										)
									);

								}
								?>

							</ul>

						</nav><!-- .primary-menu-wrapper -->

						<?php
					}?>
</div>
				
</header>

<?php
		// Output the menu modal.
get_template_part( 'template-parts/modal-menu' );
