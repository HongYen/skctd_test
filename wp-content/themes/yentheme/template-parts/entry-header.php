<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$entry_header_classes = '';

if ( is_singular() ) {
	$entry_header_classes .= ' header-footer-group';
}

?>

<header class="entry-header<?php echo esc_attr( $entry_header_classes ); ?>">

	<div class="entry-header-inner section-inner medium">

		<?php
			/**
			 * Allow child themes and plugins to filter the display of the categories in the entry header.
			 *
			 * @since Twenty Twenty 1.0
			 *
			 * @param bool   Whether to show the categories in header, Default true.
			 */
		$show_categories = apply_filters( 'twentytwenty_show_categories_in_entry_header', true );

		if ( is_singular() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
			?>		
			<div class="created_by">
	    		<div class="avatar">
	    			<img alt="Avatar" src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar.png"></div>
	    		<div class="info-create">
	    			<div class="time"><span>Bài đăng</span>
	    			<span> <?php echo date("d/m/Y", strtotime($post->post_date)); ?></span>
	    		</div>
	    			<div class="user">By <span class="blue">Danh</span></div>
				</div>
	    	</div>
	<?php
		} else {
			the_title( '<h2 class="entry-title heading-size-1"><a href="' . get_permalink() . '">', '</a></h2>' );
		}

		$intro_text_width = '';

		if ( is_singular() ) {
			$intro_text_width = ' small';
		} else {
			$intro_text_width = ' thin';
		}

		if ( has_excerpt() && is_singular() ) {
			?>

			<div class="intro-text section-inner max-percentage<?php echo $intro_text_width; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>">
				<?php the_excerpt(); ?>
			</div>

			<?php
		}

		
		?>

	</div><!-- .entry-header-inner -->

</header><!-- .entry-header -->
