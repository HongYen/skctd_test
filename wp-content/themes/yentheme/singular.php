<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();

?>
<main id="site-content " class="content-post" role="main">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<?php
				dimox_breadcrumbs();
				?>
		</div>
		<div class="col-sm-9">
			<?php

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );
		}
	}?>
		</div>
	<div class="col-sm-3">
	<?php
		$categories = get_the_category($post->ID);
		$category = reset($categories);
		$category_name = $category->name;
		$category_id = $category->term_id;
		$parent = get_category($category->category_parent);
		$parent_name = is_array($parent) ? $parent->name : '';
		$catquery = new WP_Query( 'cat='. $category_id .'&posts_per_page='. get_field('show_quantity_max', 'option') .'' ); 
		?>
	<h3 class="box_title">
				<?php echo $category_name . ' mới nhất'?>
			</h3>
			<div class="animate-box"><span class="animate-border"></span></div>
			<?php
			$index = 0;
			while($catquery->have_posts()) : $catquery->the_post();
				?>

				<div class="box_content box_content-right">
					<div class="item">
						<div class="row">
							<div class="col-sm-4 custom-pr-5">
								<div class="image">
									<img src="<?php the_post_thumbnail_url(); ?>">
									<a href="<?php echo get_permalink($post->ID); ?>" title='<?php echo $post->post_title ?>' class="view"><i class="fas fa-eye"></i></a>
								</div>
							</div>
							<div class="col-sm-8 custom-pl-5">
								<div class="info">
									<h3 class="title">
										<a href="<?php echo get_permalink($post->ID); ?>" title='<?php echo $post->post_title ?>'>
											<?php echo $post->post_title; ?>
										</a>
									</h3>
									<!-- <p dir="ltr"><?php echo  $post->post_content; ?></p> -->
									<p class="category-post">
										<a href="<?php echo get_permalink($category_name); ?>" class=""><?php echo $category_name; ?></a>
										<span>/</span>
										<span><?php echo date("d/m/Y", strtotime($post->post_date)); ?></span>

									</p>
								</div>


							</div>
						</div>

					</div>
				</div>
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div>
	</div>
</div>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<?php get_form_register();?>
<?php get_footer(); ?>
