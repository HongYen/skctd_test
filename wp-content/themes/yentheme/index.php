<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header('alternative');
?>

<main id="site-content" role="main">
	<div class="banner">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="slide-banner">
						<a href="/">	
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-WEB-LIVE.jpg" alt="Sống khỏe cùng tiểu đường">
						</a>

						<div class="link-live">
							<a class="btn-fb" href="https://www.facebook.com/100580201682678/videos/244948883235752/">Live Facebook </a>
							<a href="https://www.youtube.com/channel/UCV23mLwio0WUm0nxFq4MxXg/live" class="btn-youtobe">
								Live Youtobe
							</a>
						</div>
						<div class="box_menuTop hidden-xs hidden-sm">
							<ul>
								<li>
									<a href="" target="_self" class="">
										<span class="title">Bệnh tiểu đường tuýp 2</span>
										<span class="arrow"><i class="fas fa-caret-right"></i>
										</span>
									</a>
								</li>
								<li  >
									<a href="" target="_self" class="">
										<span class="title">Bệnh tiểu đường tuýp 1</span>
										<span class="arrow"><i class="fas fa-caret-right"></i>
										</span>
									</a>
								</li>
								<li >
									<a href="" target="_self" class="">
										<span class="title">Tiểu đường thai kỳ</span>
										<span class="arrow"><i class="fas fa-caret-right"></i>
										</span>
									</a>
								</li>
								<li  >
									<a href="" target="_self" class="">
										<span class="title">Hiểu đúng về chỉ số xét nghiệm</span>
										<span class="arrow"><i class="fas fa-caret-right"></i>
										</span>
									</a>
								</li>
								<li  >
									<a href="" target="_self" class="">
										<span class="title">Bệnh tiểu đường ở trẻ em</span>
										<span class="arrow">
											<i class="fas fa-caret-right"></i>
										</span>
									</a>
								</li>
								<li  >
									<a href="" target="_self" class="">
										<span class="title">Bệnh tiểu đường ở người cao tuổi</span><span class="arrow"><i class="fas fa-caret-right"></i></span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="box-headline">
						<div class="row">
							<div class="col-sm-12">
								<marquee>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/songkhoecungtieuduong-final-web-1.png" alt="Headline">
								</a> 
							</marquee>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<?php
	// global $query_string;
	// query_posts ('posts_per_page=6');
	if ( have_posts() ) {
		?>
		<div class="list-post">
			<div class="container">
				<div class="row">
					
					<?php $catquery = new WP_Query( 'cat='. get_field('id_post_for_category', 'option') .'&posts_per_page='. get_field('show_quantity_max', 'option') .'' ); ?> 
					<?php
					while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="col-md-4 custom-pr-5">
							<?php
							the_post();

							get_template_part( 'template-parts/content', get_post_type() ); ?>
						</div>


					<?php endwhile;
					wp_reset_postdata();?>

				</div>
			</div>
		</div>
	}
		<div class="box_highlightHome">
		
			<div class="container">
				<div class="content">
					<div class="row">
						<div class="col-sm-9">
							<div class="block-hot blog-hot">
								<h2 class="section-title">
									<a href="/category/chien-luoc-kieng-3-chan", title="Chiến lược kiềng 3 chân" class="text-main">Chiến lược "Kiềng 3 chân"</a>
									
								</h2>
								<div class="row">
									<div class="box_content">
										<div class="col-sm-8">
											<div class="item">
												<div class="item-blog">
													<div class="item-blog-img">
														<a href="/category/dinh-duong" title="Dinh dưỡng"><img src= "<?php echo get_template_directory_uri(); ?>/assets/images/dinhduong.png" alt="Dinh dưỡng"></a>
														<a href="/category/dinh-duong" class="view"><i class="fas fa-eye"></i></a>

													</div>
													<div class="item-blog-info">
														<h2 class="item-blog-title text-center"><a href="/category/dinh-duong" title="Dinh dưỡng">Dinh dưỡng</a></h2>
													</div>
													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="item">
												<div class="item-blog">
													<div class="item-blog-img">
														<a href="/category/van-dong" title="Vận động"><img src= "<?php echo get_template_directory_uri(); ?>/assets/images/vandong.png" alt="Vận động"></a>
														<a href="/category/van-dong" class="view"><i class="fas fa-eye"></i></a>
													</div>
													<div class="item-blog-info">
														<h2 class="item-blog-title text-center"><a href="/category/van-dong" title="Vận động">Vận động</a></h2>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="item-blog">
													<div class="item-blog-img">
														<a href="/category/dieu-tri" title="Điều trị"><img src= "<?php echo get_template_directory_uri(); ?>/assets/images/dieutri.png" alt="Điều trị"></a>
														<a href="/category/dieu-tri" class="view"><i class="fas fa-eye"></i></a>
													</div>
													<div class="item-blog-info">
														<h2 class="item-blog-title text-center"><a href="/category/dieu-tri" title="Điều trị">Điều trị</a></h2>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="box_videoHome" id="box_videoHome">
								<h2 class="box_title"><a href="/category/trai-nghiem-thanh-vien">Trải nghiệm thành viên</a></h2>

								<div class="item">
									<div class="embed-responsive embed-responsive-4by3">
										<a href="#" title="Post Malone - Circles"><img src= "<?php echo get_template_directory_uri(); ?>/assets/images/26a23d823bcfc6919fde.jpg" alt="Post Malone - Circles"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box_newsHome">
			<div class="container">
				<div class="content">
					<div class="row">
						<div class="col-sm-6">
							<div class="box_defaultNew">
								<h3 class="box_title">
									<a href="/category/bien-chung" title="Biến chứng" class="text-main">
										<?php
										$category = get_the_category_by_ID(get_field('id_danh_muc', 'option'));
										echo esc_html( $category );
										?>
									</a>
								</h3>
								<div class="row">
									<?php $catquery = new WP_Query( 'cat='. get_field('id_danh_muc', 'option') .'&posts_per_page='. get_field('limit_post_category_home', 'option') .'' ); ?> 
									<?php
									$index = 0;
									while($catquery->have_posts()) : $catquery->the_post();
										$index++; 
										$class = 'col-md-12 list-item';
										if ($index == 1) {
											$class = 'col-md-8 custom-pr-10';
										}
										if ($index == 2) {
											$class = 'col-md-4 custom-pl-10';
										}
										if ($index == 3) {
											$class = 'col-md-4 custom-pl-10';
										}
										?>
										<div class="<?php echo $class; ?>">
											<div class="box_content">
												<div class="item">
													<div class="image">
														<img src="<?php the_post_thumbnail_url(); ?>">
														<a href="<?php echo get_permalink(''); ?>" class="view" title="<?php echo $post->post_title ?>"><i class="fas fa-eye"></i></a>
													</div>
													<div class="info">
														<h3 class="title">
															<a href="<?php echo get_permalink(''); ?>" title="<?php echo $post->post_title ?>">
																<?php echo $post->post_title; ?></a>
															</h3>
															<p dir="ltr"><?php echo  $post->post_content; ?></p>
														</div>

														
													</div>
												</div>
												
											
										</div>
									<?php endwhile;
									wp_reset_postdata();
									?>
								</div>
							</div>
</div>
							<div class="col-sm-6">
								<div class="box_defaultNew">
									<h3 class="box_title">
										<a href="/category/tin-tuc" title="Tin tức" class="text-main">
											<?php
											$category = get_the_category_by_ID(get_field('id_danh_muc_2', 'option'));
											echo esc_html( $category );
											?>
										</a>
									</h3>
									<div class="row">
										<!-- dùng custom filed để lấy ra bài viet theo danh mục vs sô luong mong muon -->
										<?php $catquery = new WP_Query( 'cat='. get_field('id_danh_muc_2', 'option') .'&posts_per_page='. get_field('limit_post_category_home_2', 'option') .'' ); ?> 
										<?php
										$index = 0;
										while($catquery->have_posts()) : $catquery->the_post();
											$index++; 
											$class = 'col-md-12 list-item';
											if ($index == 1) {
												$class = 'col-md-8 custom-pr-10';
											}
											if ($index == 2) {
												$class = 'col-md-4 custom-pl-10';
											}
											if ($index == 3) {
												$class = 'col-md-4 custom-pl-10';
											}
											?>
											<div class="<?php echo $class; ?>">
												<div class="box_content">
													<div class="item">
														<div class="image">
															<img src="<?php the_post_thumbnail_url(); ?>">
															<a href="<?php echo $post->post_title ?>" class="view"><i class="fas fa-eye"></i></a>
														</div>

														<div class="info">
															<h3 class="title">
																<a href="/bien-chung-cua-tieu-duong" title="<?php echo $post->post_title ?>">
																	<?php echo $post->post_title; ?></a>
																</h3>
															</div>
															
															
														</div>
													</div>

												</div>
												
											<?php endwhile;
											wp_reset_postdata();
											?>
										</div>
									</div>
								</div>
							</div>												
						</div>
					</div>
				</div>

			</div>
		</div>

		<?php get_form_register();?>
		<?php} ?>
	<!-- elseif ( is_search() ) {
		?>

		<div class="no-search-results-form section-inner thin">

			<?php
			get_search_form(
				array(
					'label' => __( 'search again', 'twentytwenty' ),
				)
			);
			?>

		</div>

		<?php
	}
	?> -->

	<?php //get_template_part( 'template-parts/pagination' ); ?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php
get_footer();
