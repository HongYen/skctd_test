<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
<footer id="site-footer" role="contentinfo" class="header-footer-group">

	<div class="main-footer">

		<div class="container">
			<div class="row">

				<div class="item-col col-xs-12 col-sm-6 col-md-3">
					<div class="item-footer">

						<div class="item-footer-content">
							<p><a href="#"><img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo-HAB-tren-web.png" style="height:71px; width:121px"></a></p>

							<p>CÔNG TY CỔ PHẦN&nbsp;HAB VIỆT NAM</p>

							<p>Số 61 Nguyễn Khang, Cầu Giấy, Hà Nội.</p>

							<p><strong><a href="tel:085.888.8585">085.888.8585</a></strong></p>

							<p><a href="mail:tieuduong@hab.vn">tieuduong@hab.vn</a><a href="mailto:contact@langtech.vn"> </a></p>
							<!-- Google Tag Manager -->
							<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
								new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
							j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
							'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
						})(window,document,'script','dataLayer','GTM-T252G3K');</script>
						<!-- End Google Tag Manager -->
					</div>
				</div>
			</div>

			<div class="item-col col-xs-12 col-sm-6 col-md-3">
				<div class="item-footer">
					<h3 class="item-footer-title">Menu</h3>
					<div class="item-footer-content">
						<ul>
							<li><a href="/gioi-thieu">Giới thiệu</a></li>
							<li><a href="#">Sản phẩm</a></li>
							<li><a href="#">Bảng giá</a></li>
							<li><a href="/hoi-dap">Hỏi đáp</a></li>
							<li><a href="/kien-thuc">Kiến thức</a></li>
						</ul>

					</div>
				</div>
			</div>

			<div class="item-col col-xs-12 col-sm-6 col-md-3">
				<div class="item-footer">
					<h3 class="item-footer-title">Kiến thức</h3>
					<div class="item-footer-content">
						<ul>
							<li><a href="#">Tiểu đường là gì</a></li>
							<li><a href="#">Những ai có nguy cơ mắc tiểu đường</a></li>
							<li><a href="#">Tiền tiểu đường</a></li>
							<li><a href="#">Tiểu đường tuýp 1</a></li>
							<li><a href="#">Tiểu đường tuýp&nbsp;</a>2</li>
							<li><a href="#">Tiểu đường thai kỳ</a></li>
						</ul>

						<p>&nbsp;</p>

					</div>
				</div>
			</div>
			<div class="item-col col-xs-12 col-sm-6 col-md-3">
				<div class="item-footer">
					<h3 class="item-footer-title">Ứng dụng</h3>
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/IOS.png"></div>
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/android.png"></div>
				</div>
			</div>
			
		</div>
		<a href="javascript:void(0)" title="" class="btn_top" style="display: none;">
			<i class="fas fa-chevron-up"></i>
		</a>
	</div><!-- .section-inner -->

</footer><!-- #site-footer -->

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/lib/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>
</body>
</html>
