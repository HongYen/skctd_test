<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'yentheme' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_~J=,D;G[h_Hc~n3b~s9,G:5GQd8Y*l%.G9F~K<c#Eq@A^=Ct9)l#b]bu1:LkGF`' );
define( 'SECURE_AUTH_KEY',  'Zgul-1?<&<ABG#gw~^j6F g3IoawH80hBYVBmmPJIiz9&?Y.5KY4EZN*TDKEjdi9' );
define( 'LOGGED_IN_KEY',    'Or=]iQ9KP_]*3ow7RKl?Anf*:cAN ;YN:63A#2q),CCeqWQG1[8=;(QscdrRV.6L' );
define( 'NONCE_KEY',        'mAtrf7SW4IUk3NcGY3q1,vK[jA81iS5_GoMBsH`S,Suy|AoBlsWpnY4mL#Gt]Ju6' );
define( 'AUTH_SALT',        'zy4R;(Ml<(YaJn@In$1 ~DTSsT /<a5FuD>nNf$Db2AY4;`R%W0R2uwn}nDIo]jr' );
define( 'SECURE_AUTH_SALT', 'w $O:n97>e0MWG~(qsC>?;Q8H-,_xay$&yS_blMMXy/H5G+;jYP]XJpdJ`rUzEZI' );
define( 'LOGGED_IN_SALT',   'FdiVB!)MN9Gv#s@g^ED{b;[czl~/!u0z}eaFQ_g]kp;6t*I/=YTU7Q*U^oKF*M=X' );
define( 'NONCE_SALT',       'maqtvKCBE<g#8o^m;0Mg^v<L&_vuzqdA3j1$;=u%I7#j@;`mKBo006*luLf=e<^$' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
